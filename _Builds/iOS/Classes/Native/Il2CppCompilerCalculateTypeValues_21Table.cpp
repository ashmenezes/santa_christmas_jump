﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// AppManager
struct AppManager_t4031413908;
// BallController
struct BallController_t2992829471;
// GoogleMobileAds.Api.CustomNativeTemplateAd
struct CustomNativeTemplateAd_t3403582769;
// GoogleMobileAds.Api.InterstitialAd
struct InterstitialAd_t207380487;
// GoogleMobileAds.Api.RewardBasedVideoAd
struct RewardBasedVideoAd_t2288675867;
// GoogleMobileAds.Common.IAdLoaderClient
struct IAdLoaderClient_t2960292220;
// GoogleMobileAds.Common.IBannerClient
struct IBannerClient_t2365802935;
// GoogleMobileAds.Common.ICustomNativeTemplateClient
struct ICustomNativeTemplateClient_t2241739572;
// GoogleMobileAds.Common.IInterstitialClient
struct IInterstitialClient_t3845106718;
// GoogleMobileAds.Common.IMobileAdsClient
struct IMobileAdsClient_t673744088;
// GoogleMobileAds.Common.IRewardBasedVideoAdClient
struct IRewardBasedVideoAdClient_t3292701488;
// GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidFailToReceiveAdWithErrorCallback
struct GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432;
// GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback
struct GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418;
// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidDismissScreenCallback
struct GADUAdViewDidDismissScreenCallback_t972393216;
// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidFailToReceiveAdWithErrorCallback
struct GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547;
// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidReceiveAdCallback
struct GADUAdViewDidReceiveAdCallback_t2543294242;
// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillLeaveApplicationCallback
struct GADUAdViewWillLeaveApplicationCallback_t3323587265;
// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillPresentScreenCallback
struct GADUAdViewWillPresentScreenCallback_t2057580186;
// GoogleMobileAds.iOS.CustomNativeTemplateClient/GADUNativeCustomTemplateDidReceiveClick
struct GADUNativeCustomTemplateDidReceiveClick_t350204406;
// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidDismissScreenCallback
struct GADUInterstitialDidDismissScreenCallback_t1339081348;
// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidFailToReceiveAdWithErrorCallback
struct GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714;
// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidReceiveAdCallback
struct GADUInterstitialDidReceiveAdCallback_t821971233;
// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillLeaveApplicationCallback
struct GADUInterstitialWillLeaveApplicationCallback_t1816935820;
// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillPresentScreenCallback
struct GADUInterstitialWillPresentScreenCallback_t539653454;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback
struct GADURewardBasedVideoAdDidCloseCallback_t623082069;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCompleteCallback
struct GADURewardBasedVideoAdDidCompleteCallback_t2076181;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback
struct GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback
struct GADURewardBasedVideoAdDidOpenCallback_t3638490629;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback
struct GADURewardBasedVideoAdDidReceiveAdCallback_t462486315;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback
struct GADURewardBasedVideoAdDidRewardCallback_t990863796;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback
struct GADURewardBasedVideoAdDidStartCallback_t2792276088;
// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback
struct GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531;
// Ragdoll
struct Ragdoll_t3598225712;
// System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>
struct Action_2_t1070808194;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>>
struct Dictionary_2_t856064493;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType>
struct HashSet_1_t324370440;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t412400163;
// System.Collections.Generic.List`1<AppManager/Floor>
struct List_1_t2427226674;
// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras>
struct List_1_t3723909906;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2736452219;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam>
struct List_1_t3952196670;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t235857739;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs>
struct EventHandler_1_t780121155;
// System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs>
struct EventHandler_1_t1283979565;
// System.EventHandler`1<GoogleMobileAds.Api.Reward>
struct EventHandler_1_t1628180368;
// System.EventHandler`1<System.EventArgs>
struct EventHandler_1_t1515976428;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.IDisposable
struct IDisposable_t3640265483;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct AnalyticsEventParamListContainer_t587083383;
// UnityEngine.Analytics.AnalyticsEventTracker
struct AnalyticsEventTracker_t2285229262;
// UnityEngine.Analytics.EventTrigger
struct EventTrigger_t2527451695;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.StandardEventPayload
struct StandardEventPayload_t1629891255;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t3943537984;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifndef U3CMODULEU3E_T692745547_H
#define U3CMODULEU3E_T692745547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745547 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745547_H
#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CENDGAMEU3EC__ITERATOR0_T76120099_H
#define U3CENDGAMEU3EC__ITERATOR0_T76120099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppManager/<EndGame>c__Iterator0
struct  U3CEndGameU3Ec__Iterator0_t76120099  : public RuntimeObject
{
public:
	// AppManager AppManager/<EndGame>c__Iterator0::$this
	AppManager_t4031413908 * ___U24this_0;
	// System.Object AppManager/<EndGame>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean AppManager/<EndGame>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 AppManager/<EndGame>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CEndGameU3Ec__Iterator0_t76120099, ___U24this_0)); }
	inline AppManager_t4031413908 * get_U24this_0() const { return ___U24this_0; }
	inline AppManager_t4031413908 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AppManager_t4031413908 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CEndGameU3Ec__Iterator0_t76120099, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CEndGameU3Ec__Iterator0_t76120099, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CEndGameU3Ec__Iterator0_t76120099, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENDGAMEU3EC__ITERATOR0_T76120099_H
#ifndef FLOOR_T955151932_H
#define FLOOR_T955151932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppManager/Floor
struct  Floor_t955151932  : public RuntimeObject
{
public:
	// UnityEngine.GameObject AppManager/Floor::prefab
	GameObject_t1113636619 * ___prefab_0;
	// System.Single AppManager/Floor::weight
	float ___weight_1;
	// System.Single AppManager/Floor::sumWeight
	float ___sumWeight_2;

public:
	inline static int32_t get_offset_of_prefab_0() { return static_cast<int32_t>(offsetof(Floor_t955151932, ___prefab_0)); }
	inline GameObject_t1113636619 * get_prefab_0() const { return ___prefab_0; }
	inline GameObject_t1113636619 ** get_address_of_prefab_0() { return &___prefab_0; }
	inline void set_prefab_0(GameObject_t1113636619 * value)
	{
		___prefab_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefab_0), value);
	}

	inline static int32_t get_offset_of_weight_1() { return static_cast<int32_t>(offsetof(Floor_t955151932, ___weight_1)); }
	inline float get_weight_1() const { return ___weight_1; }
	inline float* get_address_of_weight_1() { return &___weight_1; }
	inline void set_weight_1(float value)
	{
		___weight_1 = value;
	}

	inline static int32_t get_offset_of_sumWeight_2() { return static_cast<int32_t>(offsetof(Floor_t955151932, ___sumWeight_2)); }
	inline float get_sumWeight_2() const { return ___sumWeight_2; }
	inline float* get_address_of_sumWeight_2() { return &___sumWeight_2; }
	inline void set_sumWeight_2(float value)
	{
		___sumWeight_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOOR_T955151932_H
#ifndef ADLOADER_T3255226653_H
#define ADLOADER_T3255226653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdLoader
struct  AdLoader_t3255226653  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IAdLoaderClient GoogleMobileAds.Api.AdLoader::adLoaderClient
	RuntimeObject* ___adLoaderClient_0;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.AdLoader::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_1;
	// System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs> GoogleMobileAds.Api.AdLoader::OnCustomNativeTemplateAdLoaded
	EventHandler_1_t1283979565 * ___OnCustomNativeTemplateAdLoaded_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>> GoogleMobileAds.Api.AdLoader::<CustomNativeTemplateClickHandlers>k__BackingField
	Dictionary_2_t856064493 * ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3;
	// System.String GoogleMobileAds.Api.AdLoader::<AdUnitId>k__BackingField
	String_t* ___U3CAdUnitIdU3Ek__BackingField_4;
	// System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType> GoogleMobileAds.Api.AdLoader::<AdTypes>k__BackingField
	HashSet_1_t324370440 * ___U3CAdTypesU3Ek__BackingField_5;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdLoader::<TemplateIds>k__BackingField
	HashSet_1_t412400163 * ___U3CTemplateIdsU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_adLoaderClient_0() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___adLoaderClient_0)); }
	inline RuntimeObject* get_adLoaderClient_0() const { return ___adLoaderClient_0; }
	inline RuntimeObject** get_address_of_adLoaderClient_0() { return &___adLoaderClient_0; }
	inline void set_adLoaderClient_0(RuntimeObject* value)
	{
		___adLoaderClient_0 = value;
		Il2CppCodeGenWriteBarrier((&___adLoaderClient_0), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_1() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___OnAdFailedToLoad_1)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_1() const { return ___OnAdFailedToLoad_1; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_1() { return &___OnAdFailedToLoad_1; }
	inline void set_OnAdFailedToLoad_1(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_1), value);
	}

	inline static int32_t get_offset_of_OnCustomNativeTemplateAdLoaded_2() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___OnCustomNativeTemplateAdLoaded_2)); }
	inline EventHandler_1_t1283979565 * get_OnCustomNativeTemplateAdLoaded_2() const { return ___OnCustomNativeTemplateAdLoaded_2; }
	inline EventHandler_1_t1283979565 ** get_address_of_OnCustomNativeTemplateAdLoaded_2() { return &___OnCustomNativeTemplateAdLoaded_2; }
	inline void set_OnCustomNativeTemplateAdLoaded_2(EventHandler_1_t1283979565 * value)
	{
		___OnCustomNativeTemplateAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnCustomNativeTemplateAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3)); }
	inline Dictionary_2_t856064493 * get_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() const { return ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3; }
	inline Dictionary_2_t856064493 ** get_address_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() { return &___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3; }
	inline void set_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(Dictionary_2_t856064493 * value)
	{
		___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CAdUnitIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___U3CAdUnitIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CAdUnitIdU3Ek__BackingField_4() const { return ___U3CAdUnitIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CAdUnitIdU3Ek__BackingField_4() { return &___U3CAdUnitIdU3Ek__BackingField_4; }
	inline void set_U3CAdUnitIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CAdUnitIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdUnitIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CAdTypesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___U3CAdTypesU3Ek__BackingField_5)); }
	inline HashSet_1_t324370440 * get_U3CAdTypesU3Ek__BackingField_5() const { return ___U3CAdTypesU3Ek__BackingField_5; }
	inline HashSet_1_t324370440 ** get_address_of_U3CAdTypesU3Ek__BackingField_5() { return &___U3CAdTypesU3Ek__BackingField_5; }
	inline void set_U3CAdTypesU3Ek__BackingField_5(HashSet_1_t324370440 * value)
	{
		___U3CAdTypesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdTypesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CTemplateIdsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdLoader_t3255226653, ___U3CTemplateIdsU3Ek__BackingField_6)); }
	inline HashSet_1_t412400163 * get_U3CTemplateIdsU3Ek__BackingField_6() const { return ___U3CTemplateIdsU3Ek__BackingField_6; }
	inline HashSet_1_t412400163 ** get_address_of_U3CTemplateIdsU3Ek__BackingField_6() { return &___U3CTemplateIdsU3Ek__BackingField_6; }
	inline void set_U3CTemplateIdsU3Ek__BackingField_6(HashSet_1_t412400163 * value)
	{
		___U3CTemplateIdsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTemplateIdsU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLOADER_T3255226653_H
#ifndef BUILDER_T2638244817_H
#define BUILDER_T2638244817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdLoader/Builder
struct  Builder_t2638244817  : public RuntimeObject
{
public:
	// System.String GoogleMobileAds.Api.AdLoader/Builder::<AdUnitId>k__BackingField
	String_t* ___U3CAdUnitIdU3Ek__BackingField_0;
	// System.Collections.Generic.HashSet`1<GoogleMobileAds.Api.NativeAdType> GoogleMobileAds.Api.AdLoader/Builder::<AdTypes>k__BackingField
	HashSet_1_t324370440 * ___U3CAdTypesU3Ek__BackingField_1;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdLoader/Builder::<TemplateIds>k__BackingField
	HashSet_1_t412400163 * ___U3CTemplateIdsU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>> GoogleMobileAds.Api.AdLoader/Builder::<CustomNativeTemplateClickHandlers>k__BackingField
	Dictionary_2_t856064493 * ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CAdUnitIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Builder_t2638244817, ___U3CAdUnitIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CAdUnitIdU3Ek__BackingField_0() const { return ___U3CAdUnitIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAdUnitIdU3Ek__BackingField_0() { return &___U3CAdUnitIdU3Ek__BackingField_0; }
	inline void set_U3CAdUnitIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CAdUnitIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdUnitIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAdTypesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Builder_t2638244817, ___U3CAdTypesU3Ek__BackingField_1)); }
	inline HashSet_1_t324370440 * get_U3CAdTypesU3Ek__BackingField_1() const { return ___U3CAdTypesU3Ek__BackingField_1; }
	inline HashSet_1_t324370440 ** get_address_of_U3CAdTypesU3Ek__BackingField_1() { return &___U3CAdTypesU3Ek__BackingField_1; }
	inline void set_U3CAdTypesU3Ek__BackingField_1(HashSet_1_t324370440 * value)
	{
		___U3CAdTypesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAdTypesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTemplateIdsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Builder_t2638244817, ___U3CTemplateIdsU3Ek__BackingField_2)); }
	inline HashSet_1_t412400163 * get_U3CTemplateIdsU3Ek__BackingField_2() const { return ___U3CTemplateIdsU3Ek__BackingField_2; }
	inline HashSet_1_t412400163 ** get_address_of_U3CTemplateIdsU3Ek__BackingField_2() { return &___U3CTemplateIdsU3Ek__BackingField_2; }
	inline void set_U3CTemplateIdsU3Ek__BackingField_2(HashSet_1_t412400163 * value)
	{
		___U3CTemplateIdsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTemplateIdsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Builder_t2638244817, ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3)); }
	inline Dictionary_2_t856064493 * get_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() const { return ___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3; }
	inline Dictionary_2_t856064493 ** get_address_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3() { return &___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3; }
	inline void set_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(Dictionary_2_t856064493 * value)
	{
		___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T2638244817_H
#ifndef ADSIZE_T2717023072_H
#define ADSIZE_T2717023072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdSize
struct  AdSize_t2717023072  : public RuntimeObject
{
public:
	// System.Boolean GoogleMobileAds.Api.AdSize::isSmartBanner
	bool ___isSmartBanner_0;
	// System.Int32 GoogleMobileAds.Api.AdSize::width
	int32_t ___width_1;
	// System.Int32 GoogleMobileAds.Api.AdSize::height
	int32_t ___height_2;

public:
	inline static int32_t get_offset_of_isSmartBanner_0() { return static_cast<int32_t>(offsetof(AdSize_t2717023072, ___isSmartBanner_0)); }
	inline bool get_isSmartBanner_0() const { return ___isSmartBanner_0; }
	inline bool* get_address_of_isSmartBanner_0() { return &___isSmartBanner_0; }
	inline void set_isSmartBanner_0(bool value)
	{
		___isSmartBanner_0 = value;
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(AdSize_t2717023072, ___width_1)); }
	inline int32_t get_width_1() const { return ___width_1; }
	inline int32_t* get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(int32_t value)
	{
		___width_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(AdSize_t2717023072, ___height_2)); }
	inline int32_t get_height_2() const { return ___height_2; }
	inline int32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(int32_t value)
	{
		___height_2 = value;
	}
};

struct AdSize_t2717023072_StaticFields
{
public:
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::Banner
	AdSize_t2717023072 * ___Banner_3;
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::MediumRectangle
	AdSize_t2717023072 * ___MediumRectangle_4;
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::IABBanner
	AdSize_t2717023072 * ___IABBanner_5;
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::Leaderboard
	AdSize_t2717023072 * ___Leaderboard_6;
	// GoogleMobileAds.Api.AdSize GoogleMobileAds.Api.AdSize::SmartBanner
	AdSize_t2717023072 * ___SmartBanner_7;
	// System.Int32 GoogleMobileAds.Api.AdSize::FullWidth
	int32_t ___FullWidth_8;

public:
	inline static int32_t get_offset_of_Banner_3() { return static_cast<int32_t>(offsetof(AdSize_t2717023072_StaticFields, ___Banner_3)); }
	inline AdSize_t2717023072 * get_Banner_3() const { return ___Banner_3; }
	inline AdSize_t2717023072 ** get_address_of_Banner_3() { return &___Banner_3; }
	inline void set_Banner_3(AdSize_t2717023072 * value)
	{
		___Banner_3 = value;
		Il2CppCodeGenWriteBarrier((&___Banner_3), value);
	}

	inline static int32_t get_offset_of_MediumRectangle_4() { return static_cast<int32_t>(offsetof(AdSize_t2717023072_StaticFields, ___MediumRectangle_4)); }
	inline AdSize_t2717023072 * get_MediumRectangle_4() const { return ___MediumRectangle_4; }
	inline AdSize_t2717023072 ** get_address_of_MediumRectangle_4() { return &___MediumRectangle_4; }
	inline void set_MediumRectangle_4(AdSize_t2717023072 * value)
	{
		___MediumRectangle_4 = value;
		Il2CppCodeGenWriteBarrier((&___MediumRectangle_4), value);
	}

	inline static int32_t get_offset_of_IABBanner_5() { return static_cast<int32_t>(offsetof(AdSize_t2717023072_StaticFields, ___IABBanner_5)); }
	inline AdSize_t2717023072 * get_IABBanner_5() const { return ___IABBanner_5; }
	inline AdSize_t2717023072 ** get_address_of_IABBanner_5() { return &___IABBanner_5; }
	inline void set_IABBanner_5(AdSize_t2717023072 * value)
	{
		___IABBanner_5 = value;
		Il2CppCodeGenWriteBarrier((&___IABBanner_5), value);
	}

	inline static int32_t get_offset_of_Leaderboard_6() { return static_cast<int32_t>(offsetof(AdSize_t2717023072_StaticFields, ___Leaderboard_6)); }
	inline AdSize_t2717023072 * get_Leaderboard_6() const { return ___Leaderboard_6; }
	inline AdSize_t2717023072 ** get_address_of_Leaderboard_6() { return &___Leaderboard_6; }
	inline void set_Leaderboard_6(AdSize_t2717023072 * value)
	{
		___Leaderboard_6 = value;
		Il2CppCodeGenWriteBarrier((&___Leaderboard_6), value);
	}

	inline static int32_t get_offset_of_SmartBanner_7() { return static_cast<int32_t>(offsetof(AdSize_t2717023072_StaticFields, ___SmartBanner_7)); }
	inline AdSize_t2717023072 * get_SmartBanner_7() const { return ___SmartBanner_7; }
	inline AdSize_t2717023072 ** get_address_of_SmartBanner_7() { return &___SmartBanner_7; }
	inline void set_SmartBanner_7(AdSize_t2717023072 * value)
	{
		___SmartBanner_7 = value;
		Il2CppCodeGenWriteBarrier((&___SmartBanner_7), value);
	}

	inline static int32_t get_offset_of_FullWidth_8() { return static_cast<int32_t>(offsetof(AdSize_t2717023072_StaticFields, ___FullWidth_8)); }
	inline int32_t get_FullWidth_8() const { return ___FullWidth_8; }
	inline int32_t* get_address_of_FullWidth_8() { return &___FullWidth_8; }
	inline void set_FullWidth_8(int32_t value)
	{
		___FullWidth_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSIZE_T2717023072_H
#ifndef BANNERVIEW_T2480907735_H
#define BANNERVIEW_T2480907735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.BannerView
struct  BannerView_t2480907735  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IBannerClient GoogleMobileAds.Api.BannerView::client
	RuntimeObject* ___client_0;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.BannerView::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_1;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.BannerView::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_2;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.BannerView::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.BannerView::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.BannerView::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_5;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(BannerView_t2480907735, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_OnAdLoaded_1() { return static_cast<int32_t>(offsetof(BannerView_t2480907735, ___OnAdLoaded_1)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_1() const { return ___OnAdLoaded_1; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_1() { return &___OnAdLoaded_1; }
	inline void set_OnAdLoaded_1(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_1), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_2() { return static_cast<int32_t>(offsetof(BannerView_t2480907735, ___OnAdFailedToLoad_2)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_2() const { return ___OnAdFailedToLoad_2; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_2() { return &___OnAdFailedToLoad_2; }
	inline void set_OnAdFailedToLoad_2(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_2), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_3() { return static_cast<int32_t>(offsetof(BannerView_t2480907735, ___OnAdOpening_3)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_3() const { return ___OnAdOpening_3; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_3() { return &___OnAdOpening_3; }
	inline void set_OnAdOpening_3(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_3), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_4() { return static_cast<int32_t>(offsetof(BannerView_t2480907735, ___OnAdClosed_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_4() const { return ___OnAdClosed_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_4() { return &___OnAdClosed_4; }
	inline void set_OnAdClosed_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_4), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_5() { return static_cast<int32_t>(offsetof(BannerView_t2480907735, ___OnAdLeavingApplication_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_5() const { return ___OnAdLeavingApplication_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_5() { return &___OnAdLeavingApplication_5; }
	inline void set_OnAdLeavingApplication_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANNERVIEW_T2480907735_H
#ifndef CUSTOMNATIVETEMPLATEAD_T3403582769_H
#define CUSTOMNATIVETEMPLATEAD_T3403582769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.CustomNativeTemplateAd
struct  CustomNativeTemplateAd_t3403582769  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.ICustomNativeTemplateClient GoogleMobileAds.Api.CustomNativeTemplateAd::client
	RuntimeObject* ___client_0;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(CustomNativeTemplateAd_t3403582769, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMNATIVETEMPLATEAD_T3403582769_H
#ifndef INTERSTITIALAD_T207380487_H
#define INTERSTITIALAD_T207380487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.InterstitialAd
struct  InterstitialAd_t207380487  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IInterstitialClient GoogleMobileAds.Api.InterstitialAd::client
	RuntimeObject* ___client_0;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_1;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_2;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.InterstitialAd::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_5;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(InterstitialAd_t207380487, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_OnAdLoaded_1() { return static_cast<int32_t>(offsetof(InterstitialAd_t207380487, ___OnAdLoaded_1)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_1() const { return ___OnAdLoaded_1; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_1() { return &___OnAdLoaded_1; }
	inline void set_OnAdLoaded_1(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_1), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_2() { return static_cast<int32_t>(offsetof(InterstitialAd_t207380487, ___OnAdFailedToLoad_2)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_2() const { return ___OnAdFailedToLoad_2; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_2() { return &___OnAdFailedToLoad_2; }
	inline void set_OnAdFailedToLoad_2(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_2), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_3() { return static_cast<int32_t>(offsetof(InterstitialAd_t207380487, ___OnAdOpening_3)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_3() const { return ___OnAdOpening_3; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_3() { return &___OnAdOpening_3; }
	inline void set_OnAdOpening_3(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_3), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_4() { return static_cast<int32_t>(offsetof(InterstitialAd_t207380487, ___OnAdClosed_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_4() const { return ___OnAdClosed_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_4() { return &___OnAdClosed_4; }
	inline void set_OnAdClosed_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_4), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_5() { return static_cast<int32_t>(offsetof(InterstitialAd_t207380487, ___OnAdLeavingApplication_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_5() const { return ___OnAdLeavingApplication_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_5() { return &___OnAdLeavingApplication_5; }
	inline void set_OnAdLeavingApplication_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERSTITIALAD_T207380487_H
#ifndef MEDIATIONEXTRAS_T2251835164_H
#define MEDIATIONEXTRAS_T2251835164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.Mediation.MediationExtras
struct  MediationExtras_t2251835164  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.Mediation.MediationExtras::<Extras>k__BackingField
	Dictionary_2_t1632706988 * ___U3CExtrasU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MediationExtras_t2251835164, ___U3CExtrasU3Ek__BackingField_0)); }
	inline Dictionary_2_t1632706988 * get_U3CExtrasU3Ek__BackingField_0() const { return ___U3CExtrasU3Ek__BackingField_0; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CExtrasU3Ek__BackingField_0() { return &___U3CExtrasU3Ek__BackingField_0; }
	inline void set_U3CExtrasU3Ek__BackingField_0(Dictionary_2_t1632706988 * value)
	{
		___U3CExtrasU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtrasU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATIONEXTRAS_T2251835164_H
#ifndef MOBILEADS_T1050225196_H
#define MOBILEADS_T1050225196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.MobileAds
struct  MobileAds_t1050225196  : public RuntimeObject
{
public:

public:
};

struct MobileAds_t1050225196_StaticFields
{
public:
	// GoogleMobileAds.Common.IMobileAdsClient GoogleMobileAds.Api.MobileAds::client
	RuntimeObject* ___client_0;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(MobileAds_t1050225196_StaticFields, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEADS_T1050225196_H
#ifndef REWARDBASEDVIDEOAD_T2288675867_H
#define REWARDBASEDVIDEOAD_T2288675867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.RewardBasedVideoAd
struct  RewardBasedVideoAd_t2288675867  : public RuntimeObject
{
public:
	// GoogleMobileAds.Common.IRewardBasedVideoAdClient GoogleMobileAds.Api.RewardBasedVideoAd::client
	RuntimeObject* ___client_0;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdStarted
	EventHandler_1_t1515976428 * ___OnAdStarted_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_6;
	// System.EventHandler`1<GoogleMobileAds.Api.Reward> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdRewarded
	EventHandler_1_t1628180368 * ___OnAdRewarded_7;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_8;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Api.RewardBasedVideoAd::OnAdCompleted
	EventHandler_1_t1515976428 * ___OnAdCompleted_9;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___client_0)); }
	inline RuntimeObject* get_client_0() const { return ___client_0; }
	inline RuntimeObject** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(RuntimeObject* value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_OnAdLoaded_2() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdLoaded_2)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_2() const { return ___OnAdLoaded_2; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_2() { return &___OnAdLoaded_2; }
	inline void set_OnAdLoaded_2(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_3() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdFailedToLoad_3)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_3() const { return ___OnAdFailedToLoad_3; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_3() { return &___OnAdFailedToLoad_3; }
	inline void set_OnAdFailedToLoad_3(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_3), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_4() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdOpening_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_4() const { return ___OnAdOpening_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_4() { return &___OnAdOpening_4; }
	inline void set_OnAdOpening_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_4), value);
	}

	inline static int32_t get_offset_of_OnAdStarted_5() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdStarted_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdStarted_5() const { return ___OnAdStarted_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdStarted_5() { return &___OnAdStarted_5; }
	inline void set_OnAdStarted_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdStarted_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdStarted_5), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_6() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdClosed_6)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_6() const { return ___OnAdClosed_6; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_6() { return &___OnAdClosed_6; }
	inline void set_OnAdClosed_6(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_6), value);
	}

	inline static int32_t get_offset_of_OnAdRewarded_7() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdRewarded_7)); }
	inline EventHandler_1_t1628180368 * get_OnAdRewarded_7() const { return ___OnAdRewarded_7; }
	inline EventHandler_1_t1628180368 ** get_address_of_OnAdRewarded_7() { return &___OnAdRewarded_7; }
	inline void set_OnAdRewarded_7(EventHandler_1_t1628180368 * value)
	{
		___OnAdRewarded_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdRewarded_7), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_8() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdLeavingApplication_8)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_8() const { return ___OnAdLeavingApplication_8; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_8() { return &___OnAdLeavingApplication_8; }
	inline void set_OnAdLeavingApplication_8(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_8), value);
	}

	inline static int32_t get_offset_of_OnAdCompleted_9() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867, ___OnAdCompleted_9)); }
	inline EventHandler_1_t1515976428 * get_OnAdCompleted_9() const { return ___OnAdCompleted_9; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdCompleted_9() { return &___OnAdCompleted_9; }
	inline void set_OnAdCompleted_9(EventHandler_1_t1515976428 * value)
	{
		___OnAdCompleted_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdCompleted_9), value);
	}
};

struct RewardBasedVideoAd_t2288675867_StaticFields
{
public:
	// GoogleMobileAds.Api.RewardBasedVideoAd GoogleMobileAds.Api.RewardBasedVideoAd::instance
	RewardBasedVideoAd_t2288675867 * ___instance_1;

public:
	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(RewardBasedVideoAd_t2288675867_StaticFields, ___instance_1)); }
	inline RewardBasedVideoAd_t2288675867 * get_instance_1() const { return ___instance_1; }
	inline RewardBasedVideoAd_t2288675867 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(RewardBasedVideoAd_t2288675867 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDBASEDVIDEOAD_T2288675867_H
#ifndef DUMMYCLIENT_T519661512_H
#define DUMMYCLIENT_T519661512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Common.DummyClient
struct  DummyClient_t519661512  : public RuntimeObject
{
public:
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.DummyClient::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_0;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.Common.DummyClient::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_1;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.DummyClient::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_2;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.DummyClient::OnAdStarted
	EventHandler_1_t1515976428 * ___OnAdStarted_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.DummyClient::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_4;
	// System.EventHandler`1<GoogleMobileAds.Api.Reward> GoogleMobileAds.Common.DummyClient::OnAdRewarded
	EventHandler_1_t1628180368 * ___OnAdRewarded_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.DummyClient::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_6;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.Common.DummyClient::OnAdCompleted
	EventHandler_1_t1515976428 * ___OnAdCompleted_7;
	// System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs> GoogleMobileAds.Common.DummyClient::OnCustomNativeTemplateAdLoaded
	EventHandler_1_t1283979565 * ___OnCustomNativeTemplateAdLoaded_8;

public:
	inline static int32_t get_offset_of_OnAdLoaded_0() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdLoaded_0)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_0() const { return ___OnAdLoaded_0; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_0() { return &___OnAdLoaded_0; }
	inline void set_OnAdLoaded_0(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_0), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_1() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdFailedToLoad_1)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_1() const { return ___OnAdFailedToLoad_1; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_1() { return &___OnAdFailedToLoad_1; }
	inline void set_OnAdFailedToLoad_1(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_1), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_2() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdOpening_2)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_2() const { return ___OnAdOpening_2; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_2() { return &___OnAdOpening_2; }
	inline void set_OnAdOpening_2(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_2), value);
	}

	inline static int32_t get_offset_of_OnAdStarted_3() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdStarted_3)); }
	inline EventHandler_1_t1515976428 * get_OnAdStarted_3() const { return ___OnAdStarted_3; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdStarted_3() { return &___OnAdStarted_3; }
	inline void set_OnAdStarted_3(EventHandler_1_t1515976428 * value)
	{
		___OnAdStarted_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdStarted_3), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_4() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdClosed_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_4() const { return ___OnAdClosed_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_4() { return &___OnAdClosed_4; }
	inline void set_OnAdClosed_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_4), value);
	}

	inline static int32_t get_offset_of_OnAdRewarded_5() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdRewarded_5)); }
	inline EventHandler_1_t1628180368 * get_OnAdRewarded_5() const { return ___OnAdRewarded_5; }
	inline EventHandler_1_t1628180368 ** get_address_of_OnAdRewarded_5() { return &___OnAdRewarded_5; }
	inline void set_OnAdRewarded_5(EventHandler_1_t1628180368 * value)
	{
		___OnAdRewarded_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdRewarded_5), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_6() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdLeavingApplication_6)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_6() const { return ___OnAdLeavingApplication_6; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_6() { return &___OnAdLeavingApplication_6; }
	inline void set_OnAdLeavingApplication_6(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_6), value);
	}

	inline static int32_t get_offset_of_OnAdCompleted_7() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnAdCompleted_7)); }
	inline EventHandler_1_t1515976428 * get_OnAdCompleted_7() const { return ___OnAdCompleted_7; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdCompleted_7() { return &___OnAdCompleted_7; }
	inline void set_OnAdCompleted_7(EventHandler_1_t1515976428 * value)
	{
		___OnAdCompleted_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdCompleted_7), value);
	}

	inline static int32_t get_offset_of_OnCustomNativeTemplateAdLoaded_8() { return static_cast<int32_t>(offsetof(DummyClient_t519661512, ___OnCustomNativeTemplateAdLoaded_8)); }
	inline EventHandler_1_t1283979565 * get_OnCustomNativeTemplateAdLoaded_8() const { return ___OnCustomNativeTemplateAdLoaded_8; }
	inline EventHandler_1_t1283979565 ** get_address_of_OnCustomNativeTemplateAdLoaded_8() { return &___OnCustomNativeTemplateAdLoaded_8; }
	inline void set_OnCustomNativeTemplateAdLoaded_8(EventHandler_1_t1283979565 * value)
	{
		___OnCustomNativeTemplateAdLoaded_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnCustomNativeTemplateAdLoaded_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYCLIENT_T519661512_H
#ifndef UTILS_T3548761374_H
#define UTILS_T3548761374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Common.Utils
struct  Utils_t3548761374  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T3548761374_H
#ifndef GOOGLEMOBILEADSCLIENTFACTORY_T3556675256_H
#define GOOGLEMOBILEADSCLIENTFACTORY_T3556675256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.GoogleMobileAdsClientFactory
struct  GoogleMobileAdsClientFactory_t3556675256  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEMOBILEADSCLIENTFACTORY_T3556675256_H
#ifndef EXTERNS_T92207873_H
#define EXTERNS_T92207873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.Externs
struct  Externs_t92207873  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNS_T92207873_H
#ifndef MOBILEADSCLIENT_T1008075298_H
#define MOBILEADSCLIENT_T1008075298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.MobileAdsClient
struct  MobileAdsClient_t1008075298  : public RuntimeObject
{
public:

public:
};

struct MobileAdsClient_t1008075298_StaticFields
{
public:
	// GoogleMobileAds.iOS.MobileAdsClient GoogleMobileAds.iOS.MobileAdsClient::instance
	MobileAdsClient_t1008075298 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(MobileAdsClient_t1008075298_StaticFields, ___instance_0)); }
	inline MobileAdsClient_t1008075298 * get_instance_0() const { return ___instance_0; }
	inline MobileAdsClient_t1008075298 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(MobileAdsClient_t1008075298 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEADSCLIENT_T1008075298_H
#ifndef UTILS_T143735646_H
#define UTILS_T143735646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.Utils
struct  Utils_t143735646  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T143735646_H
#ifndef U3CEXPLODESELFU3EC__ITERATOR0_T2434711746_H
#define U3CEXPLODESELFU3EC__ITERATOR0_T2434711746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ragdoll/<ExplodeSelf>c__Iterator0
struct  U3CExplodeSelfU3Ec__Iterator0_t2434711746  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator Ragdoll/<ExplodeSelf>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_0;
	// System.IDisposable Ragdoll/<ExplodeSelf>c__Iterator0::$locvar1
	RuntimeObject* ___U24locvar1_1;
	// System.Boolean Ragdoll/<ExplodeSelf>c__Iterator0::explode
	bool ___explode_2;
	// Ragdoll Ragdoll/<ExplodeSelf>c__Iterator0::$this
	Ragdoll_t3598225712 * ___U24this_3;
	// System.Object Ragdoll/<ExplodeSelf>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Ragdoll/<ExplodeSelf>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Ragdoll/<ExplodeSelf>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CExplodeSelfU3Ec__Iterator0_t2434711746, ___U24locvar0_0)); }
	inline RuntimeObject* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline RuntimeObject** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(RuntimeObject* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CExplodeSelfU3Ec__Iterator0_t2434711746, ___U24locvar1_1)); }
	inline RuntimeObject* get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline RuntimeObject** get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(RuntimeObject* value)
	{
		___U24locvar1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_1), value);
	}

	inline static int32_t get_offset_of_explode_2() { return static_cast<int32_t>(offsetof(U3CExplodeSelfU3Ec__Iterator0_t2434711746, ___explode_2)); }
	inline bool get_explode_2() const { return ___explode_2; }
	inline bool* get_address_of_explode_2() { return &___explode_2; }
	inline void set_explode_2(bool value)
	{
		___explode_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CExplodeSelfU3Ec__Iterator0_t2434711746, ___U24this_3)); }
	inline Ragdoll_t3598225712 * get_U24this_3() const { return ___U24this_3; }
	inline Ragdoll_t3598225712 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(Ragdoll_t3598225712 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CExplodeSelfU3Ec__Iterator0_t2434711746, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CExplodeSelfU3Ec__Iterator0_t2434711746, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CExplodeSelfU3Ec__Iterator0_t2434711746, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXPLODESELFU3EC__ITERATOR0_T2434711746_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#define ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct  AnalyticsEventParamListContainer_t587083383  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam> UnityEngine.Analytics.AnalyticsEventParamListContainer::m_Parameters
	List_1_t3952196670 * ___m_Parameters_0;

public:
	inline static int32_t get_offset_of_m_Parameters_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParamListContainer_t587083383, ___m_Parameters_0)); }
	inline List_1_t3952196670 * get_m_Parameters_0() const { return ___m_Parameters_0; }
	inline List_1_t3952196670 ** get_address_of_m_Parameters_0() { return &___m_Parameters_0; }
	inline void set_m_Parameters_0(List_1_t3952196670 * value)
	{
		___m_Parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifndef U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#define U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0
struct  U3CTimedTriggerU3Ec__Iterator0_t3813435494  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventTracker UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$this
	AnalyticsEventTracker_t2285229262 * ___U24this_0;
	// System.Object UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24this_0)); }
	inline AnalyticsEventTracker_t2285229262 * get_U24this_0() const { return ___U24this_0; }
	inline AnalyticsEventTracker_t2285229262 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnalyticsEventTracker_t2285229262 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifndef ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#define ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTrackerSettings
struct  AnalyticsEventTrackerSettings_t480422680  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEventTrackerSettings_t480422680_StaticFields
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::paramCountMax
	int32_t ___paramCountMax_0;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::triggerRuleCountMax
	int32_t ___triggerRuleCountMax_1;

public:
	inline static int32_t get_offset_of_paramCountMax_0() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___paramCountMax_0)); }
	inline int32_t get_paramCountMax_0() const { return ___paramCountMax_0; }
	inline int32_t* get_address_of_paramCountMax_0() { return &___paramCountMax_0; }
	inline void set_paramCountMax_0(int32_t value)
	{
		___paramCountMax_0 = value;
	}

	inline static int32_t get_offset_of_triggerRuleCountMax_1() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___triggerRuleCountMax_1)); }
	inline int32_t get_triggerRuleCountMax_1() const { return ___triggerRuleCountMax_1; }
	inline int32_t* get_address_of_triggerRuleCountMax_1() { return &___triggerRuleCountMax_1; }
	inline void set_triggerRuleCountMax_1(int32_t value)
	{
		___triggerRuleCountMax_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifndef STANDARDEVENTPAYLOAD_T1629891255_H
#define STANDARDEVENTPAYLOAD_T1629891255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.StandardEventPayload
struct  StandardEventPayload_t1629891255  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.StandardEventPayload::m_IsEventExpanded
	bool ___m_IsEventExpanded_0;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_StandardEventType
	String_t* ___m_StandardEventType_1;
	// System.Type UnityEngine.Analytics.StandardEventPayload::standardEventType
	Type_t * ___standardEventType_2;
	// UnityEngine.Analytics.AnalyticsEventParamListContainer UnityEngine.Analytics.StandardEventPayload::m_Parameters
	AnalyticsEventParamListContainer_t587083383 * ___m_Parameters_3;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_Name
	String_t* ___m_Name_5;

public:
	inline static int32_t get_offset_of_m_IsEventExpanded_0() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_IsEventExpanded_0)); }
	inline bool get_m_IsEventExpanded_0() const { return ___m_IsEventExpanded_0; }
	inline bool* get_address_of_m_IsEventExpanded_0() { return &___m_IsEventExpanded_0; }
	inline void set_m_IsEventExpanded_0(bool value)
	{
		___m_IsEventExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_StandardEventType_1() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_StandardEventType_1)); }
	inline String_t* get_m_StandardEventType_1() const { return ___m_StandardEventType_1; }
	inline String_t** get_address_of_m_StandardEventType_1() { return &___m_StandardEventType_1; }
	inline void set_m_StandardEventType_1(String_t* value)
	{
		___m_StandardEventType_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StandardEventType_1), value);
	}

	inline static int32_t get_offset_of_standardEventType_2() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___standardEventType_2)); }
	inline Type_t * get_standardEventType_2() const { return ___standardEventType_2; }
	inline Type_t ** get_address_of_standardEventType_2() { return &___standardEventType_2; }
	inline void set_standardEventType_2(Type_t * value)
	{
		___standardEventType_2 = value;
		Il2CppCodeGenWriteBarrier((&___standardEventType_2), value);
	}

	inline static int32_t get_offset_of_m_Parameters_3() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Parameters_3)); }
	inline AnalyticsEventParamListContainer_t587083383 * get_m_Parameters_3() const { return ___m_Parameters_3; }
	inline AnalyticsEventParamListContainer_t587083383 ** get_address_of_m_Parameters_3() { return &___m_Parameters_3; }
	inline void set_m_Parameters_3(AnalyticsEventParamListContainer_t587083383 * value)
	{
		___m_Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_3), value);
	}

	inline static int32_t get_offset_of_m_Name_5() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Name_5)); }
	inline String_t* get_m_Name_5() const { return ___m_Name_5; }
	inline String_t** get_address_of_m_Name_5() { return &___m_Name_5; }
	inline void set_m_Name_5(String_t* value)
	{
		___m_Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_5), value);
	}
};

struct StandardEventPayload_t1629891255_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.StandardEventPayload::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_4;

public:
	inline static int32_t get_offset_of_m_EventData_4() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255_StaticFields, ___m_EventData_4)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_4() const { return ___m_EventData_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_4() { return &___m_EventData_4; }
	inline void set_m_EventData_4(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDEVENTPAYLOAD_T1629891255_H
#ifndef TRACKABLEPROPERTY_T3943537984_H
#define TRACKABLEPROPERTY_T3943537984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t3943537984  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t235857739 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t3943537984, ___m_Fields_1)); }
	inline List_1_t235857739 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t235857739 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t235857739 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T3943537984_H
#ifndef FIELDWITHTARGET_T3058750293_H
#define FIELDWITHTARGET_T3058750293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t3058750293  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t631007953 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_Target_1)); }
	inline Object_t631007953 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t631007953 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t631007953 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T3058750293_H
#ifndef TRACKABLEPROPERTYBASE_T2121532948_H
#define TRACKABLEPROPERTYBASE_T2121532948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackablePropertyBase
struct  TrackablePropertyBase_t2121532948  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.TrackablePropertyBase::m_Target
	Object_t631007953 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackablePropertyBase::m_Path
	String_t* ___m_Path_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Target_0)); }
	inline Object_t631007953 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t631007953 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t631007953 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTYBASE_T2121532948_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef ADFAILEDTOLOADEVENTARGS_T2855961722_H
#define ADFAILEDTOLOADEVENTARGS_T2855961722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdFailedToLoadEventArgs
struct  AdFailedToLoadEventArgs_t2855961722  : public EventArgs_t3591816995
{
public:
	// System.String GoogleMobileAds.Api.AdFailedToLoadEventArgs::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AdFailedToLoadEventArgs_t2855961722, ___U3CMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADFAILEDTOLOADEVENTARGS_T2855961722_H
#ifndef CUSTOMNATIVEEVENTARGS_T3359820132_H
#define CUSTOMNATIVEEVENTARGS_T3359820132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.CustomNativeEventArgs
struct  CustomNativeEventArgs_t3359820132  : public EventArgs_t3591816995
{
public:
	// GoogleMobileAds.Api.CustomNativeTemplateAd GoogleMobileAds.Api.CustomNativeEventArgs::<nativeAd>k__BackingField
	CustomNativeTemplateAd_t3403582769 * ___U3CnativeAdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnativeAdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CustomNativeEventArgs_t3359820132, ___U3CnativeAdU3Ek__BackingField_1)); }
	inline CustomNativeTemplateAd_t3403582769 * get_U3CnativeAdU3Ek__BackingField_1() const { return ___U3CnativeAdU3Ek__BackingField_1; }
	inline CustomNativeTemplateAd_t3403582769 ** get_address_of_U3CnativeAdU3Ek__BackingField_1() { return &___U3CnativeAdU3Ek__BackingField_1; }
	inline void set_U3CnativeAdU3Ek__BackingField_1(CustomNativeTemplateAd_t3403582769 * value)
	{
		___U3CnativeAdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnativeAdU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMNATIVEEVENTARGS_T3359820132_H
#ifndef REWARD_T3704020935_H
#define REWARD_T3704020935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.Reward
struct  Reward_t3704020935  : public EventArgs_t3591816995
{
public:
	// System.String GoogleMobileAds.Api.Reward::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_1;
	// System.Double GoogleMobileAds.Api.Reward::<Amount>k__BackingField
	double ___U3CAmountU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Reward_t3704020935, ___U3CTypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_1() const { return ___U3CTypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_1() { return &___U3CTypeU3Ek__BackingField_1; }
	inline void set_U3CTypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CAmountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Reward_t3704020935, ___U3CAmountU3Ek__BackingField_2)); }
	inline double get_U3CAmountU3Ek__BackingField_2() const { return ___U3CAmountU3Ek__BackingField_2; }
	inline double* get_address_of_U3CAmountU3Ek__BackingField_2() { return &___U3CAmountU3Ek__BackingField_2; }
	inline void set_U3CAmountU3Ek__BackingField_2(double value)
	{
		___U3CAmountU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARD_T3704020935_H
#ifndef NATIVEADTYPES_T3925888818_H
#define NATIVEADTYPES_T3925888818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.NativeAdTypes
struct  NativeAdTypes_t3925888818 
{
public:
	// System.Int32 GoogleMobileAds.iOS.NativeAdTypes::CustomTemplateAd
	int32_t ___CustomTemplateAd_0;

public:
	inline static int32_t get_offset_of_CustomTemplateAd_0() { return static_cast<int32_t>(offsetof(NativeAdTypes_t3925888818, ___CustomTemplateAd_0)); }
	inline int32_t get_CustomTemplateAd_0() const { return ___CustomTemplateAd_0; }
	inline int32_t* get_address_of_CustomTemplateAd_0() { return &___CustomTemplateAd_0; }
	inline void set_CustomTemplateAd_0(int32_t value)
	{
		___CustomTemplateAd_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEADTYPES_T3925888818_H
#ifndef MONOPINVOKECALLBACKATTRIBUTE_T3472581009_H
#define MONOPINVOKECALLBACKATTRIBUTE_T3472581009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoPInvokeCallbackAttribute
struct  MonoPInvokeCallbackAttribute_t3472581009  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOPINVOKECALLBACKATTRIBUTE_T3472581009_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T1819850047_H
#define NULLABLE_1_T1819850047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t1819850047 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1819850047_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t881159249  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t881159249  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t881159249  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t881159249  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_2)); }
	inline TimeSpan_t881159249  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t881159249 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t881159249  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef TRACKABLEFIELD_T1772682203_H
#define TRACKABLEFIELD_T1772682203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableField
struct  TrackableField_t1772682203  : public TrackablePropertyBase_t2121532948
{
public:
	// System.String[] UnityEngine.Analytics.TrackableField::m_ValidTypeNames
	StringU5BU5D_t1281789340* ___m_ValidTypeNames_2;
	// System.String UnityEngine.Analytics.TrackableField::m_Type
	String_t* ___m_Type_3;
	// System.String UnityEngine.Analytics.TrackableField::m_EnumType
	String_t* ___m_EnumType_4;

public:
	inline static int32_t get_offset_of_m_ValidTypeNames_2() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_ValidTypeNames_2)); }
	inline StringU5BU5D_t1281789340* get_m_ValidTypeNames_2() const { return ___m_ValidTypeNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ValidTypeNames_2() { return &___m_ValidTypeNames_2; }
	inline void set_m_ValidTypeNames_2(StringU5BU5D_t1281789340* value)
	{
		___m_ValidTypeNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidTypeNames_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_Type_3)); }
	inline String_t* get_m_Type_3() const { return ___m_Type_3; }
	inline String_t** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(String_t* value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEFIELD_T1772682203_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ADPOSITION_T3254734156_H
#define ADPOSITION_T3254734156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdPosition
struct  AdPosition_t3254734156 
{
public:
	// System.Int32 GoogleMobileAds.Api.AdPosition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AdPosition_t3254734156, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADPOSITION_T3254734156_H
#ifndef GENDER_T1633829762_H
#define GENDER_T1633829762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.Gender
struct  Gender_t1633829762 
{
public:
	// System.Int32 GoogleMobileAds.Api.Gender::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Gender_t1633829762, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENDER_T1633829762_H
#ifndef NATIVEADTYPE_T1759420966_H
#define NATIVEADTYPE_T1759420966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.NativeAdType
struct  NativeAdType_t1759420966 
{
public:
	// System.Int32 GoogleMobileAds.Api.NativeAdType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NativeAdType_t1759420966, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEADTYPE_T1759420966_H
#ifndef ADLOADERCLIENT_T2216398974_H
#define ADLOADERCLIENT_T2216398974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.AdLoaderClient
struct  AdLoaderClient_t2216398974  : public RuntimeObject
{
public:
	// System.IntPtr GoogleMobileAds.iOS.AdLoaderClient::adLoaderPtr
	intptr_t ___adLoaderPtr_0;
	// System.IntPtr GoogleMobileAds.iOS.AdLoaderClient::adLoaderClientPtr
	intptr_t ___adLoaderClientPtr_1;
	// GoogleMobileAds.iOS.NativeAdTypes GoogleMobileAds.iOS.AdLoaderClient::adTypes
	NativeAdTypes_t3925888818  ___adTypes_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String>> GoogleMobileAds.iOS.AdLoaderClient::customNativeTemplateCallbacks
	Dictionary_2_t856064493 * ___customNativeTemplateCallbacks_3;
	// System.EventHandler`1<GoogleMobileAds.Api.CustomNativeEventArgs> GoogleMobileAds.iOS.AdLoaderClient::OnCustomNativeTemplateAdLoaded
	EventHandler_1_t1283979565 * ___OnCustomNativeTemplateAdLoaded_4;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.iOS.AdLoaderClient::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_5;

public:
	inline static int32_t get_offset_of_adLoaderPtr_0() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974, ___adLoaderPtr_0)); }
	inline intptr_t get_adLoaderPtr_0() const { return ___adLoaderPtr_0; }
	inline intptr_t* get_address_of_adLoaderPtr_0() { return &___adLoaderPtr_0; }
	inline void set_adLoaderPtr_0(intptr_t value)
	{
		___adLoaderPtr_0 = value;
	}

	inline static int32_t get_offset_of_adLoaderClientPtr_1() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974, ___adLoaderClientPtr_1)); }
	inline intptr_t get_adLoaderClientPtr_1() const { return ___adLoaderClientPtr_1; }
	inline intptr_t* get_address_of_adLoaderClientPtr_1() { return &___adLoaderClientPtr_1; }
	inline void set_adLoaderClientPtr_1(intptr_t value)
	{
		___adLoaderClientPtr_1 = value;
	}

	inline static int32_t get_offset_of_adTypes_2() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974, ___adTypes_2)); }
	inline NativeAdTypes_t3925888818  get_adTypes_2() const { return ___adTypes_2; }
	inline NativeAdTypes_t3925888818 * get_address_of_adTypes_2() { return &___adTypes_2; }
	inline void set_adTypes_2(NativeAdTypes_t3925888818  value)
	{
		___adTypes_2 = value;
	}

	inline static int32_t get_offset_of_customNativeTemplateCallbacks_3() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974, ___customNativeTemplateCallbacks_3)); }
	inline Dictionary_2_t856064493 * get_customNativeTemplateCallbacks_3() const { return ___customNativeTemplateCallbacks_3; }
	inline Dictionary_2_t856064493 ** get_address_of_customNativeTemplateCallbacks_3() { return &___customNativeTemplateCallbacks_3; }
	inline void set_customNativeTemplateCallbacks_3(Dictionary_2_t856064493 * value)
	{
		___customNativeTemplateCallbacks_3 = value;
		Il2CppCodeGenWriteBarrier((&___customNativeTemplateCallbacks_3), value);
	}

	inline static int32_t get_offset_of_OnCustomNativeTemplateAdLoaded_4() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974, ___OnCustomNativeTemplateAdLoaded_4)); }
	inline EventHandler_1_t1283979565 * get_OnCustomNativeTemplateAdLoaded_4() const { return ___OnCustomNativeTemplateAdLoaded_4; }
	inline EventHandler_1_t1283979565 ** get_address_of_OnCustomNativeTemplateAdLoaded_4() { return &___OnCustomNativeTemplateAdLoaded_4; }
	inline void set_OnCustomNativeTemplateAdLoaded_4(EventHandler_1_t1283979565 * value)
	{
		___OnCustomNativeTemplateAdLoaded_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnCustomNativeTemplateAdLoaded_4), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_5() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974, ___OnAdFailedToLoad_5)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_5() const { return ___OnAdFailedToLoad_5; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_5() { return &___OnAdFailedToLoad_5; }
	inline void set_OnAdFailedToLoad_5(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_5), value);
	}
};

struct AdLoaderClient_t2216398974_StaticFields
{
public:
	// GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback GoogleMobileAds.iOS.AdLoaderClient::<>f__mg$cache0
	GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418 * ___U3CU3Ef__mgU24cache0_6;
	// GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidFailToReceiveAdWithErrorCallback GoogleMobileAds.iOS.AdLoaderClient::<>f__mg$cache1
	GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432 * ___U3CU3Ef__mgU24cache1_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_6() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974_StaticFields, ___U3CU3Ef__mgU24cache0_6)); }
	inline GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418 * get_U3CU3Ef__mgU24cache0_6() const { return ___U3CU3Ef__mgU24cache0_6; }
	inline GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418 ** get_address_of_U3CU3Ef__mgU24cache0_6() { return &___U3CU3Ef__mgU24cache0_6; }
	inline void set_U3CU3Ef__mgU24cache0_6(GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418 * value)
	{
		___U3CU3Ef__mgU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_7() { return static_cast<int32_t>(offsetof(AdLoaderClient_t2216398974_StaticFields, ___U3CU3Ef__mgU24cache1_7)); }
	inline GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432 * get_U3CU3Ef__mgU24cache1_7() const { return ___U3CU3Ef__mgU24cache1_7; }
	inline GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432 ** get_address_of_U3CU3Ef__mgU24cache1_7() { return &___U3CU3Ef__mgU24cache1_7; }
	inline void set_U3CU3Ef__mgU24cache1_7(GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432 * value)
	{
		___U3CU3Ef__mgU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLOADERCLIENT_T2216398974_H
#ifndef BANNERCLIENT_T2577994961_H
#define BANNERCLIENT_T2577994961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.BannerClient
struct  BannerClient_t2577994961  : public RuntimeObject
{
public:
	// System.IntPtr GoogleMobileAds.iOS.BannerClient::bannerViewPtr
	intptr_t ___bannerViewPtr_0;
	// System.IntPtr GoogleMobileAds.iOS.BannerClient::bannerClientPtr
	intptr_t ___bannerClientPtr_1;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.BannerClient::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.iOS.BannerClient::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.BannerClient::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.BannerClient::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.BannerClient::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_6;

public:
	inline static int32_t get_offset_of_bannerViewPtr_0() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___bannerViewPtr_0)); }
	inline intptr_t get_bannerViewPtr_0() const { return ___bannerViewPtr_0; }
	inline intptr_t* get_address_of_bannerViewPtr_0() { return &___bannerViewPtr_0; }
	inline void set_bannerViewPtr_0(intptr_t value)
	{
		___bannerViewPtr_0 = value;
	}

	inline static int32_t get_offset_of_bannerClientPtr_1() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___bannerClientPtr_1)); }
	inline intptr_t get_bannerClientPtr_1() const { return ___bannerClientPtr_1; }
	inline intptr_t* get_address_of_bannerClientPtr_1() { return &___bannerClientPtr_1; }
	inline void set_bannerClientPtr_1(intptr_t value)
	{
		___bannerClientPtr_1 = value;
	}

	inline static int32_t get_offset_of_OnAdLoaded_2() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___OnAdLoaded_2)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_2() const { return ___OnAdLoaded_2; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_2() { return &___OnAdLoaded_2; }
	inline void set_OnAdLoaded_2(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_3() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___OnAdFailedToLoad_3)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_3() const { return ___OnAdFailedToLoad_3; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_3() { return &___OnAdFailedToLoad_3; }
	inline void set_OnAdFailedToLoad_3(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_3), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_4() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___OnAdOpening_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_4() const { return ___OnAdOpening_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_4() { return &___OnAdOpening_4; }
	inline void set_OnAdOpening_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_4), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_5() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___OnAdClosed_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_5() const { return ___OnAdClosed_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_5() { return &___OnAdClosed_5; }
	inline void set_OnAdClosed_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_5), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_6() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961, ___OnAdLeavingApplication_6)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_6() const { return ___OnAdLeavingApplication_6; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_6() { return &___OnAdLeavingApplication_6; }
	inline void set_OnAdLeavingApplication_6(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_6), value);
	}
};

struct BannerClient_t2577994961_StaticFields
{
public:
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidReceiveAdCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache0
	GADUAdViewDidReceiveAdCallback_t2543294242 * ___U3CU3Ef__mgU24cache0_7;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidFailToReceiveAdWithErrorCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache1
	GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 * ___U3CU3Ef__mgU24cache1_8;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillPresentScreenCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache2
	GADUAdViewWillPresentScreenCallback_t2057580186 * ___U3CU3Ef__mgU24cache2_9;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidDismissScreenCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache3
	GADUAdViewDidDismissScreenCallback_t972393216 * ___U3CU3Ef__mgU24cache3_10;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillLeaveApplicationCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache4
	GADUAdViewWillLeaveApplicationCallback_t3323587265 * ___U3CU3Ef__mgU24cache4_11;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidReceiveAdCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache5
	GADUAdViewDidReceiveAdCallback_t2543294242 * ___U3CU3Ef__mgU24cache5_12;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidFailToReceiveAdWithErrorCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache6
	GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 * ___U3CU3Ef__mgU24cache6_13;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillPresentScreenCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache7
	GADUAdViewWillPresentScreenCallback_t2057580186 * ___U3CU3Ef__mgU24cache7_14;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidDismissScreenCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache8
	GADUAdViewDidDismissScreenCallback_t972393216 * ___U3CU3Ef__mgU24cache8_15;
	// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillLeaveApplicationCallback GoogleMobileAds.iOS.BannerClient::<>f__mg$cache9
	GADUAdViewWillLeaveApplicationCallback_t3323587265 * ___U3CU3Ef__mgU24cache9_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline GADUAdViewDidReceiveAdCallback_t2543294242 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline GADUAdViewDidReceiveAdCallback_t2543294242 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(GADUAdViewDidReceiveAdCallback_t2543294242 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_9() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache2_9)); }
	inline GADUAdViewWillPresentScreenCallback_t2057580186 * get_U3CU3Ef__mgU24cache2_9() const { return ___U3CU3Ef__mgU24cache2_9; }
	inline GADUAdViewWillPresentScreenCallback_t2057580186 ** get_address_of_U3CU3Ef__mgU24cache2_9() { return &___U3CU3Ef__mgU24cache2_9; }
	inline void set_U3CU3Ef__mgU24cache2_9(GADUAdViewWillPresentScreenCallback_t2057580186 * value)
	{
		___U3CU3Ef__mgU24cache2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_10() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache3_10)); }
	inline GADUAdViewDidDismissScreenCallback_t972393216 * get_U3CU3Ef__mgU24cache3_10() const { return ___U3CU3Ef__mgU24cache3_10; }
	inline GADUAdViewDidDismissScreenCallback_t972393216 ** get_address_of_U3CU3Ef__mgU24cache3_10() { return &___U3CU3Ef__mgU24cache3_10; }
	inline void set_U3CU3Ef__mgU24cache3_10(GADUAdViewDidDismissScreenCallback_t972393216 * value)
	{
		___U3CU3Ef__mgU24cache3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_11() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache4_11)); }
	inline GADUAdViewWillLeaveApplicationCallback_t3323587265 * get_U3CU3Ef__mgU24cache4_11() const { return ___U3CU3Ef__mgU24cache4_11; }
	inline GADUAdViewWillLeaveApplicationCallback_t3323587265 ** get_address_of_U3CU3Ef__mgU24cache4_11() { return &___U3CU3Ef__mgU24cache4_11; }
	inline void set_U3CU3Ef__mgU24cache4_11(GADUAdViewWillLeaveApplicationCallback_t3323587265 * value)
	{
		___U3CU3Ef__mgU24cache4_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_12() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache5_12)); }
	inline GADUAdViewDidReceiveAdCallback_t2543294242 * get_U3CU3Ef__mgU24cache5_12() const { return ___U3CU3Ef__mgU24cache5_12; }
	inline GADUAdViewDidReceiveAdCallback_t2543294242 ** get_address_of_U3CU3Ef__mgU24cache5_12() { return &___U3CU3Ef__mgU24cache5_12; }
	inline void set_U3CU3Ef__mgU24cache5_12(GADUAdViewDidReceiveAdCallback_t2543294242 * value)
	{
		___U3CU3Ef__mgU24cache5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_13() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache6_13)); }
	inline GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 * get_U3CU3Ef__mgU24cache6_13() const { return ___U3CU3Ef__mgU24cache6_13; }
	inline GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 ** get_address_of_U3CU3Ef__mgU24cache6_13() { return &___U3CU3Ef__mgU24cache6_13; }
	inline void set_U3CU3Ef__mgU24cache6_13(GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547 * value)
	{
		___U3CU3Ef__mgU24cache6_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_14() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache7_14)); }
	inline GADUAdViewWillPresentScreenCallback_t2057580186 * get_U3CU3Ef__mgU24cache7_14() const { return ___U3CU3Ef__mgU24cache7_14; }
	inline GADUAdViewWillPresentScreenCallback_t2057580186 ** get_address_of_U3CU3Ef__mgU24cache7_14() { return &___U3CU3Ef__mgU24cache7_14; }
	inline void set_U3CU3Ef__mgU24cache7_14(GADUAdViewWillPresentScreenCallback_t2057580186 * value)
	{
		___U3CU3Ef__mgU24cache7_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache8_15() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache8_15)); }
	inline GADUAdViewDidDismissScreenCallback_t972393216 * get_U3CU3Ef__mgU24cache8_15() const { return ___U3CU3Ef__mgU24cache8_15; }
	inline GADUAdViewDidDismissScreenCallback_t972393216 ** get_address_of_U3CU3Ef__mgU24cache8_15() { return &___U3CU3Ef__mgU24cache8_15; }
	inline void set_U3CU3Ef__mgU24cache8_15(GADUAdViewDidDismissScreenCallback_t972393216 * value)
	{
		___U3CU3Ef__mgU24cache8_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache8_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache9_16() { return static_cast<int32_t>(offsetof(BannerClient_t2577994961_StaticFields, ___U3CU3Ef__mgU24cache9_16)); }
	inline GADUAdViewWillLeaveApplicationCallback_t3323587265 * get_U3CU3Ef__mgU24cache9_16() const { return ___U3CU3Ef__mgU24cache9_16; }
	inline GADUAdViewWillLeaveApplicationCallback_t3323587265 ** get_address_of_U3CU3Ef__mgU24cache9_16() { return &___U3CU3Ef__mgU24cache9_16; }
	inline void set_U3CU3Ef__mgU24cache9_16(GADUAdViewWillLeaveApplicationCallback_t3323587265 * value)
	{
		___U3CU3Ef__mgU24cache9_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache9_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BANNERCLIENT_T2577994961_H
#ifndef CUSTOMNATIVETEMPLATECLIENT_T296756194_H
#define CUSTOMNATIVETEMPLATECLIENT_T296756194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.CustomNativeTemplateClient
struct  CustomNativeTemplateClient_t296756194  : public RuntimeObject
{
public:
	// System.IntPtr GoogleMobileAds.iOS.CustomNativeTemplateClient::customNativeAdPtr
	intptr_t ___customNativeAdPtr_0;
	// System.IntPtr GoogleMobileAds.iOS.CustomNativeTemplateClient::customNativeTemplateAdClientPtr
	intptr_t ___customNativeTemplateAdClientPtr_1;
	// System.Action`2<GoogleMobileAds.Api.CustomNativeTemplateAd,System.String> GoogleMobileAds.iOS.CustomNativeTemplateClient::clickHandler
	Action_2_t1070808194 * ___clickHandler_2;

public:
	inline static int32_t get_offset_of_customNativeAdPtr_0() { return static_cast<int32_t>(offsetof(CustomNativeTemplateClient_t296756194, ___customNativeAdPtr_0)); }
	inline intptr_t get_customNativeAdPtr_0() const { return ___customNativeAdPtr_0; }
	inline intptr_t* get_address_of_customNativeAdPtr_0() { return &___customNativeAdPtr_0; }
	inline void set_customNativeAdPtr_0(intptr_t value)
	{
		___customNativeAdPtr_0 = value;
	}

	inline static int32_t get_offset_of_customNativeTemplateAdClientPtr_1() { return static_cast<int32_t>(offsetof(CustomNativeTemplateClient_t296756194, ___customNativeTemplateAdClientPtr_1)); }
	inline intptr_t get_customNativeTemplateAdClientPtr_1() const { return ___customNativeTemplateAdClientPtr_1; }
	inline intptr_t* get_address_of_customNativeTemplateAdClientPtr_1() { return &___customNativeTemplateAdClientPtr_1; }
	inline void set_customNativeTemplateAdClientPtr_1(intptr_t value)
	{
		___customNativeTemplateAdClientPtr_1 = value;
	}

	inline static int32_t get_offset_of_clickHandler_2() { return static_cast<int32_t>(offsetof(CustomNativeTemplateClient_t296756194, ___clickHandler_2)); }
	inline Action_2_t1070808194 * get_clickHandler_2() const { return ___clickHandler_2; }
	inline Action_2_t1070808194 ** get_address_of_clickHandler_2() { return &___clickHandler_2; }
	inline void set_clickHandler_2(Action_2_t1070808194 * value)
	{
		___clickHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___clickHandler_2), value);
	}
};

struct CustomNativeTemplateClient_t296756194_StaticFields
{
public:
	// GoogleMobileAds.iOS.CustomNativeTemplateClient/GADUNativeCustomTemplateDidReceiveClick GoogleMobileAds.iOS.CustomNativeTemplateClient::<>f__mg$cache0
	GADUNativeCustomTemplateDidReceiveClick_t350204406 * ___U3CU3Ef__mgU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(CustomNativeTemplateClient_t296756194_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline GADUNativeCustomTemplateDidReceiveClick_t350204406 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline GADUNativeCustomTemplateDidReceiveClick_t350204406 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(GADUNativeCustomTemplateDidReceiveClick_t350204406 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMNATIVETEMPLATECLIENT_T296756194_H
#ifndef INTERSTITIALCLIENT_T301873194_H
#define INTERSTITIALCLIENT_T301873194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.InterstitialClient
struct  InterstitialClient_t301873194  : public RuntimeObject
{
public:
	// System.IntPtr GoogleMobileAds.iOS.InterstitialClient::interstitialPtr
	intptr_t ___interstitialPtr_0;
	// System.IntPtr GoogleMobileAds.iOS.InterstitialClient::interstitialClientPtr
	intptr_t ___interstitialClientPtr_1;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.InterstitialClient::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.iOS.InterstitialClient::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.InterstitialClient::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.InterstitialClient::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.InterstitialClient::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_6;

public:
	inline static int32_t get_offset_of_interstitialPtr_0() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___interstitialPtr_0)); }
	inline intptr_t get_interstitialPtr_0() const { return ___interstitialPtr_0; }
	inline intptr_t* get_address_of_interstitialPtr_0() { return &___interstitialPtr_0; }
	inline void set_interstitialPtr_0(intptr_t value)
	{
		___interstitialPtr_0 = value;
	}

	inline static int32_t get_offset_of_interstitialClientPtr_1() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___interstitialClientPtr_1)); }
	inline intptr_t get_interstitialClientPtr_1() const { return ___interstitialClientPtr_1; }
	inline intptr_t* get_address_of_interstitialClientPtr_1() { return &___interstitialClientPtr_1; }
	inline void set_interstitialClientPtr_1(intptr_t value)
	{
		___interstitialClientPtr_1 = value;
	}

	inline static int32_t get_offset_of_OnAdLoaded_2() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___OnAdLoaded_2)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_2() const { return ___OnAdLoaded_2; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_2() { return &___OnAdLoaded_2; }
	inline void set_OnAdLoaded_2(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_3() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___OnAdFailedToLoad_3)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_3() const { return ___OnAdFailedToLoad_3; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_3() { return &___OnAdFailedToLoad_3; }
	inline void set_OnAdFailedToLoad_3(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_3), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_4() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___OnAdOpening_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_4() const { return ___OnAdOpening_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_4() { return &___OnAdOpening_4; }
	inline void set_OnAdOpening_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_4), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_5() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___OnAdClosed_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_5() const { return ___OnAdClosed_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_5() { return &___OnAdClosed_5; }
	inline void set_OnAdClosed_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_5), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_6() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194, ___OnAdLeavingApplication_6)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_6() const { return ___OnAdLeavingApplication_6; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_6() { return &___OnAdLeavingApplication_6; }
	inline void set_OnAdLeavingApplication_6(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_6), value);
	}
};

struct InterstitialClient_t301873194_StaticFields
{
public:
	// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidReceiveAdCallback GoogleMobileAds.iOS.InterstitialClient::<>f__mg$cache0
	GADUInterstitialDidReceiveAdCallback_t821971233 * ___U3CU3Ef__mgU24cache0_7;
	// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidFailToReceiveAdWithErrorCallback GoogleMobileAds.iOS.InterstitialClient::<>f__mg$cache1
	GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714 * ___U3CU3Ef__mgU24cache1_8;
	// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillPresentScreenCallback GoogleMobileAds.iOS.InterstitialClient::<>f__mg$cache2
	GADUInterstitialWillPresentScreenCallback_t539653454 * ___U3CU3Ef__mgU24cache2_9;
	// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidDismissScreenCallback GoogleMobileAds.iOS.InterstitialClient::<>f__mg$cache3
	GADUInterstitialDidDismissScreenCallback_t1339081348 * ___U3CU3Ef__mgU24cache3_10;
	// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillLeaveApplicationCallback GoogleMobileAds.iOS.InterstitialClient::<>f__mg$cache4
	GADUInterstitialWillLeaveApplicationCallback_t1816935820 * ___U3CU3Ef__mgU24cache4_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline GADUInterstitialDidReceiveAdCallback_t821971233 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline GADUInterstitialDidReceiveAdCallback_t821971233 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(GADUInterstitialDidReceiveAdCallback_t821971233 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_8() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194_StaticFields, ___U3CU3Ef__mgU24cache1_8)); }
	inline GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714 * get_U3CU3Ef__mgU24cache1_8() const { return ___U3CU3Ef__mgU24cache1_8; }
	inline GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714 ** get_address_of_U3CU3Ef__mgU24cache1_8() { return &___U3CU3Ef__mgU24cache1_8; }
	inline void set_U3CU3Ef__mgU24cache1_8(GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714 * value)
	{
		___U3CU3Ef__mgU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_9() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194_StaticFields, ___U3CU3Ef__mgU24cache2_9)); }
	inline GADUInterstitialWillPresentScreenCallback_t539653454 * get_U3CU3Ef__mgU24cache2_9() const { return ___U3CU3Ef__mgU24cache2_9; }
	inline GADUInterstitialWillPresentScreenCallback_t539653454 ** get_address_of_U3CU3Ef__mgU24cache2_9() { return &___U3CU3Ef__mgU24cache2_9; }
	inline void set_U3CU3Ef__mgU24cache2_9(GADUInterstitialWillPresentScreenCallback_t539653454 * value)
	{
		___U3CU3Ef__mgU24cache2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_10() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194_StaticFields, ___U3CU3Ef__mgU24cache3_10)); }
	inline GADUInterstitialDidDismissScreenCallback_t1339081348 * get_U3CU3Ef__mgU24cache3_10() const { return ___U3CU3Ef__mgU24cache3_10; }
	inline GADUInterstitialDidDismissScreenCallback_t1339081348 ** get_address_of_U3CU3Ef__mgU24cache3_10() { return &___U3CU3Ef__mgU24cache3_10; }
	inline void set_U3CU3Ef__mgU24cache3_10(GADUInterstitialDidDismissScreenCallback_t1339081348 * value)
	{
		___U3CU3Ef__mgU24cache3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_11() { return static_cast<int32_t>(offsetof(InterstitialClient_t301873194_StaticFields, ___U3CU3Ef__mgU24cache4_11)); }
	inline GADUInterstitialWillLeaveApplicationCallback_t1816935820 * get_U3CU3Ef__mgU24cache4_11() const { return ___U3CU3Ef__mgU24cache4_11; }
	inline GADUInterstitialWillLeaveApplicationCallback_t1816935820 ** get_address_of_U3CU3Ef__mgU24cache4_11() { return &___U3CU3Ef__mgU24cache4_11; }
	inline void set_U3CU3Ef__mgU24cache4_11(GADUInterstitialWillLeaveApplicationCallback_t1816935820 * value)
	{
		___U3CU3Ef__mgU24cache4_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERSTITIALCLIENT_T301873194_H
#ifndef REWARDBASEDVIDEOADCLIENT_T745716004_H
#define REWARDBASEDVIDEOADCLIENT_T745716004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient
struct  RewardBasedVideoAdClient_t745716004  : public RuntimeObject
{
public:
	// System.IntPtr GoogleMobileAds.iOS.RewardBasedVideoAdClient::rewardBasedVideoAdPtr
	intptr_t ___rewardBasedVideoAdPtr_0;
	// System.IntPtr GoogleMobileAds.iOS.RewardBasedVideoAdClient::rewardBasedVideoAdClientPtr
	intptr_t ___rewardBasedVideoAdClientPtr_1;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdLoaded
	EventHandler_1_t1515976428 * ___OnAdLoaded_2;
	// System.EventHandler`1<GoogleMobileAds.Api.AdFailedToLoadEventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdFailedToLoad
	EventHandler_1_t780121155 * ___OnAdFailedToLoad_3;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdOpening
	EventHandler_1_t1515976428 * ___OnAdOpening_4;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdStarted
	EventHandler_1_t1515976428 * ___OnAdStarted_5;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdClosed
	EventHandler_1_t1515976428 * ___OnAdClosed_6;
	// System.EventHandler`1<GoogleMobileAds.Api.Reward> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdRewarded
	EventHandler_1_t1628180368 * ___OnAdRewarded_7;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdLeavingApplication
	EventHandler_1_t1515976428 * ___OnAdLeavingApplication_8;
	// System.EventHandler`1<System.EventArgs> GoogleMobileAds.iOS.RewardBasedVideoAdClient::OnAdCompleted
	EventHandler_1_t1515976428 * ___OnAdCompleted_9;

public:
	inline static int32_t get_offset_of_rewardBasedVideoAdPtr_0() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___rewardBasedVideoAdPtr_0)); }
	inline intptr_t get_rewardBasedVideoAdPtr_0() const { return ___rewardBasedVideoAdPtr_0; }
	inline intptr_t* get_address_of_rewardBasedVideoAdPtr_0() { return &___rewardBasedVideoAdPtr_0; }
	inline void set_rewardBasedVideoAdPtr_0(intptr_t value)
	{
		___rewardBasedVideoAdPtr_0 = value;
	}

	inline static int32_t get_offset_of_rewardBasedVideoAdClientPtr_1() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___rewardBasedVideoAdClientPtr_1)); }
	inline intptr_t get_rewardBasedVideoAdClientPtr_1() const { return ___rewardBasedVideoAdClientPtr_1; }
	inline intptr_t* get_address_of_rewardBasedVideoAdClientPtr_1() { return &___rewardBasedVideoAdClientPtr_1; }
	inline void set_rewardBasedVideoAdClientPtr_1(intptr_t value)
	{
		___rewardBasedVideoAdClientPtr_1 = value;
	}

	inline static int32_t get_offset_of_OnAdLoaded_2() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdLoaded_2)); }
	inline EventHandler_1_t1515976428 * get_OnAdLoaded_2() const { return ___OnAdLoaded_2; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLoaded_2() { return &___OnAdLoaded_2; }
	inline void set_OnAdLoaded_2(EventHandler_1_t1515976428 * value)
	{
		___OnAdLoaded_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLoaded_2), value);
	}

	inline static int32_t get_offset_of_OnAdFailedToLoad_3() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdFailedToLoad_3)); }
	inline EventHandler_1_t780121155 * get_OnAdFailedToLoad_3() const { return ___OnAdFailedToLoad_3; }
	inline EventHandler_1_t780121155 ** get_address_of_OnAdFailedToLoad_3() { return &___OnAdFailedToLoad_3; }
	inline void set_OnAdFailedToLoad_3(EventHandler_1_t780121155 * value)
	{
		___OnAdFailedToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdFailedToLoad_3), value);
	}

	inline static int32_t get_offset_of_OnAdOpening_4() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdOpening_4)); }
	inline EventHandler_1_t1515976428 * get_OnAdOpening_4() const { return ___OnAdOpening_4; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdOpening_4() { return &___OnAdOpening_4; }
	inline void set_OnAdOpening_4(EventHandler_1_t1515976428 * value)
	{
		___OnAdOpening_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdOpening_4), value);
	}

	inline static int32_t get_offset_of_OnAdStarted_5() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdStarted_5)); }
	inline EventHandler_1_t1515976428 * get_OnAdStarted_5() const { return ___OnAdStarted_5; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdStarted_5() { return &___OnAdStarted_5; }
	inline void set_OnAdStarted_5(EventHandler_1_t1515976428 * value)
	{
		___OnAdStarted_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdStarted_5), value);
	}

	inline static int32_t get_offset_of_OnAdClosed_6() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdClosed_6)); }
	inline EventHandler_1_t1515976428 * get_OnAdClosed_6() const { return ___OnAdClosed_6; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdClosed_6() { return &___OnAdClosed_6; }
	inline void set_OnAdClosed_6(EventHandler_1_t1515976428 * value)
	{
		___OnAdClosed_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdClosed_6), value);
	}

	inline static int32_t get_offset_of_OnAdRewarded_7() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdRewarded_7)); }
	inline EventHandler_1_t1628180368 * get_OnAdRewarded_7() const { return ___OnAdRewarded_7; }
	inline EventHandler_1_t1628180368 ** get_address_of_OnAdRewarded_7() { return &___OnAdRewarded_7; }
	inline void set_OnAdRewarded_7(EventHandler_1_t1628180368 * value)
	{
		___OnAdRewarded_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdRewarded_7), value);
	}

	inline static int32_t get_offset_of_OnAdLeavingApplication_8() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdLeavingApplication_8)); }
	inline EventHandler_1_t1515976428 * get_OnAdLeavingApplication_8() const { return ___OnAdLeavingApplication_8; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdLeavingApplication_8() { return &___OnAdLeavingApplication_8; }
	inline void set_OnAdLeavingApplication_8(EventHandler_1_t1515976428 * value)
	{
		___OnAdLeavingApplication_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdLeavingApplication_8), value);
	}

	inline static int32_t get_offset_of_OnAdCompleted_9() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004, ___OnAdCompleted_9)); }
	inline EventHandler_1_t1515976428 * get_OnAdCompleted_9() const { return ___OnAdCompleted_9; }
	inline EventHandler_1_t1515976428 ** get_address_of_OnAdCompleted_9() { return &___OnAdCompleted_9; }
	inline void set_OnAdCompleted_9(EventHandler_1_t1515976428 * value)
	{
		___OnAdCompleted_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnAdCompleted_9), value);
	}
};

struct RewardBasedVideoAdClient_t745716004_StaticFields
{
public:
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache0
	GADURewardBasedVideoAdDidReceiveAdCallback_t462486315 * ___U3CU3Ef__mgU24cache0_10;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache1
	GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788 * ___U3CU3Ef__mgU24cache1_11;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache2
	GADURewardBasedVideoAdDidOpenCallback_t3638490629 * ___U3CU3Ef__mgU24cache2_12;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache3
	GADURewardBasedVideoAdDidStartCallback_t2792276088 * ___U3CU3Ef__mgU24cache3_13;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache4
	GADURewardBasedVideoAdDidCloseCallback_t623082069 * ___U3CU3Ef__mgU24cache4_14;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache5
	GADURewardBasedVideoAdDidRewardCallback_t990863796 * ___U3CU3Ef__mgU24cache5_15;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache6
	GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531 * ___U3CU3Ef__mgU24cache6_16;
	// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCompleteCallback GoogleMobileAds.iOS.RewardBasedVideoAdClient::<>f__mg$cache7
	GADURewardBasedVideoAdDidCompleteCallback_t2076181 * ___U3CU3Ef__mgU24cache7_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_10() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache0_10)); }
	inline GADURewardBasedVideoAdDidReceiveAdCallback_t462486315 * get_U3CU3Ef__mgU24cache0_10() const { return ___U3CU3Ef__mgU24cache0_10; }
	inline GADURewardBasedVideoAdDidReceiveAdCallback_t462486315 ** get_address_of_U3CU3Ef__mgU24cache0_10() { return &___U3CU3Ef__mgU24cache0_10; }
	inline void set_U3CU3Ef__mgU24cache0_10(GADURewardBasedVideoAdDidReceiveAdCallback_t462486315 * value)
	{
		___U3CU3Ef__mgU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_11() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache1_11)); }
	inline GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788 * get_U3CU3Ef__mgU24cache1_11() const { return ___U3CU3Ef__mgU24cache1_11; }
	inline GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788 ** get_address_of_U3CU3Ef__mgU24cache1_11() { return &___U3CU3Ef__mgU24cache1_11; }
	inline void set_U3CU3Ef__mgU24cache1_11(GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788 * value)
	{
		___U3CU3Ef__mgU24cache1_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_12() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache2_12)); }
	inline GADURewardBasedVideoAdDidOpenCallback_t3638490629 * get_U3CU3Ef__mgU24cache2_12() const { return ___U3CU3Ef__mgU24cache2_12; }
	inline GADURewardBasedVideoAdDidOpenCallback_t3638490629 ** get_address_of_U3CU3Ef__mgU24cache2_12() { return &___U3CU3Ef__mgU24cache2_12; }
	inline void set_U3CU3Ef__mgU24cache2_12(GADURewardBasedVideoAdDidOpenCallback_t3638490629 * value)
	{
		___U3CU3Ef__mgU24cache2_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_13() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache3_13)); }
	inline GADURewardBasedVideoAdDidStartCallback_t2792276088 * get_U3CU3Ef__mgU24cache3_13() const { return ___U3CU3Ef__mgU24cache3_13; }
	inline GADURewardBasedVideoAdDidStartCallback_t2792276088 ** get_address_of_U3CU3Ef__mgU24cache3_13() { return &___U3CU3Ef__mgU24cache3_13; }
	inline void set_U3CU3Ef__mgU24cache3_13(GADURewardBasedVideoAdDidStartCallback_t2792276088 * value)
	{
		___U3CU3Ef__mgU24cache3_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_14() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache4_14)); }
	inline GADURewardBasedVideoAdDidCloseCallback_t623082069 * get_U3CU3Ef__mgU24cache4_14() const { return ___U3CU3Ef__mgU24cache4_14; }
	inline GADURewardBasedVideoAdDidCloseCallback_t623082069 ** get_address_of_U3CU3Ef__mgU24cache4_14() { return &___U3CU3Ef__mgU24cache4_14; }
	inline void set_U3CU3Ef__mgU24cache4_14(GADURewardBasedVideoAdDidCloseCallback_t623082069 * value)
	{
		___U3CU3Ef__mgU24cache4_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache4_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache5_15() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache5_15)); }
	inline GADURewardBasedVideoAdDidRewardCallback_t990863796 * get_U3CU3Ef__mgU24cache5_15() const { return ___U3CU3Ef__mgU24cache5_15; }
	inline GADURewardBasedVideoAdDidRewardCallback_t990863796 ** get_address_of_U3CU3Ef__mgU24cache5_15() { return &___U3CU3Ef__mgU24cache5_15; }
	inline void set_U3CU3Ef__mgU24cache5_15(GADURewardBasedVideoAdDidRewardCallback_t990863796 * value)
	{
		___U3CU3Ef__mgU24cache5_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache5_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache6_16() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache6_16)); }
	inline GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531 * get_U3CU3Ef__mgU24cache6_16() const { return ___U3CU3Ef__mgU24cache6_16; }
	inline GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531 ** get_address_of_U3CU3Ef__mgU24cache6_16() { return &___U3CU3Ef__mgU24cache6_16; }
	inline void set_U3CU3Ef__mgU24cache6_16(GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531 * value)
	{
		___U3CU3Ef__mgU24cache6_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache6_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache7_17() { return static_cast<int32_t>(offsetof(RewardBasedVideoAdClient_t745716004_StaticFields, ___U3CU3Ef__mgU24cache7_17)); }
	inline GADURewardBasedVideoAdDidCompleteCallback_t2076181 * get_U3CU3Ef__mgU24cache7_17() const { return ___U3CU3Ef__mgU24cache7_17; }
	inline GADURewardBasedVideoAdDidCompleteCallback_t2076181 ** get_address_of_U3CU3Ef__mgU24cache7_17() { return &___U3CU3Ef__mgU24cache7_17; }
	inline void set_U3CU3Ef__mgU24cache7_17(GADURewardBasedVideoAdDidCompleteCallback_t2076181 * value)
	{
		___U3CU3Ef__mgU24cache7_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache7_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDBASEDVIDEOADCLIENT_T745716004_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef REQUIREMENTTYPE_T3584265503_H
#define REQUIREMENTTYPE_T3584265503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam/RequirementType
struct  RequirementType_t3584265503 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventParam/RequirementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequirementType_t3584265503, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREMENTTYPE_T3584265503_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef PROPERTYTYPE_T4040930247_H
#define PROPERTYTYPE_T4040930247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty/PropertyType
struct  PropertyType_t4040930247 
{
public:
	// System.Int32 UnityEngine.Analytics.ValueProperty/PropertyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyType_t4040930247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T4040930247_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef GETSETATTRIBUTE_T1349027187_H
#define GETSETATTRIBUTE_T1349027187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GetSetAttribute
struct  GetSetAttribute_t1349027187  : public PropertyAttribute_t3677895545
{
public:
	// System.String UnityEngine.PostProcessing.GetSetAttribute::name
	String_t* ___name_0;
	// System.Boolean UnityEngine.PostProcessing.GetSetAttribute::dirty
	bool ___dirty_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GetSetAttribute_t1349027187, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_dirty_1() { return static_cast<int32_t>(offsetof(GetSetAttribute_t1349027187, ___dirty_1)); }
	inline bool get_dirty_1() const { return ___dirty_1; }
	inline bool* get_address_of_dirty_1() { return &___dirty_1; }
	inline void set_dirty_1(bool value)
	{
		___dirty_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSETATTRIBUTE_T1349027187_H
#ifndef MINATTRIBUTE_T4172004135_H
#define MINATTRIBUTE_T4172004135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MinAttribute
struct  MinAttribute_t4172004135  : public PropertyAttribute_t3677895545
{
public:
	// System.Single UnityEngine.PostProcessing.MinAttribute::min
	float ___min_0;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinAttribute_t4172004135, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINATTRIBUTE_T4172004135_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_0)); }
	inline TimeSpan_t881159249  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t881159249 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t881159249  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_2)); }
	inline DateTime_t3738529785  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t3738529785  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_3)); }
	inline DateTime_t3738529785  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t3738529785 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t3738529785  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_T3356391844_H
#define NULLABLE_1_T3356391844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<GoogleMobileAds.Api.Gender>
struct  Nullable_1_t3356391844 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3356391844, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3356391844, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3356391844_H
#ifndef ANALYTICSEVENTPARAM_T2480121928_H
#define ANALYTICSEVENTPARAM_T2480121928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam
struct  AnalyticsEventParam_t2480121928  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventParam/RequirementType UnityEngine.Analytics.AnalyticsEventParam::m_RequirementType
	int32_t ___m_RequirementType_0;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_GroupID
	String_t* ___m_GroupID_1;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Tooltip
	String_t* ___m_Tooltip_2;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Name
	String_t* ___m_Name_3;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.AnalyticsEventParam::m_Value
	ValueProperty_t1868393739 * ___m_Value_4;

public:
	inline static int32_t get_offset_of_m_RequirementType_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_RequirementType_0)); }
	inline int32_t get_m_RequirementType_0() const { return ___m_RequirementType_0; }
	inline int32_t* get_address_of_m_RequirementType_0() { return &___m_RequirementType_0; }
	inline void set_m_RequirementType_0(int32_t value)
	{
		___m_RequirementType_0 = value;
	}

	inline static int32_t get_offset_of_m_GroupID_1() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_GroupID_1)); }
	inline String_t* get_m_GroupID_1() const { return ___m_GroupID_1; }
	inline String_t** get_address_of_m_GroupID_1() { return &___m_GroupID_1; }
	inline void set_m_GroupID_1(String_t* value)
	{
		___m_GroupID_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GroupID_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}

	inline static int32_t get_offset_of_m_Name_3() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Name_3)); }
	inline String_t* get_m_Name_3() const { return ___m_Name_3; }
	inline String_t** get_address_of_m_Name_3() { return &___m_Name_3; }
	inline void set_m_Name_3(String_t* value)
	{
		___m_Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_3), value);
	}

	inline static int32_t get_offset_of_m_Value_4() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Value_4)); }
	inline ValueProperty_t1868393739 * get_m_Value_4() const { return ___m_Value_4; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_4() { return &___m_Value_4; }
	inline void set_m_Value_4(ValueProperty_t1868393739 * value)
	{
		___m_Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAM_T2480121928_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef VALUEPROPERTY_T1868393739_H
#define VALUEPROPERTY_T1868393739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty
struct  ValueProperty_t1868393739  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EditingCustomValue
	bool ___m_EditingCustomValue_0;
	// System.Int32 UnityEngine.Analytics.ValueProperty::m_PopupIndex
	int32_t ___m_PopupIndex_1;
	// System.String UnityEngine.Analytics.ValueProperty::m_CustomValue
	String_t* ___m_CustomValue_2;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_FixedType
	bool ___m_FixedType_3;
	// System.String UnityEngine.Analytics.ValueProperty::m_EnumType
	String_t* ___m_EnumType_4;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EnumTypeIsCustomizable
	bool ___m_EnumTypeIsCustomizable_5;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_CanDisable
	bool ___m_CanDisable_6;
	// UnityEngine.Analytics.ValueProperty/PropertyType UnityEngine.Analytics.ValueProperty::m_PropertyType
	int32_t ___m_PropertyType_7;
	// System.String UnityEngine.Analytics.ValueProperty::m_ValueType
	String_t* ___m_ValueType_8;
	// System.String UnityEngine.Analytics.ValueProperty::m_Value
	String_t* ___m_Value_9;
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.ValueProperty::m_Target
	TrackableField_t1772682203 * ___m_Target_10;

public:
	inline static int32_t get_offset_of_m_EditingCustomValue_0() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EditingCustomValue_0)); }
	inline bool get_m_EditingCustomValue_0() const { return ___m_EditingCustomValue_0; }
	inline bool* get_address_of_m_EditingCustomValue_0() { return &___m_EditingCustomValue_0; }
	inline void set_m_EditingCustomValue_0(bool value)
	{
		___m_EditingCustomValue_0 = value;
	}

	inline static int32_t get_offset_of_m_PopupIndex_1() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PopupIndex_1)); }
	inline int32_t get_m_PopupIndex_1() const { return ___m_PopupIndex_1; }
	inline int32_t* get_address_of_m_PopupIndex_1() { return &___m_PopupIndex_1; }
	inline void set_m_PopupIndex_1(int32_t value)
	{
		___m_PopupIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomValue_2() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CustomValue_2)); }
	inline String_t* get_m_CustomValue_2() const { return ___m_CustomValue_2; }
	inline String_t** get_address_of_m_CustomValue_2() { return &___m_CustomValue_2; }
	inline void set_m_CustomValue_2(String_t* value)
	{
		___m_CustomValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomValue_2), value);
	}

	inline static int32_t get_offset_of_m_FixedType_3() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_FixedType_3)); }
	inline bool get_m_FixedType_3() const { return ___m_FixedType_3; }
	inline bool* get_address_of_m_FixedType_3() { return &___m_FixedType_3; }
	inline void set_m_FixedType_3(bool value)
	{
		___m_FixedType_3 = value;
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}

	inline static int32_t get_offset_of_m_EnumTypeIsCustomizable_5() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumTypeIsCustomizable_5)); }
	inline bool get_m_EnumTypeIsCustomizable_5() const { return ___m_EnumTypeIsCustomizable_5; }
	inline bool* get_address_of_m_EnumTypeIsCustomizable_5() { return &___m_EnumTypeIsCustomizable_5; }
	inline void set_m_EnumTypeIsCustomizable_5(bool value)
	{
		___m_EnumTypeIsCustomizable_5 = value;
	}

	inline static int32_t get_offset_of_m_CanDisable_6() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CanDisable_6)); }
	inline bool get_m_CanDisable_6() const { return ___m_CanDisable_6; }
	inline bool* get_address_of_m_CanDisable_6() { return &___m_CanDisable_6; }
	inline void set_m_CanDisable_6(bool value)
	{
		___m_CanDisable_6 = value;
	}

	inline static int32_t get_offset_of_m_PropertyType_7() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PropertyType_7)); }
	inline int32_t get_m_PropertyType_7() const { return ___m_PropertyType_7; }
	inline int32_t* get_address_of_m_PropertyType_7() { return &___m_PropertyType_7; }
	inline void set_m_PropertyType_7(int32_t value)
	{
		___m_PropertyType_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueType_8() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_ValueType_8)); }
	inline String_t* get_m_ValueType_8() const { return ___m_ValueType_8; }
	inline String_t** get_address_of_m_ValueType_8() { return &___m_ValueType_8; }
	inline void set_m_ValueType_8(String_t* value)
	{
		___m_ValueType_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueType_8), value);
	}

	inline static int32_t get_offset_of_m_Value_9() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Value_9)); }
	inline String_t* get_m_Value_9() const { return ___m_Value_9; }
	inline String_t** get_address_of_m_Value_9() { return &___m_Value_9; }
	inline void set_m_Value_9(String_t* value)
	{
		___m_Value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_9), value);
	}

	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Target_10)); }
	inline TrackableField_t1772682203 * get_m_Target_10() const { return ___m_Target_10; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(TrackableField_t1772682203 * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROPERTY_T1868393739_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GADUADLOADERDIDFAILTORECEIVEADWITHERRORCALLBACK_T3488831432_H
#define GADUADLOADERDIDFAILTORECEIVEADWITHERRORCALLBACK_T3488831432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidFailToReceiveAdWithErrorCallback
struct  GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADLOADERDIDFAILTORECEIVEADWITHERRORCALLBACK_T3488831432_H
#ifndef GADUADLOADERDIDRECEIVENATIVECUSTOMTEMPLATEADCALLBACK_T2228922418_H
#define GADUADLOADERDIDRECEIVENATIVECUSTOMTEMPLATEADCALLBACK_T2228922418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.AdLoaderClient/GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback
struct  GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADLOADERDIDRECEIVENATIVECUSTOMTEMPLATEADCALLBACK_T2228922418_H
#ifndef GADUADVIEWDIDDISMISSSCREENCALLBACK_T972393216_H
#define GADUADVIEWDIDDISMISSSCREENCALLBACK_T972393216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidDismissScreenCallback
struct  GADUAdViewDidDismissScreenCallback_t972393216  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADVIEWDIDDISMISSSCREENCALLBACK_T972393216_H
#ifndef GADUADVIEWDIDFAILTORECEIVEADWITHERRORCALLBACK_T643467547_H
#define GADUADVIEWDIDFAILTORECEIVEADWITHERRORCALLBACK_T643467547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidFailToReceiveAdWithErrorCallback
struct  GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADVIEWDIDFAILTORECEIVEADWITHERRORCALLBACK_T643467547_H
#ifndef GADUADVIEWDIDRECEIVEADCALLBACK_T2543294242_H
#define GADUADVIEWDIDRECEIVEADCALLBACK_T2543294242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.BannerClient/GADUAdViewDidReceiveAdCallback
struct  GADUAdViewDidReceiveAdCallback_t2543294242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADVIEWDIDRECEIVEADCALLBACK_T2543294242_H
#ifndef GADUADVIEWWILLLEAVEAPPLICATIONCALLBACK_T3323587265_H
#define GADUADVIEWWILLLEAVEAPPLICATIONCALLBACK_T3323587265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillLeaveApplicationCallback
struct  GADUAdViewWillLeaveApplicationCallback_t3323587265  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADVIEWWILLLEAVEAPPLICATIONCALLBACK_T3323587265_H
#ifndef GADUADVIEWWILLPRESENTSCREENCALLBACK_T2057580186_H
#define GADUADVIEWWILLPRESENTSCREENCALLBACK_T2057580186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.BannerClient/GADUAdViewWillPresentScreenCallback
struct  GADUAdViewWillPresentScreenCallback_t2057580186  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUADVIEWWILLPRESENTSCREENCALLBACK_T2057580186_H
#ifndef GADUNATIVECUSTOMTEMPLATEDIDRECEIVECLICK_T350204406_H
#define GADUNATIVECUSTOMTEMPLATEDIDRECEIVECLICK_T350204406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.CustomNativeTemplateClient/GADUNativeCustomTemplateDidReceiveClick
struct  GADUNativeCustomTemplateDidReceiveClick_t350204406  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUNATIVECUSTOMTEMPLATEDIDRECEIVECLICK_T350204406_H
#ifndef GADUINTERSTITIALDIDDISMISSSCREENCALLBACK_T1339081348_H
#define GADUINTERSTITIALDIDDISMISSSCREENCALLBACK_T1339081348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidDismissScreenCallback
struct  GADUInterstitialDidDismissScreenCallback_t1339081348  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUINTERSTITIALDIDDISMISSSCREENCALLBACK_T1339081348_H
#ifndef GADUINTERSTITIALDIDFAILTORECEIVEADWITHERRORCALLBACK_T1323914714_H
#define GADUINTERSTITIALDIDFAILTORECEIVEADWITHERRORCALLBACK_T1323914714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidFailToReceiveAdWithErrorCallback
struct  GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUINTERSTITIALDIDFAILTORECEIVEADWITHERRORCALLBACK_T1323914714_H
#ifndef GADUINTERSTITIALDIDRECEIVEADCALLBACK_T821971233_H
#define GADUINTERSTITIALDIDRECEIVEADCALLBACK_T821971233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialDidReceiveAdCallback
struct  GADUInterstitialDidReceiveAdCallback_t821971233  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUINTERSTITIALDIDRECEIVEADCALLBACK_T821971233_H
#ifndef GADUINTERSTITIALWILLLEAVEAPPLICATIONCALLBACK_T1816935820_H
#define GADUINTERSTITIALWILLLEAVEAPPLICATIONCALLBACK_T1816935820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillLeaveApplicationCallback
struct  GADUInterstitialWillLeaveApplicationCallback_t1816935820  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUINTERSTITIALWILLLEAVEAPPLICATIONCALLBACK_T1816935820_H
#ifndef GADUINTERSTITIALWILLPRESENTSCREENCALLBACK_T539653454_H
#define GADUINTERSTITIALWILLPRESENTSCREENCALLBACK_T539653454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.InterstitialClient/GADUInterstitialWillPresentScreenCallback
struct  GADUInterstitialWillPresentScreenCallback_t539653454  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUINTERSTITIALWILLPRESENTSCREENCALLBACK_T539653454_H
#ifndef GADUREWARDBASEDVIDEOADDIDCLOSECALLBACK_T623082069_H
#define GADUREWARDBASEDVIDEOADDIDCLOSECALLBACK_T623082069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCloseCallback
struct  GADURewardBasedVideoAdDidCloseCallback_t623082069  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDCLOSECALLBACK_T623082069_H
#ifndef GADUREWARDBASEDVIDEOADDIDCOMPLETECALLBACK_T2076181_H
#define GADUREWARDBASEDVIDEOADDIDCOMPLETECALLBACK_T2076181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidCompleteCallback
struct  GADURewardBasedVideoAdDidCompleteCallback_t2076181  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDCOMPLETECALLBACK_T2076181_H
#ifndef GADUREWARDBASEDVIDEOADDIDFAILTORECEIVEADWITHERRORCALLBACK_T3979086788_H
#define GADUREWARDBASEDVIDEOADDIDFAILTORECEIVEADWITHERRORCALLBACK_T3979086788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback
struct  GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDFAILTORECEIVEADWITHERRORCALLBACK_T3979086788_H
#ifndef GADUREWARDBASEDVIDEOADDIDOPENCALLBACK_T3638490629_H
#define GADUREWARDBASEDVIDEOADDIDOPENCALLBACK_T3638490629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidOpenCallback
struct  GADURewardBasedVideoAdDidOpenCallback_t3638490629  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDOPENCALLBACK_T3638490629_H
#ifndef GADUREWARDBASEDVIDEOADDIDRECEIVEADCALLBACK_T462486315_H
#define GADUREWARDBASEDVIDEOADDIDRECEIVEADCALLBACK_T462486315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidReceiveAdCallback
struct  GADURewardBasedVideoAdDidReceiveAdCallback_t462486315  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDRECEIVEADCALLBACK_T462486315_H
#ifndef GADUREWARDBASEDVIDEOADDIDREWARDCALLBACK_T990863796_H
#define GADUREWARDBASEDVIDEOADDIDREWARDCALLBACK_T990863796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidRewardCallback
struct  GADURewardBasedVideoAdDidRewardCallback_t990863796  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDREWARDCALLBACK_T990863796_H
#ifndef GADUREWARDBASEDVIDEOADDIDSTARTCALLBACK_T2792276088_H
#define GADUREWARDBASEDVIDEOADDIDSTARTCALLBACK_T2792276088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdDidStartCallback
struct  GADURewardBasedVideoAdDidStartCallback_t2792276088  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADDIDSTARTCALLBACK_T2792276088_H
#ifndef GADUREWARDBASEDVIDEOADWILLLEAVEAPPLICATIONCALLBACK_T3217042531_H
#define GADUREWARDBASEDVIDEOADWILLLEAVEAPPLICATIONCALLBACK_T3217042531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.iOS.RewardBasedVideoAdClient/GADURewardBasedVideoAdWillLeaveApplicationCallback
struct  GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GADUREWARDBASEDVIDEOADWILLLEAVEAPPLICATIONCALLBACK_T3217042531_H
#ifndef NULLABLE_1_T1166124571_H
#define NULLABLE_1_T1166124571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t1166124571 
{
public:
	// T System.Nullable`1::value
	DateTime_t3738529785  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1166124571, ___value_0)); }
	inline DateTime_t3738529785  get_value_0() const { return ___value_0; }
	inline DateTime_t3738529785 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t3738529785  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1166124571, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1166124571_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ADREQUEST_T1573687800_H
#define ADREQUEST_T1573687800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdRequest
struct  AdRequest_t1573687800  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> GoogleMobileAds.Api.AdRequest::<TestDevices>k__BackingField
	List_1_t3319525431 * ___U3CTestDevicesU3Ek__BackingField_2;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdRequest::<Keywords>k__BackingField
	HashSet_1_t412400163 * ___U3CKeywordsU3Ek__BackingField_3;
	// System.Nullable`1<System.DateTime> GoogleMobileAds.Api.AdRequest::<Birthday>k__BackingField
	Nullable_1_t1166124571  ___U3CBirthdayU3Ek__BackingField_4;
	// System.Nullable`1<GoogleMobileAds.Api.Gender> GoogleMobileAds.Api.AdRequest::<Gender>k__BackingField
	Nullable_1_t3356391844  ___U3CGenderU3Ek__BackingField_5;
	// System.Nullable`1<System.Boolean> GoogleMobileAds.Api.AdRequest::<TagForChildDirectedTreatment>k__BackingField
	Nullable_1_t1819850047  ___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.AdRequest::<Extras>k__BackingField
	Dictionary_2_t1632706988 * ___U3CExtrasU3Ek__BackingField_7;
	// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras> GoogleMobileAds.Api.AdRequest::<MediationExtras>k__BackingField
	List_1_t3723909906 * ___U3CMediationExtrasU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CTestDevicesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CTestDevicesU3Ek__BackingField_2)); }
	inline List_1_t3319525431 * get_U3CTestDevicesU3Ek__BackingField_2() const { return ___U3CTestDevicesU3Ek__BackingField_2; }
	inline List_1_t3319525431 ** get_address_of_U3CTestDevicesU3Ek__BackingField_2() { return &___U3CTestDevicesU3Ek__BackingField_2; }
	inline void set_U3CTestDevicesU3Ek__BackingField_2(List_1_t3319525431 * value)
	{
		___U3CTestDevicesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTestDevicesU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CKeywordsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CKeywordsU3Ek__BackingField_3)); }
	inline HashSet_1_t412400163 * get_U3CKeywordsU3Ek__BackingField_3() const { return ___U3CKeywordsU3Ek__BackingField_3; }
	inline HashSet_1_t412400163 ** get_address_of_U3CKeywordsU3Ek__BackingField_3() { return &___U3CKeywordsU3Ek__BackingField_3; }
	inline void set_U3CKeywordsU3Ek__BackingField_3(HashSet_1_t412400163 * value)
	{
		___U3CKeywordsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeywordsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CBirthdayU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CBirthdayU3Ek__BackingField_4)); }
	inline Nullable_1_t1166124571  get_U3CBirthdayU3Ek__BackingField_4() const { return ___U3CBirthdayU3Ek__BackingField_4; }
	inline Nullable_1_t1166124571 * get_address_of_U3CBirthdayU3Ek__BackingField_4() { return &___U3CBirthdayU3Ek__BackingField_4; }
	inline void set_U3CBirthdayU3Ek__BackingField_4(Nullable_1_t1166124571  value)
	{
		___U3CBirthdayU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CGenderU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CGenderU3Ek__BackingField_5)); }
	inline Nullable_1_t3356391844  get_U3CGenderU3Ek__BackingField_5() const { return ___U3CGenderU3Ek__BackingField_5; }
	inline Nullable_1_t3356391844 * get_address_of_U3CGenderU3Ek__BackingField_5() { return &___U3CGenderU3Ek__BackingField_5; }
	inline void set_U3CGenderU3Ek__BackingField_5(Nullable_1_t3356391844  value)
	{
		___U3CGenderU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6)); }
	inline Nullable_1_t1819850047  get_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6() const { return ___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6; }
	inline Nullable_1_t1819850047 * get_address_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6() { return &___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6; }
	inline void set_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6(Nullable_1_t1819850047  value)
	{
		___U3CTagForChildDirectedTreatmentU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CExtrasU3Ek__BackingField_7)); }
	inline Dictionary_2_t1632706988 * get_U3CExtrasU3Ek__BackingField_7() const { return ___U3CExtrasU3Ek__BackingField_7; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CExtrasU3Ek__BackingField_7() { return &___U3CExtrasU3Ek__BackingField_7; }
	inline void set_U3CExtrasU3Ek__BackingField_7(Dictionary_2_t1632706988 * value)
	{
		___U3CExtrasU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtrasU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CMediationExtrasU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AdRequest_t1573687800, ___U3CMediationExtrasU3Ek__BackingField_8)); }
	inline List_1_t3723909906 * get_U3CMediationExtrasU3Ek__BackingField_8() const { return ___U3CMediationExtrasU3Ek__BackingField_8; }
	inline List_1_t3723909906 ** get_address_of_U3CMediationExtrasU3Ek__BackingField_8() { return &___U3CMediationExtrasU3Ek__BackingField_8; }
	inline void set_U3CMediationExtrasU3Ek__BackingField_8(List_1_t3723909906 * value)
	{
		___U3CMediationExtrasU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMediationExtrasU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADREQUEST_T1573687800_H
#ifndef BUILDER_T3490962960_H
#define BUILDER_T3490962960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Api.AdRequest/Builder
struct  Builder_t3490962960  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> GoogleMobileAds.Api.AdRequest/Builder::<TestDevices>k__BackingField
	List_1_t3319525431 * ___U3CTestDevicesU3Ek__BackingField_0;
	// System.Collections.Generic.HashSet`1<System.String> GoogleMobileAds.Api.AdRequest/Builder::<Keywords>k__BackingField
	HashSet_1_t412400163 * ___U3CKeywordsU3Ek__BackingField_1;
	// System.Nullable`1<System.DateTime> GoogleMobileAds.Api.AdRequest/Builder::<Birthday>k__BackingField
	Nullable_1_t1166124571  ___U3CBirthdayU3Ek__BackingField_2;
	// System.Nullable`1<GoogleMobileAds.Api.Gender> GoogleMobileAds.Api.AdRequest/Builder::<Gender>k__BackingField
	Nullable_1_t3356391844  ___U3CGenderU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> GoogleMobileAds.Api.AdRequest/Builder::<ChildDirectedTreatmentTag>k__BackingField
	Nullable_1_t1819850047  ___U3CChildDirectedTreatmentTagU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> GoogleMobileAds.Api.AdRequest/Builder::<Extras>k__BackingField
	Dictionary_2_t1632706988 * ___U3CExtrasU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<GoogleMobileAds.Api.Mediation.MediationExtras> GoogleMobileAds.Api.AdRequest/Builder::<MediationExtras>k__BackingField
	List_1_t3723909906 * ___U3CMediationExtrasU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTestDevicesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CTestDevicesU3Ek__BackingField_0)); }
	inline List_1_t3319525431 * get_U3CTestDevicesU3Ek__BackingField_0() const { return ___U3CTestDevicesU3Ek__BackingField_0; }
	inline List_1_t3319525431 ** get_address_of_U3CTestDevicesU3Ek__BackingField_0() { return &___U3CTestDevicesU3Ek__BackingField_0; }
	inline void set_U3CTestDevicesU3Ek__BackingField_0(List_1_t3319525431 * value)
	{
		___U3CTestDevicesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTestDevicesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CKeywordsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CKeywordsU3Ek__BackingField_1)); }
	inline HashSet_1_t412400163 * get_U3CKeywordsU3Ek__BackingField_1() const { return ___U3CKeywordsU3Ek__BackingField_1; }
	inline HashSet_1_t412400163 ** get_address_of_U3CKeywordsU3Ek__BackingField_1() { return &___U3CKeywordsU3Ek__BackingField_1; }
	inline void set_U3CKeywordsU3Ek__BackingField_1(HashSet_1_t412400163 * value)
	{
		___U3CKeywordsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeywordsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CBirthdayU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CBirthdayU3Ek__BackingField_2)); }
	inline Nullable_1_t1166124571  get_U3CBirthdayU3Ek__BackingField_2() const { return ___U3CBirthdayU3Ek__BackingField_2; }
	inline Nullable_1_t1166124571 * get_address_of_U3CBirthdayU3Ek__BackingField_2() { return &___U3CBirthdayU3Ek__BackingField_2; }
	inline void set_U3CBirthdayU3Ek__BackingField_2(Nullable_1_t1166124571  value)
	{
		___U3CBirthdayU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CGenderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CGenderU3Ek__BackingField_3)); }
	inline Nullable_1_t3356391844  get_U3CGenderU3Ek__BackingField_3() const { return ___U3CGenderU3Ek__BackingField_3; }
	inline Nullable_1_t3356391844 * get_address_of_U3CGenderU3Ek__BackingField_3() { return &___U3CGenderU3Ek__BackingField_3; }
	inline void set_U3CGenderU3Ek__BackingField_3(Nullable_1_t3356391844  value)
	{
		___U3CGenderU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CChildDirectedTreatmentTagU3Ek__BackingField_4)); }
	inline Nullable_1_t1819850047  get_U3CChildDirectedTreatmentTagU3Ek__BackingField_4() const { return ___U3CChildDirectedTreatmentTagU3Ek__BackingField_4; }
	inline Nullable_1_t1819850047 * get_address_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4() { return &___U3CChildDirectedTreatmentTagU3Ek__BackingField_4; }
	inline void set_U3CChildDirectedTreatmentTagU3Ek__BackingField_4(Nullable_1_t1819850047  value)
	{
		___U3CChildDirectedTreatmentTagU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CExtrasU3Ek__BackingField_5)); }
	inline Dictionary_2_t1632706988 * get_U3CExtrasU3Ek__BackingField_5() const { return ___U3CExtrasU3Ek__BackingField_5; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CExtrasU3Ek__BackingField_5() { return &___U3CExtrasU3Ek__BackingField_5; }
	inline void set_U3CExtrasU3Ek__BackingField_5(Dictionary_2_t1632706988 * value)
	{
		___U3CExtrasU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtrasU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CMediationExtrasU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Builder_t3490962960, ___U3CMediationExtrasU3Ek__BackingField_6)); }
	inline List_1_t3723909906 * get_U3CMediationExtrasU3Ek__BackingField_6() const { return ___U3CMediationExtrasU3Ek__BackingField_6; }
	inline List_1_t3723909906 ** get_address_of_U3CMediationExtrasU3Ek__BackingField_6() { return &___U3CMediationExtrasU3Ek__BackingField_6; }
	inline void set_U3CMediationExtrasU3Ek__BackingField_6(List_1_t3723909906 * value)
	{
		___U3CMediationExtrasU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMediationExtrasU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T3490962960_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ADSMANAGER_T4014920720_H
#define ADSMANAGER_T4014920720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdsManager
struct  AdsManager_t4014920720  : public MonoBehaviour_t3962482529
{
public:
	// GoogleMobileAds.Api.InterstitialAd AdsManager::interstitial
	InterstitialAd_t207380487 * ___interstitial_4;
	// GoogleMobileAds.Api.RewardBasedVideoAd AdsManager::rewardBasedVideo
	RewardBasedVideoAd_t2288675867 * ___rewardBasedVideo_5;

public:
	inline static int32_t get_offset_of_interstitial_4() { return static_cast<int32_t>(offsetof(AdsManager_t4014920720, ___interstitial_4)); }
	inline InterstitialAd_t207380487 * get_interstitial_4() const { return ___interstitial_4; }
	inline InterstitialAd_t207380487 ** get_address_of_interstitial_4() { return &___interstitial_4; }
	inline void set_interstitial_4(InterstitialAd_t207380487 * value)
	{
		___interstitial_4 = value;
		Il2CppCodeGenWriteBarrier((&___interstitial_4), value);
	}

	inline static int32_t get_offset_of_rewardBasedVideo_5() { return static_cast<int32_t>(offsetof(AdsManager_t4014920720, ___rewardBasedVideo_5)); }
	inline RewardBasedVideoAd_t2288675867 * get_rewardBasedVideo_5() const { return ___rewardBasedVideo_5; }
	inline RewardBasedVideoAd_t2288675867 ** get_address_of_rewardBasedVideo_5() { return &___rewardBasedVideo_5; }
	inline void set_rewardBasedVideo_5(RewardBasedVideoAd_t2288675867 * value)
	{
		___rewardBasedVideo_5 = value;
		Il2CppCodeGenWriteBarrier((&___rewardBasedVideo_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSMANAGER_T4014920720_H
#ifndef APPMANAGER_T4031413908_H
#define APPMANAGER_T4031413908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AppManager
struct  AppManager_t4031413908  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 AppManager::Level
	int32_t ___Level_4;
	// System.Int32 AppManager::Best
	int32_t ___Best_5;
	// System.Int32 AppManager::NumberOfFloors
	int32_t ___NumberOfFloors_6;
	// System.Int32 AppManager::MinNumOfZones
	int32_t ___MinNumOfZones_7;
	// System.Int32 AppManager::MaxNumOfZones
	int32_t ___MaxNumOfZones_8;
	// UnityEngine.GameObject AppManager::GameOverPanel
	GameObject_t1113636619 * ___GameOverPanel_9;
	// UnityEngine.GameObject AppManager::GameOverEffects
	GameObject_t1113636619 * ___GameOverEffects_10;
	// UnityEngine.GameObject AppManager::WinGamePanel
	GameObject_t1113636619 * ___WinGamePanel_11;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> AppManager::Floors
	List_1_t2585711361 * ___Floors_12;
	// UnityEngine.Camera AppManager::cam
	Camera_t4157153871 * ___cam_13;
	// UnityEngine.Transform AppManager::StartFloorPosition
	Transform_t3600365921 * ___StartFloorPosition_14;
	// UnityEngine.GameObject AppManager::UIPanel
	GameObject_t1113636619 * ___UIPanel_15;
	// UnityEngine.UI.Text AppManager::LevelTextBox
	Text_t1901882714 * ___LevelTextBox_16;
	// UnityEngine.UI.Text AppManager::BestTextBox
	Text_t1901882714 * ___BestTextBox_17;
	// UnityEngine.UI.Text AppManager::NextLevelTextBox
	Text_t1901882714 * ___NextLevelTextBox_18;
	// UnityEngine.AudioClip AppManager::SlipSound
	AudioClip_t3680889665 * ___SlipSound_19;
	// UnityEngine.AudioClip AppManager::BounceSound
	AudioClip_t3680889665 * ___BounceSound_20;
	// System.Collections.Generic.List`1<AppManager/Floor> AppManager::FloorPrefabs
	List_1_t2427226674 * ___FloorPrefabs_21;
	// UnityEngine.GameObject AppManager::startFloorPrefab
	GameObject_t1113636619 * ___startFloorPrefab_22;
	// UnityEngine.GameObject AppManager::endFloorPrefab
	GameObject_t1113636619 * ___endFloorPrefab_23;
	// UnityEngine.GameObject AppManager::floorZone
	GameObject_t1113636619 * ___floorZone_24;
	// System.Single AppManager::sumWeight
	float ___sumWeight_25;

public:
	inline static int32_t get_offset_of_Level_4() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___Level_4)); }
	inline int32_t get_Level_4() const { return ___Level_4; }
	inline int32_t* get_address_of_Level_4() { return &___Level_4; }
	inline void set_Level_4(int32_t value)
	{
		___Level_4 = value;
	}

	inline static int32_t get_offset_of_Best_5() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___Best_5)); }
	inline int32_t get_Best_5() const { return ___Best_5; }
	inline int32_t* get_address_of_Best_5() { return &___Best_5; }
	inline void set_Best_5(int32_t value)
	{
		___Best_5 = value;
	}

	inline static int32_t get_offset_of_NumberOfFloors_6() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___NumberOfFloors_6)); }
	inline int32_t get_NumberOfFloors_6() const { return ___NumberOfFloors_6; }
	inline int32_t* get_address_of_NumberOfFloors_6() { return &___NumberOfFloors_6; }
	inline void set_NumberOfFloors_6(int32_t value)
	{
		___NumberOfFloors_6 = value;
	}

	inline static int32_t get_offset_of_MinNumOfZones_7() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___MinNumOfZones_7)); }
	inline int32_t get_MinNumOfZones_7() const { return ___MinNumOfZones_7; }
	inline int32_t* get_address_of_MinNumOfZones_7() { return &___MinNumOfZones_7; }
	inline void set_MinNumOfZones_7(int32_t value)
	{
		___MinNumOfZones_7 = value;
	}

	inline static int32_t get_offset_of_MaxNumOfZones_8() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___MaxNumOfZones_8)); }
	inline int32_t get_MaxNumOfZones_8() const { return ___MaxNumOfZones_8; }
	inline int32_t* get_address_of_MaxNumOfZones_8() { return &___MaxNumOfZones_8; }
	inline void set_MaxNumOfZones_8(int32_t value)
	{
		___MaxNumOfZones_8 = value;
	}

	inline static int32_t get_offset_of_GameOverPanel_9() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___GameOverPanel_9)); }
	inline GameObject_t1113636619 * get_GameOverPanel_9() const { return ___GameOverPanel_9; }
	inline GameObject_t1113636619 ** get_address_of_GameOverPanel_9() { return &___GameOverPanel_9; }
	inline void set_GameOverPanel_9(GameObject_t1113636619 * value)
	{
		___GameOverPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___GameOverPanel_9), value);
	}

	inline static int32_t get_offset_of_GameOverEffects_10() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___GameOverEffects_10)); }
	inline GameObject_t1113636619 * get_GameOverEffects_10() const { return ___GameOverEffects_10; }
	inline GameObject_t1113636619 ** get_address_of_GameOverEffects_10() { return &___GameOverEffects_10; }
	inline void set_GameOverEffects_10(GameObject_t1113636619 * value)
	{
		___GameOverEffects_10 = value;
		Il2CppCodeGenWriteBarrier((&___GameOverEffects_10), value);
	}

	inline static int32_t get_offset_of_WinGamePanel_11() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___WinGamePanel_11)); }
	inline GameObject_t1113636619 * get_WinGamePanel_11() const { return ___WinGamePanel_11; }
	inline GameObject_t1113636619 ** get_address_of_WinGamePanel_11() { return &___WinGamePanel_11; }
	inline void set_WinGamePanel_11(GameObject_t1113636619 * value)
	{
		___WinGamePanel_11 = value;
		Il2CppCodeGenWriteBarrier((&___WinGamePanel_11), value);
	}

	inline static int32_t get_offset_of_Floors_12() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___Floors_12)); }
	inline List_1_t2585711361 * get_Floors_12() const { return ___Floors_12; }
	inline List_1_t2585711361 ** get_address_of_Floors_12() { return &___Floors_12; }
	inline void set_Floors_12(List_1_t2585711361 * value)
	{
		___Floors_12 = value;
		Il2CppCodeGenWriteBarrier((&___Floors_12), value);
	}

	inline static int32_t get_offset_of_cam_13() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___cam_13)); }
	inline Camera_t4157153871 * get_cam_13() const { return ___cam_13; }
	inline Camera_t4157153871 ** get_address_of_cam_13() { return &___cam_13; }
	inline void set_cam_13(Camera_t4157153871 * value)
	{
		___cam_13 = value;
		Il2CppCodeGenWriteBarrier((&___cam_13), value);
	}

	inline static int32_t get_offset_of_StartFloorPosition_14() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___StartFloorPosition_14)); }
	inline Transform_t3600365921 * get_StartFloorPosition_14() const { return ___StartFloorPosition_14; }
	inline Transform_t3600365921 ** get_address_of_StartFloorPosition_14() { return &___StartFloorPosition_14; }
	inline void set_StartFloorPosition_14(Transform_t3600365921 * value)
	{
		___StartFloorPosition_14 = value;
		Il2CppCodeGenWriteBarrier((&___StartFloorPosition_14), value);
	}

	inline static int32_t get_offset_of_UIPanel_15() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___UIPanel_15)); }
	inline GameObject_t1113636619 * get_UIPanel_15() const { return ___UIPanel_15; }
	inline GameObject_t1113636619 ** get_address_of_UIPanel_15() { return &___UIPanel_15; }
	inline void set_UIPanel_15(GameObject_t1113636619 * value)
	{
		___UIPanel_15 = value;
		Il2CppCodeGenWriteBarrier((&___UIPanel_15), value);
	}

	inline static int32_t get_offset_of_LevelTextBox_16() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___LevelTextBox_16)); }
	inline Text_t1901882714 * get_LevelTextBox_16() const { return ___LevelTextBox_16; }
	inline Text_t1901882714 ** get_address_of_LevelTextBox_16() { return &___LevelTextBox_16; }
	inline void set_LevelTextBox_16(Text_t1901882714 * value)
	{
		___LevelTextBox_16 = value;
		Il2CppCodeGenWriteBarrier((&___LevelTextBox_16), value);
	}

	inline static int32_t get_offset_of_BestTextBox_17() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___BestTextBox_17)); }
	inline Text_t1901882714 * get_BestTextBox_17() const { return ___BestTextBox_17; }
	inline Text_t1901882714 ** get_address_of_BestTextBox_17() { return &___BestTextBox_17; }
	inline void set_BestTextBox_17(Text_t1901882714 * value)
	{
		___BestTextBox_17 = value;
		Il2CppCodeGenWriteBarrier((&___BestTextBox_17), value);
	}

	inline static int32_t get_offset_of_NextLevelTextBox_18() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___NextLevelTextBox_18)); }
	inline Text_t1901882714 * get_NextLevelTextBox_18() const { return ___NextLevelTextBox_18; }
	inline Text_t1901882714 ** get_address_of_NextLevelTextBox_18() { return &___NextLevelTextBox_18; }
	inline void set_NextLevelTextBox_18(Text_t1901882714 * value)
	{
		___NextLevelTextBox_18 = value;
		Il2CppCodeGenWriteBarrier((&___NextLevelTextBox_18), value);
	}

	inline static int32_t get_offset_of_SlipSound_19() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___SlipSound_19)); }
	inline AudioClip_t3680889665 * get_SlipSound_19() const { return ___SlipSound_19; }
	inline AudioClip_t3680889665 ** get_address_of_SlipSound_19() { return &___SlipSound_19; }
	inline void set_SlipSound_19(AudioClip_t3680889665 * value)
	{
		___SlipSound_19 = value;
		Il2CppCodeGenWriteBarrier((&___SlipSound_19), value);
	}

	inline static int32_t get_offset_of_BounceSound_20() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___BounceSound_20)); }
	inline AudioClip_t3680889665 * get_BounceSound_20() const { return ___BounceSound_20; }
	inline AudioClip_t3680889665 ** get_address_of_BounceSound_20() { return &___BounceSound_20; }
	inline void set_BounceSound_20(AudioClip_t3680889665 * value)
	{
		___BounceSound_20 = value;
		Il2CppCodeGenWriteBarrier((&___BounceSound_20), value);
	}

	inline static int32_t get_offset_of_FloorPrefabs_21() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___FloorPrefabs_21)); }
	inline List_1_t2427226674 * get_FloorPrefabs_21() const { return ___FloorPrefabs_21; }
	inline List_1_t2427226674 ** get_address_of_FloorPrefabs_21() { return &___FloorPrefabs_21; }
	inline void set_FloorPrefabs_21(List_1_t2427226674 * value)
	{
		___FloorPrefabs_21 = value;
		Il2CppCodeGenWriteBarrier((&___FloorPrefabs_21), value);
	}

	inline static int32_t get_offset_of_startFloorPrefab_22() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___startFloorPrefab_22)); }
	inline GameObject_t1113636619 * get_startFloorPrefab_22() const { return ___startFloorPrefab_22; }
	inline GameObject_t1113636619 ** get_address_of_startFloorPrefab_22() { return &___startFloorPrefab_22; }
	inline void set_startFloorPrefab_22(GameObject_t1113636619 * value)
	{
		___startFloorPrefab_22 = value;
		Il2CppCodeGenWriteBarrier((&___startFloorPrefab_22), value);
	}

	inline static int32_t get_offset_of_endFloorPrefab_23() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___endFloorPrefab_23)); }
	inline GameObject_t1113636619 * get_endFloorPrefab_23() const { return ___endFloorPrefab_23; }
	inline GameObject_t1113636619 ** get_address_of_endFloorPrefab_23() { return &___endFloorPrefab_23; }
	inline void set_endFloorPrefab_23(GameObject_t1113636619 * value)
	{
		___endFloorPrefab_23 = value;
		Il2CppCodeGenWriteBarrier((&___endFloorPrefab_23), value);
	}

	inline static int32_t get_offset_of_floorZone_24() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___floorZone_24)); }
	inline GameObject_t1113636619 * get_floorZone_24() const { return ___floorZone_24; }
	inline GameObject_t1113636619 ** get_address_of_floorZone_24() { return &___floorZone_24; }
	inline void set_floorZone_24(GameObject_t1113636619 * value)
	{
		___floorZone_24 = value;
		Il2CppCodeGenWriteBarrier((&___floorZone_24), value);
	}

	inline static int32_t get_offset_of_sumWeight_25() { return static_cast<int32_t>(offsetof(AppManager_t4031413908, ___sumWeight_25)); }
	inline float get_sumWeight_25() const { return ___sumWeight_25; }
	inline float* get_address_of_sumWeight_25() { return &___sumWeight_25; }
	inline void set_sumWeight_25(float value)
	{
		___sumWeight_25 = value;
	}
};

struct AppManager_t4031413908_StaticFields
{
public:
	// AppManager AppManager::instance
	AppManager_t4031413908 * ___instance_26;

public:
	inline static int32_t get_offset_of_instance_26() { return static_cast<int32_t>(offsetof(AppManager_t4031413908_StaticFields, ___instance_26)); }
	inline AppManager_t4031413908 * get_instance_26() const { return ___instance_26; }
	inline AppManager_t4031413908 ** get_address_of_instance_26() { return &___instance_26; }
	inline void set_instance_26(AppManager_t4031413908 * value)
	{
		___instance_26 = value;
		Il2CppCodeGenWriteBarrier((&___instance_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPMANAGER_T4031413908_H
#ifndef BALLCONTROLLER_T2992829471_H
#define BALLCONTROLLER_T2992829471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallController
struct  BallController_t2992829471  : public MonoBehaviour_t3962482529
{
public:
	// System.Single BallController::velocity
	float ___velocity_4;
	// System.Boolean BallController::Combo_Mode
	bool ___Combo_Mode_5;
	// System.Single BallController::diffHeight
	float ___diffHeight_6;
	// System.Int32 BallController::floorComboCount
	int32_t ___floorComboCount_7;
	// System.Int32 BallController::MaxComboCount
	int32_t ___MaxComboCount_8;
	// System.Boolean BallController::ragdoll
	bool ___ragdoll_9;
	// UnityEngine.Material BallController::currentMaterial
	Material_t340375123 * ___currentMaterial_10;
	// UnityEngine.Material BallController::RedBallMaterial
	Material_t340375123 * ___RedBallMaterial_11;
	// UnityEngine.GameObject BallController::Trail
	GameObject_t1113636619 * ___Trail_12;
	// UnityEngine.GameObject BallController::Santa
	GameObject_t1113636619 * ___Santa_13;
	// UnityEngine.Color BallController::defaultTrailColor
	Color_t2555686324  ___defaultTrailColor_14;
	// UnityEngine.Color BallController::comboTrailColor
	Color_t2555686324  ___comboTrailColor_15;
	// UnityEngine.UI.Text BallController::combomodetext
	Text_t1901882714 * ___combomodetext_16;
	// UnityEngine.UI.Text BallController::combocounttext
	Text_t1901882714 * ___combocounttext_17;
	// UnityEngine.GameObject BallController::SnowSplash
	GameObject_t1113636619 * ___SnowSplash_18;
	// UnityEngine.Rigidbody BallController::rb
	Rigidbody_t3916780224 * ___rb_19;
	// System.Single BallController::storedVelocity
	float ___storedVelocity_20;
	// UnityEngine.Vector3 BallController::savedVelocity
	Vector3_t3722313464  ___savedVelocity_21;

public:
	inline static int32_t get_offset_of_velocity_4() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___velocity_4)); }
	inline float get_velocity_4() const { return ___velocity_4; }
	inline float* get_address_of_velocity_4() { return &___velocity_4; }
	inline void set_velocity_4(float value)
	{
		___velocity_4 = value;
	}

	inline static int32_t get_offset_of_Combo_Mode_5() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___Combo_Mode_5)); }
	inline bool get_Combo_Mode_5() const { return ___Combo_Mode_5; }
	inline bool* get_address_of_Combo_Mode_5() { return &___Combo_Mode_5; }
	inline void set_Combo_Mode_5(bool value)
	{
		___Combo_Mode_5 = value;
	}

	inline static int32_t get_offset_of_diffHeight_6() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___diffHeight_6)); }
	inline float get_diffHeight_6() const { return ___diffHeight_6; }
	inline float* get_address_of_diffHeight_6() { return &___diffHeight_6; }
	inline void set_diffHeight_6(float value)
	{
		___diffHeight_6 = value;
	}

	inline static int32_t get_offset_of_floorComboCount_7() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___floorComboCount_7)); }
	inline int32_t get_floorComboCount_7() const { return ___floorComboCount_7; }
	inline int32_t* get_address_of_floorComboCount_7() { return &___floorComboCount_7; }
	inline void set_floorComboCount_7(int32_t value)
	{
		___floorComboCount_7 = value;
	}

	inline static int32_t get_offset_of_MaxComboCount_8() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___MaxComboCount_8)); }
	inline int32_t get_MaxComboCount_8() const { return ___MaxComboCount_8; }
	inline int32_t* get_address_of_MaxComboCount_8() { return &___MaxComboCount_8; }
	inline void set_MaxComboCount_8(int32_t value)
	{
		___MaxComboCount_8 = value;
	}

	inline static int32_t get_offset_of_ragdoll_9() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___ragdoll_9)); }
	inline bool get_ragdoll_9() const { return ___ragdoll_9; }
	inline bool* get_address_of_ragdoll_9() { return &___ragdoll_9; }
	inline void set_ragdoll_9(bool value)
	{
		___ragdoll_9 = value;
	}

	inline static int32_t get_offset_of_currentMaterial_10() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___currentMaterial_10)); }
	inline Material_t340375123 * get_currentMaterial_10() const { return ___currentMaterial_10; }
	inline Material_t340375123 ** get_address_of_currentMaterial_10() { return &___currentMaterial_10; }
	inline void set_currentMaterial_10(Material_t340375123 * value)
	{
		___currentMaterial_10 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_10), value);
	}

	inline static int32_t get_offset_of_RedBallMaterial_11() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___RedBallMaterial_11)); }
	inline Material_t340375123 * get_RedBallMaterial_11() const { return ___RedBallMaterial_11; }
	inline Material_t340375123 ** get_address_of_RedBallMaterial_11() { return &___RedBallMaterial_11; }
	inline void set_RedBallMaterial_11(Material_t340375123 * value)
	{
		___RedBallMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&___RedBallMaterial_11), value);
	}

	inline static int32_t get_offset_of_Trail_12() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___Trail_12)); }
	inline GameObject_t1113636619 * get_Trail_12() const { return ___Trail_12; }
	inline GameObject_t1113636619 ** get_address_of_Trail_12() { return &___Trail_12; }
	inline void set_Trail_12(GameObject_t1113636619 * value)
	{
		___Trail_12 = value;
		Il2CppCodeGenWriteBarrier((&___Trail_12), value);
	}

	inline static int32_t get_offset_of_Santa_13() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___Santa_13)); }
	inline GameObject_t1113636619 * get_Santa_13() const { return ___Santa_13; }
	inline GameObject_t1113636619 ** get_address_of_Santa_13() { return &___Santa_13; }
	inline void set_Santa_13(GameObject_t1113636619 * value)
	{
		___Santa_13 = value;
		Il2CppCodeGenWriteBarrier((&___Santa_13), value);
	}

	inline static int32_t get_offset_of_defaultTrailColor_14() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___defaultTrailColor_14)); }
	inline Color_t2555686324  get_defaultTrailColor_14() const { return ___defaultTrailColor_14; }
	inline Color_t2555686324 * get_address_of_defaultTrailColor_14() { return &___defaultTrailColor_14; }
	inline void set_defaultTrailColor_14(Color_t2555686324  value)
	{
		___defaultTrailColor_14 = value;
	}

	inline static int32_t get_offset_of_comboTrailColor_15() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___comboTrailColor_15)); }
	inline Color_t2555686324  get_comboTrailColor_15() const { return ___comboTrailColor_15; }
	inline Color_t2555686324 * get_address_of_comboTrailColor_15() { return &___comboTrailColor_15; }
	inline void set_comboTrailColor_15(Color_t2555686324  value)
	{
		___comboTrailColor_15 = value;
	}

	inline static int32_t get_offset_of_combomodetext_16() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___combomodetext_16)); }
	inline Text_t1901882714 * get_combomodetext_16() const { return ___combomodetext_16; }
	inline Text_t1901882714 ** get_address_of_combomodetext_16() { return &___combomodetext_16; }
	inline void set_combomodetext_16(Text_t1901882714 * value)
	{
		___combomodetext_16 = value;
		Il2CppCodeGenWriteBarrier((&___combomodetext_16), value);
	}

	inline static int32_t get_offset_of_combocounttext_17() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___combocounttext_17)); }
	inline Text_t1901882714 * get_combocounttext_17() const { return ___combocounttext_17; }
	inline Text_t1901882714 ** get_address_of_combocounttext_17() { return &___combocounttext_17; }
	inline void set_combocounttext_17(Text_t1901882714 * value)
	{
		___combocounttext_17 = value;
		Il2CppCodeGenWriteBarrier((&___combocounttext_17), value);
	}

	inline static int32_t get_offset_of_SnowSplash_18() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___SnowSplash_18)); }
	inline GameObject_t1113636619 * get_SnowSplash_18() const { return ___SnowSplash_18; }
	inline GameObject_t1113636619 ** get_address_of_SnowSplash_18() { return &___SnowSplash_18; }
	inline void set_SnowSplash_18(GameObject_t1113636619 * value)
	{
		___SnowSplash_18 = value;
		Il2CppCodeGenWriteBarrier((&___SnowSplash_18), value);
	}

	inline static int32_t get_offset_of_rb_19() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___rb_19)); }
	inline Rigidbody_t3916780224 * get_rb_19() const { return ___rb_19; }
	inline Rigidbody_t3916780224 ** get_address_of_rb_19() { return &___rb_19; }
	inline void set_rb_19(Rigidbody_t3916780224 * value)
	{
		___rb_19 = value;
		Il2CppCodeGenWriteBarrier((&___rb_19), value);
	}

	inline static int32_t get_offset_of_storedVelocity_20() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___storedVelocity_20)); }
	inline float get_storedVelocity_20() const { return ___storedVelocity_20; }
	inline float* get_address_of_storedVelocity_20() { return &___storedVelocity_20; }
	inline void set_storedVelocity_20(float value)
	{
		___storedVelocity_20 = value;
	}

	inline static int32_t get_offset_of_savedVelocity_21() { return static_cast<int32_t>(offsetof(BallController_t2992829471, ___savedVelocity_21)); }
	inline Vector3_t3722313464  get_savedVelocity_21() const { return ___savedVelocity_21; }
	inline Vector3_t3722313464 * get_address_of_savedVelocity_21() { return &___savedVelocity_21; }
	inline void set_savedVelocity_21(Vector3_t3722313464  value)
	{
		___savedVelocity_21 = value;
	}
};

struct BallController_t2992829471_StaticFields
{
public:
	// BallController BallController::instance
	BallController_t2992829471 * ___instance_22;

public:
	inline static int32_t get_offset_of_instance_22() { return static_cast<int32_t>(offsetof(BallController_t2992829471_StaticFields, ___instance_22)); }
	inline BallController_t2992829471 * get_instance_22() const { return ___instance_22; }
	inline BallController_t2992829471 ** get_address_of_instance_22() { return &___instance_22; }
	inline void set_instance_22(BallController_t2992829471 * value)
	{
		___instance_22 = value;
		Il2CppCodeGenWriteBarrier((&___instance_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALLCONTROLLER_T2992829471_H
#ifndef CAMERACONTROLLER_T3346819214_H
#define CAMERACONTROLLER_T3346819214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraController
struct  CameraController_t3346819214  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform CameraController::TargetObject
	Transform_t3600365921 * ___TargetObject_4;
	// System.Single CameraController::followDistance
	float ___followDistance_5;
	// System.Single CameraController::followHeight
	float ___followHeight_6;
	// System.Boolean CameraController::smoothedFollow
	bool ___smoothedFollow_7;
	// System.Single CameraController::smoothSpeed
	float ___smoothSpeed_8;
	// System.Boolean CameraController::useFixedLookDirection
	bool ___useFixedLookDirection_9;
	// UnityEngine.Vector3 CameraController::fixedLookDirection
	Vector3_t3722313464  ___fixedLookDirection_10;

public:
	inline static int32_t get_offset_of_TargetObject_4() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___TargetObject_4)); }
	inline Transform_t3600365921 * get_TargetObject_4() const { return ___TargetObject_4; }
	inline Transform_t3600365921 ** get_address_of_TargetObject_4() { return &___TargetObject_4; }
	inline void set_TargetObject_4(Transform_t3600365921 * value)
	{
		___TargetObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___TargetObject_4), value);
	}

	inline static int32_t get_offset_of_followDistance_5() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___followDistance_5)); }
	inline float get_followDistance_5() const { return ___followDistance_5; }
	inline float* get_address_of_followDistance_5() { return &___followDistance_5; }
	inline void set_followDistance_5(float value)
	{
		___followDistance_5 = value;
	}

	inline static int32_t get_offset_of_followHeight_6() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___followHeight_6)); }
	inline float get_followHeight_6() const { return ___followHeight_6; }
	inline float* get_address_of_followHeight_6() { return &___followHeight_6; }
	inline void set_followHeight_6(float value)
	{
		___followHeight_6 = value;
	}

	inline static int32_t get_offset_of_smoothedFollow_7() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___smoothedFollow_7)); }
	inline bool get_smoothedFollow_7() const { return ___smoothedFollow_7; }
	inline bool* get_address_of_smoothedFollow_7() { return &___smoothedFollow_7; }
	inline void set_smoothedFollow_7(bool value)
	{
		___smoothedFollow_7 = value;
	}

	inline static int32_t get_offset_of_smoothSpeed_8() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___smoothSpeed_8)); }
	inline float get_smoothSpeed_8() const { return ___smoothSpeed_8; }
	inline float* get_address_of_smoothSpeed_8() { return &___smoothSpeed_8; }
	inline void set_smoothSpeed_8(float value)
	{
		___smoothSpeed_8 = value;
	}

	inline static int32_t get_offset_of_useFixedLookDirection_9() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___useFixedLookDirection_9)); }
	inline bool get_useFixedLookDirection_9() const { return ___useFixedLookDirection_9; }
	inline bool* get_address_of_useFixedLookDirection_9() { return &___useFixedLookDirection_9; }
	inline void set_useFixedLookDirection_9(bool value)
	{
		___useFixedLookDirection_9 = value;
	}

	inline static int32_t get_offset_of_fixedLookDirection_10() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___fixedLookDirection_10)); }
	inline Vector3_t3722313464  get_fixedLookDirection_10() const { return ___fixedLookDirection_10; }
	inline Vector3_t3722313464 * get_address_of_fixedLookDirection_10() { return &___fixedLookDirection_10; }
	inline void set_fixedLookDirection_10(Vector3_t3722313464  value)
	{
		___fixedLookDirection_10 = value;
	}
};

struct CameraController_t3346819214_StaticFields
{
public:
	// CameraController CameraController::instance
	CameraController_t3346819214 * ___instance_11;

public:
	inline static int32_t get_offset_of_instance_11() { return static_cast<int32_t>(offsetof(CameraController_t3346819214_StaticFields, ___instance_11)); }
	inline CameraController_t3346819214 * get_instance_11() const { return ___instance_11; }
	inline CameraController_t3346819214 ** get_address_of_instance_11() { return &___instance_11; }
	inline void set_instance_11(CameraController_t3346819214 * value)
	{
		___instance_11 = value;
		Il2CppCodeGenWriteBarrier((&___instance_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T3346819214_H
#ifndef EXPLOSIONFORCE_T3427080444_H
#define EXPLOSIONFORCE_T3427080444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExplosionForce
struct  ExplosionForce_t3427080444  : public MonoBehaviour_t3962482529
{
public:
	// System.Single ExplosionForce::radius
	float ___radius_4;
	// System.Single ExplosionForce::power
	float ___power_5;

public:
	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(ExplosionForce_t3427080444, ___radius_4)); }
	inline float get_radius_4() const { return ___radius_4; }
	inline float* get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(float value)
	{
		___radius_4 = value;
	}

	inline static int32_t get_offset_of_power_5() { return static_cast<int32_t>(offsetof(ExplosionForce_t3427080444, ___power_5)); }
	inline float get_power_5() const { return ___power_5; }
	inline float* get_address_of_power_5() { return &___power_5; }
	inline void set_power_5(float value)
	{
		___power_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPLOSIONFORCE_T3427080444_H
#ifndef FLOORCOLLIDER_T3259734113_H
#define FLOORCOLLIDER_T3259734113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloorCollider
struct  FloorCollider_t3259734113  : public MonoBehaviour_t3962482529
{
public:
	// BallController FloorCollider::parent
	BallController_t2992829471 * ___parent_4;
	// System.Boolean FloorCollider::sentinel
	bool ___sentinel_5;

public:
	inline static int32_t get_offset_of_parent_4() { return static_cast<int32_t>(offsetof(FloorCollider_t3259734113, ___parent_4)); }
	inline BallController_t2992829471 * get_parent_4() const { return ___parent_4; }
	inline BallController_t2992829471 ** get_address_of_parent_4() { return &___parent_4; }
	inline void set_parent_4(BallController_t2992829471 * value)
	{
		___parent_4 = value;
		Il2CppCodeGenWriteBarrier((&___parent_4), value);
	}

	inline static int32_t get_offset_of_sentinel_5() { return static_cast<int32_t>(offsetof(FloorCollider_t3259734113, ___sentinel_5)); }
	inline bool get_sentinel_5() const { return ___sentinel_5; }
	inline bool* get_address_of_sentinel_5() { return &___sentinel_5; }
	inline void set_sentinel_5(bool value)
	{
		___sentinel_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOORCOLLIDER_T3259734113_H
#ifndef FLOORTRIGGER_T2171280715_H
#define FLOORTRIGGER_T2171280715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloorTrigger
struct  FloorTrigger_t2171280715  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOORTRIGGER_T2171280715_H
#ifndef GAMECOUNTER_T1203613968_H
#define GAMECOUNTER_T1203613968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCounter
struct  GameCounter_t1203613968  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct GameCounter_t1203613968_StaticFields
{
public:
	// System.Int32 GameCounter::NumberOfGamesPlayed
	int32_t ___NumberOfGamesPlayed_4;

public:
	inline static int32_t get_offset_of_NumberOfGamesPlayed_4() { return static_cast<int32_t>(offsetof(GameCounter_t1203613968_StaticFields, ___NumberOfGamesPlayed_4)); }
	inline int32_t get_NumberOfGamesPlayed_4() const { return ___NumberOfGamesPlayed_4; }
	inline int32_t* get_address_of_NumberOfGamesPlayed_4() { return &___NumberOfGamesPlayed_4; }
	inline void set_NumberOfGamesPlayed_4(int32_t value)
	{
		___NumberOfGamesPlayed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECOUNTER_T1203613968_H
#ifndef MOBILEADSEVENTEXECUTOR_T1363665247_H
#define MOBILEADSEVENTEXECUTOR_T1363665247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMobileAds.Common.MobileAdsEventExecutor
struct  MobileAdsEventExecutor_t1363665247  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct MobileAdsEventExecutor_t1363665247_StaticFields
{
public:
	// GoogleMobileAds.Common.MobileAdsEventExecutor GoogleMobileAds.Common.MobileAdsEventExecutor::instance
	MobileAdsEventExecutor_t1363665247 * ___instance_4;
	// System.Collections.Generic.List`1<System.Action> GoogleMobileAds.Common.MobileAdsEventExecutor::adEventsQueue
	List_1_t2736452219 * ___adEventsQueue_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) GoogleMobileAds.Common.MobileAdsEventExecutor::adEventsQueueEmpty
	bool ___adEventsQueueEmpty_6;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(MobileAdsEventExecutor_t1363665247_StaticFields, ___instance_4)); }
	inline MobileAdsEventExecutor_t1363665247 * get_instance_4() const { return ___instance_4; }
	inline MobileAdsEventExecutor_t1363665247 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(MobileAdsEventExecutor_t1363665247 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_adEventsQueue_5() { return static_cast<int32_t>(offsetof(MobileAdsEventExecutor_t1363665247_StaticFields, ___adEventsQueue_5)); }
	inline List_1_t2736452219 * get_adEventsQueue_5() const { return ___adEventsQueue_5; }
	inline List_1_t2736452219 ** get_address_of_adEventsQueue_5() { return &___adEventsQueue_5; }
	inline void set_adEventsQueue_5(List_1_t2736452219 * value)
	{
		___adEventsQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&___adEventsQueue_5), value);
	}

	inline static int32_t get_offset_of_adEventsQueueEmpty_6() { return static_cast<int32_t>(offsetof(MobileAdsEventExecutor_t1363665247_StaticFields, ___adEventsQueueEmpty_6)); }
	inline bool get_adEventsQueueEmpty_6() const { return ___adEventsQueueEmpty_6; }
	inline bool* get_address_of_adEventsQueueEmpty_6() { return &___adEventsQueueEmpty_6; }
	inline void set_adEventsQueueEmpty_6(bool value)
	{
		___adEventsQueueEmpty_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEADSEVENTEXECUTOR_T1363665247_H
#ifndef LEVELCONTROLLER_T24418946_H
#define LEVELCONTROLLER_T24418946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelController
struct  LevelController_t24418946  : public MonoBehaviour_t3962482529
{
public:
	// System.Single LevelController::f_lastX
	float ___f_lastX_4;
	// System.Single LevelController::f_difX
	float ___f_difX_5;
	// System.Single LevelController::mult
	float ___mult_6;
	// System.Single LevelController::multAnd
	float ___multAnd_7;

public:
	inline static int32_t get_offset_of_f_lastX_4() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___f_lastX_4)); }
	inline float get_f_lastX_4() const { return ___f_lastX_4; }
	inline float* get_address_of_f_lastX_4() { return &___f_lastX_4; }
	inline void set_f_lastX_4(float value)
	{
		___f_lastX_4 = value;
	}

	inline static int32_t get_offset_of_f_difX_5() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___f_difX_5)); }
	inline float get_f_difX_5() const { return ___f_difX_5; }
	inline float* get_address_of_f_difX_5() { return &___f_difX_5; }
	inline void set_f_difX_5(float value)
	{
		___f_difX_5 = value;
	}

	inline static int32_t get_offset_of_mult_6() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___mult_6)); }
	inline float get_mult_6() const { return ___mult_6; }
	inline float* get_address_of_mult_6() { return &___mult_6; }
	inline void set_mult_6(float value)
	{
		___mult_6 = value;
	}

	inline static int32_t get_offset_of_multAnd_7() { return static_cast<int32_t>(offsetof(LevelController_t24418946, ___multAnd_7)); }
	inline float get_multAnd_7() const { return ___multAnd_7; }
	inline float* get_address_of_multAnd_7() { return &___multAnd_7; }
	inline void set_multAnd_7(float value)
	{
		___multAnd_7 = value;
	}
};

struct LevelController_t24418946_StaticFields
{
public:
	// LevelController LevelController::instance
	LevelController_t24418946 * ___instance_8;

public:
	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(LevelController_t24418946_StaticFields, ___instance_8)); }
	inline LevelController_t24418946 * get_instance_8() const { return ___instance_8; }
	inline LevelController_t24418946 ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(LevelController_t24418946 * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___instance_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCONTROLLER_T24418946_H
#ifndef RAGDOLL_T3598225712_H
#define RAGDOLL_T3598225712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ragdoll
struct  Ragdoll_t3598225712  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Ragdoll::move
	bool ___move_4;
	// UnityEngine.Rigidbody Ragdoll::rigidBody
	Rigidbody_t3916780224 * ___rigidBody_5;
	// System.Single Ragdoll::radius
	float ___radius_6;
	// System.Single Ragdoll::power
	float ___power_7;

public:
	inline static int32_t get_offset_of_move_4() { return static_cast<int32_t>(offsetof(Ragdoll_t3598225712, ___move_4)); }
	inline bool get_move_4() const { return ___move_4; }
	inline bool* get_address_of_move_4() { return &___move_4; }
	inline void set_move_4(bool value)
	{
		___move_4 = value;
	}

	inline static int32_t get_offset_of_rigidBody_5() { return static_cast<int32_t>(offsetof(Ragdoll_t3598225712, ___rigidBody_5)); }
	inline Rigidbody_t3916780224 * get_rigidBody_5() const { return ___rigidBody_5; }
	inline Rigidbody_t3916780224 ** get_address_of_rigidBody_5() { return &___rigidBody_5; }
	inline void set_rigidBody_5(Rigidbody_t3916780224 * value)
	{
		___rigidBody_5 = value;
		Il2CppCodeGenWriteBarrier((&___rigidBody_5), value);
	}

	inline static int32_t get_offset_of_radius_6() { return static_cast<int32_t>(offsetof(Ragdoll_t3598225712, ___radius_6)); }
	inline float get_radius_6() const { return ___radius_6; }
	inline float* get_address_of_radius_6() { return &___radius_6; }
	inline void set_radius_6(float value)
	{
		___radius_6 = value;
	}

	inline static int32_t get_offset_of_power_7() { return static_cast<int32_t>(offsetof(Ragdoll_t3598225712, ___power_7)); }
	inline float get_power_7() const { return ___power_7; }
	inline float* get_address_of_power_7() { return &___power_7; }
	inline void set_power_7(float value)
	{
		___power_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAGDOLL_T3598225712_H
#ifndef ANALYTICSEVENTTRACKER_T2285229262_H
#define ANALYTICSEVENTTRACKER_T2285229262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker
struct  AnalyticsEventTracker_t2285229262  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Analytics.EventTrigger UnityEngine.Analytics.AnalyticsEventTracker::m_Trigger
	EventTrigger_t2527451695 * ___m_Trigger_4;
	// UnityEngine.Analytics.StandardEventPayload UnityEngine.Analytics.AnalyticsEventTracker::m_EventPayload
	StandardEventPayload_t1629891255 * ___m_EventPayload_5;

public:
	inline static int32_t get_offset_of_m_Trigger_4() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_Trigger_4)); }
	inline EventTrigger_t2527451695 * get_m_Trigger_4() const { return ___m_Trigger_4; }
	inline EventTrigger_t2527451695 ** get_address_of_m_Trigger_4() { return &___m_Trigger_4; }
	inline void set_m_Trigger_4(EventTrigger_t2527451695 * value)
	{
		___m_Trigger_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Trigger_4), value);
	}

	inline static int32_t get_offset_of_m_EventPayload_5() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_EventPayload_5)); }
	inline StandardEventPayload_t1629891255 * get_m_EventPayload_5() const { return ___m_EventPayload_5; }
	inline StandardEventPayload_t1629891255 ** get_address_of_m_EventPayload_5() { return &___m_EventPayload_5; }
	inline void set_m_EventPayload_5(StandardEventPayload_t1629891255 * value)
	{
		___m_EventPayload_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventPayload_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKER_T2285229262_H
#ifndef ANALYTICSTRACKER_T731021378_H
#define ANALYTICSTRACKER_T731021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t731021378  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t2865362463 * ___m_Dict_5;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_6;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t3943537984 * ___m_TrackableProperty_7;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_8;

public:
	inline static int32_t get_offset_of_m_EventName_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_EventName_4)); }
	inline String_t* get_m_EventName_4() const { return ___m_EventName_4; }
	inline String_t** get_address_of_m_EventName_4() { return &___m_EventName_4; }
	inline void set_m_EventName_4(String_t* value)
	{
		___m_EventName_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_4), value);
	}

	inline static int32_t get_offset_of_m_Dict_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Dict_5)); }
	inline Dictionary_2_t2865362463 * get_m_Dict_5() const { return ___m_Dict_5; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_Dict_5() { return &___m_Dict_5; }
	inline void set_m_Dict_5(Dictionary_2_t2865362463 * value)
	{
		___m_Dict_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_5), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_PrevDictHash_6)); }
	inline int32_t get_m_PrevDictHash_6() const { return ___m_PrevDictHash_6; }
	inline int32_t* get_address_of_m_PrevDictHash_6() { return &___m_PrevDictHash_6; }
	inline void set_m_PrevDictHash_6(int32_t value)
	{
		___m_PrevDictHash_6 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_7() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_TrackableProperty_7)); }
	inline TrackableProperty_t3943537984 * get_m_TrackableProperty_7() const { return ___m_TrackableProperty_7; }
	inline TrackableProperty_t3943537984 ** get_address_of_m_TrackableProperty_7() { return &___m_TrackableProperty_7; }
	inline void set_m_TrackableProperty_7(TrackableProperty_t3943537984 * value)
	{
		___m_TrackableProperty_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_7), value);
	}

	inline static int32_t get_offset_of_m_Trigger_8() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Trigger_8)); }
	inline int32_t get_m_Trigger_8() const { return ___m_Trigger_8; }
	inline int32_t* get_address_of_m_Trigger_8() { return &___m_Trigger_8; }
	inline void set_m_Trigger_8(int32_t value)
	{
		___m_Trigger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T731021378_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (U3CModuleU3E_t692745546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (AnalyticsEventTracker_t2285229262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[2] = 
{
	AnalyticsEventTracker_t2285229262::get_offset_of_m_Trigger_4(),
	AnalyticsEventTracker_t2285229262::get_offset_of_m_EventPayload_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (U3CTimedTriggerU3Ec__Iterator0_t3813435494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[4] = 
{
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24this_0(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24current_1(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24disposing_2(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (AnalyticsEventTrackerSettings_t480422680), -1, sizeof(AnalyticsEventTrackerSettings_t480422680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2103[2] = 
{
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_paramCountMax_0(),
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_triggerRuleCountMax_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (AnalyticsEventParam_t2480121928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[5] = 
{
	AnalyticsEventParam_t2480121928::get_offset_of_m_RequirementType_0(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_GroupID_1(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Tooltip_2(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Name_3(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (RequirementType_t3584265503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2105[4] = 
{
	RequirementType_t3584265503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (AnalyticsEventParamListContainer_t587083383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[1] = 
{
	AnalyticsEventParamListContainer_t587083383::get_offset_of_m_Parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (StandardEventPayload_t1629891255), -1, sizeof(StandardEventPayload_t1629891255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2107[6] = 
{
	StandardEventPayload_t1629891255::get_offset_of_m_IsEventExpanded_0(),
	StandardEventPayload_t1629891255::get_offset_of_m_StandardEventType_1(),
	StandardEventPayload_t1629891255::get_offset_of_standardEventType_2(),
	StandardEventPayload_t1629891255::get_offset_of_m_Parameters_3(),
	StandardEventPayload_t1629891255_StaticFields::get_offset_of_m_EventData_4(),
	StandardEventPayload_t1629891255::get_offset_of_m_Name_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (TrackableField_t1772682203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[3] = 
{
	TrackableField_t1772682203::get_offset_of_m_ValidTypeNames_2(),
	TrackableField_t1772682203::get_offset_of_m_Type_3(),
	TrackableField_t1772682203::get_offset_of_m_EnumType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (TrackablePropertyBase_t2121532948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[2] = 
{
	TrackablePropertyBase_t2121532948::get_offset_of_m_Target_0(),
	TrackablePropertyBase_t2121532948::get_offset_of_m_Path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (ValueProperty_t1868393739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[11] = 
{
	ValueProperty_t1868393739::get_offset_of_m_EditingCustomValue_0(),
	ValueProperty_t1868393739::get_offset_of_m_PopupIndex_1(),
	ValueProperty_t1868393739::get_offset_of_m_CustomValue_2(),
	ValueProperty_t1868393739::get_offset_of_m_FixedType_3(),
	ValueProperty_t1868393739::get_offset_of_m_EnumType_4(),
	ValueProperty_t1868393739::get_offset_of_m_EnumTypeIsCustomizable_5(),
	ValueProperty_t1868393739::get_offset_of_m_CanDisable_6(),
	ValueProperty_t1868393739::get_offset_of_m_PropertyType_7(),
	ValueProperty_t1868393739::get_offset_of_m_ValueType_8(),
	ValueProperty_t1868393739::get_offset_of_m_Value_9(),
	ValueProperty_t1868393739::get_offset_of_m_Target_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (PropertyType_t4040930247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2111[4] = 
{
	PropertyType_t4040930247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (AnalyticsTracker_t731021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[5] = 
{
	AnalyticsTracker_t731021378::get_offset_of_m_EventName_4(),
	AnalyticsTracker_t731021378::get_offset_of_m_Dict_5(),
	AnalyticsTracker_t731021378::get_offset_of_m_PrevDictHash_6(),
	AnalyticsTracker_t731021378::get_offset_of_m_TrackableProperty_7(),
	AnalyticsTracker_t731021378::get_offset_of_m_Trigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (Trigger_t4199345191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2113[8] = 
{
	Trigger_t4199345191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (TrackableProperty_t3943537984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[2] = 
{
	0,
	TrackableProperty_t3943537984::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (FieldWithTarget_t3058750293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[6] = 
{
	FieldWithTarget_t3058750293::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t3058750293::get_offset_of_m_Target_1(),
	FieldWithTarget_t3058750293::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t3058750293::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t3058750293::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t3058750293::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2116[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2117[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2118[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2119[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (U3CModuleU3E_t692745547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (AppManager_t4031413908), -1, sizeof(AppManager_t4031413908_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2127[23] = 
{
	AppManager_t4031413908::get_offset_of_Level_4(),
	AppManager_t4031413908::get_offset_of_Best_5(),
	AppManager_t4031413908::get_offset_of_NumberOfFloors_6(),
	AppManager_t4031413908::get_offset_of_MinNumOfZones_7(),
	AppManager_t4031413908::get_offset_of_MaxNumOfZones_8(),
	AppManager_t4031413908::get_offset_of_GameOverPanel_9(),
	AppManager_t4031413908::get_offset_of_GameOverEffects_10(),
	AppManager_t4031413908::get_offset_of_WinGamePanel_11(),
	AppManager_t4031413908::get_offset_of_Floors_12(),
	AppManager_t4031413908::get_offset_of_cam_13(),
	AppManager_t4031413908::get_offset_of_StartFloorPosition_14(),
	AppManager_t4031413908::get_offset_of_UIPanel_15(),
	AppManager_t4031413908::get_offset_of_LevelTextBox_16(),
	AppManager_t4031413908::get_offset_of_BestTextBox_17(),
	AppManager_t4031413908::get_offset_of_NextLevelTextBox_18(),
	AppManager_t4031413908::get_offset_of_SlipSound_19(),
	AppManager_t4031413908::get_offset_of_BounceSound_20(),
	AppManager_t4031413908::get_offset_of_FloorPrefabs_21(),
	AppManager_t4031413908::get_offset_of_startFloorPrefab_22(),
	AppManager_t4031413908::get_offset_of_endFloorPrefab_23(),
	AppManager_t4031413908::get_offset_of_floorZone_24(),
	AppManager_t4031413908::get_offset_of_sumWeight_25(),
	AppManager_t4031413908_StaticFields::get_offset_of_instance_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (Floor_t955151932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[3] = 
{
	Floor_t955151932::get_offset_of_prefab_0(),
	Floor_t955151932::get_offset_of_weight_1(),
	Floor_t955151932::get_offset_of_sumWeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (U3CEndGameU3Ec__Iterator0_t76120099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[4] = 
{
	U3CEndGameU3Ec__Iterator0_t76120099::get_offset_of_U24this_0(),
	U3CEndGameU3Ec__Iterator0_t76120099::get_offset_of_U24current_1(),
	U3CEndGameU3Ec__Iterator0_t76120099::get_offset_of_U24disposing_2(),
	U3CEndGameU3Ec__Iterator0_t76120099::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (BallController_t2992829471), -1, sizeof(BallController_t2992829471_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2130[19] = 
{
	BallController_t2992829471::get_offset_of_velocity_4(),
	BallController_t2992829471::get_offset_of_Combo_Mode_5(),
	BallController_t2992829471::get_offset_of_diffHeight_6(),
	BallController_t2992829471::get_offset_of_floorComboCount_7(),
	BallController_t2992829471::get_offset_of_MaxComboCount_8(),
	BallController_t2992829471::get_offset_of_ragdoll_9(),
	BallController_t2992829471::get_offset_of_currentMaterial_10(),
	BallController_t2992829471::get_offset_of_RedBallMaterial_11(),
	BallController_t2992829471::get_offset_of_Trail_12(),
	BallController_t2992829471::get_offset_of_Santa_13(),
	BallController_t2992829471::get_offset_of_defaultTrailColor_14(),
	BallController_t2992829471::get_offset_of_comboTrailColor_15(),
	BallController_t2992829471::get_offset_of_combomodetext_16(),
	BallController_t2992829471::get_offset_of_combocounttext_17(),
	BallController_t2992829471::get_offset_of_SnowSplash_18(),
	BallController_t2992829471::get_offset_of_rb_19(),
	BallController_t2992829471::get_offset_of_storedVelocity_20(),
	BallController_t2992829471::get_offset_of_savedVelocity_21(),
	BallController_t2992829471_StaticFields::get_offset_of_instance_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (CameraController_t3346819214), -1, sizeof(CameraController_t3346819214_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2131[8] = 
{
	CameraController_t3346819214::get_offset_of_TargetObject_4(),
	CameraController_t3346819214::get_offset_of_followDistance_5(),
	CameraController_t3346819214::get_offset_of_followHeight_6(),
	CameraController_t3346819214::get_offset_of_smoothedFollow_7(),
	CameraController_t3346819214::get_offset_of_smoothSpeed_8(),
	CameraController_t3346819214::get_offset_of_useFixedLookDirection_9(),
	CameraController_t3346819214::get_offset_of_fixedLookDirection_10(),
	CameraController_t3346819214_StaticFields::get_offset_of_instance_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (ExplosionForce_t3427080444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[2] = 
{
	ExplosionForce_t3427080444::get_offset_of_radius_4(),
	ExplosionForce_t3427080444::get_offset_of_power_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (FloorCollider_t3259734113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[2] = 
{
	FloorCollider_t3259734113::get_offset_of_parent_4(),
	FloorCollider_t3259734113::get_offset_of_sentinel_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (FloorTrigger_t2171280715), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (GameCounter_t1203613968), -1, sizeof(GameCounter_t1203613968_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2135[1] = 
{
	GameCounter_t1203613968_StaticFields::get_offset_of_NumberOfGamesPlayed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (LevelController_t24418946), -1, sizeof(LevelController_t24418946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2136[5] = 
{
	LevelController_t24418946::get_offset_of_f_lastX_4(),
	LevelController_t24418946::get_offset_of_f_difX_5(),
	LevelController_t24418946::get_offset_of_mult_6(),
	LevelController_t24418946::get_offset_of_multAnd_7(),
	LevelController_t24418946_StaticFields::get_offset_of_instance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (Ragdoll_t3598225712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[4] = 
{
	Ragdoll_t3598225712::get_offset_of_move_4(),
	Ragdoll_t3598225712::get_offset_of_rigidBody_5(),
	Ragdoll_t3598225712::get_offset_of_radius_6(),
	Ragdoll_t3598225712::get_offset_of_power_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (U3CExplodeSelfU3Ec__Iterator0_t2434711746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[7] = 
{
	U3CExplodeSelfU3Ec__Iterator0_t2434711746::get_offset_of_U24locvar0_0(),
	U3CExplodeSelfU3Ec__Iterator0_t2434711746::get_offset_of_U24locvar1_1(),
	U3CExplodeSelfU3Ec__Iterator0_t2434711746::get_offset_of_explode_2(),
	U3CExplodeSelfU3Ec__Iterator0_t2434711746::get_offset_of_U24this_3(),
	U3CExplodeSelfU3Ec__Iterator0_t2434711746::get_offset_of_U24current_4(),
	U3CExplodeSelfU3Ec__Iterator0_t2434711746::get_offset_of_U24disposing_5(),
	U3CExplodeSelfU3Ec__Iterator0_t2434711746::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (AdsManager_t4014920720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[2] = 
{
	AdsManager_t4014920720::get_offset_of_interstitial_4(),
	AdsManager_t4014920720::get_offset_of_rewardBasedVideo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (AdFailedToLoadEventArgs_t2855961722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[1] = 
{
	AdFailedToLoadEventArgs_t2855961722::get_offset_of_U3CMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (NativeAdType_t1759420966)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2141[2] = 
{
	NativeAdType_t1759420966::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (AdLoader_t3255226653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[7] = 
{
	AdLoader_t3255226653::get_offset_of_adLoaderClient_0(),
	AdLoader_t3255226653::get_offset_of_OnAdFailedToLoad_1(),
	AdLoader_t3255226653::get_offset_of_OnCustomNativeTemplateAdLoaded_2(),
	AdLoader_t3255226653::get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(),
	AdLoader_t3255226653::get_offset_of_U3CAdUnitIdU3Ek__BackingField_4(),
	AdLoader_t3255226653::get_offset_of_U3CAdTypesU3Ek__BackingField_5(),
	AdLoader_t3255226653::get_offset_of_U3CTemplateIdsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (Builder_t2638244817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[4] = 
{
	Builder_t2638244817::get_offset_of_U3CAdUnitIdU3Ek__BackingField_0(),
	Builder_t2638244817::get_offset_of_U3CAdTypesU3Ek__BackingField_1(),
	Builder_t2638244817::get_offset_of_U3CTemplateIdsU3Ek__BackingField_2(),
	Builder_t2638244817::get_offset_of_U3CCustomNativeTemplateClickHandlersU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (AdPosition_t3254734156)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2144[8] = 
{
	AdPosition_t3254734156::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (AdRequest_t1573687800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[9] = 
{
	0,
	0,
	AdRequest_t1573687800::get_offset_of_U3CTestDevicesU3Ek__BackingField_2(),
	AdRequest_t1573687800::get_offset_of_U3CKeywordsU3Ek__BackingField_3(),
	AdRequest_t1573687800::get_offset_of_U3CBirthdayU3Ek__BackingField_4(),
	AdRequest_t1573687800::get_offset_of_U3CGenderU3Ek__BackingField_5(),
	AdRequest_t1573687800::get_offset_of_U3CTagForChildDirectedTreatmentU3Ek__BackingField_6(),
	AdRequest_t1573687800::get_offset_of_U3CExtrasU3Ek__BackingField_7(),
	AdRequest_t1573687800::get_offset_of_U3CMediationExtrasU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (Builder_t3490962960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[7] = 
{
	Builder_t3490962960::get_offset_of_U3CTestDevicesU3Ek__BackingField_0(),
	Builder_t3490962960::get_offset_of_U3CKeywordsU3Ek__BackingField_1(),
	Builder_t3490962960::get_offset_of_U3CBirthdayU3Ek__BackingField_2(),
	Builder_t3490962960::get_offset_of_U3CGenderU3Ek__BackingField_3(),
	Builder_t3490962960::get_offset_of_U3CChildDirectedTreatmentTagU3Ek__BackingField_4(),
	Builder_t3490962960::get_offset_of_U3CExtrasU3Ek__BackingField_5(),
	Builder_t3490962960::get_offset_of_U3CMediationExtrasU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (AdSize_t2717023072), -1, sizeof(AdSize_t2717023072_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2147[9] = 
{
	AdSize_t2717023072::get_offset_of_isSmartBanner_0(),
	AdSize_t2717023072::get_offset_of_width_1(),
	AdSize_t2717023072::get_offset_of_height_2(),
	AdSize_t2717023072_StaticFields::get_offset_of_Banner_3(),
	AdSize_t2717023072_StaticFields::get_offset_of_MediumRectangle_4(),
	AdSize_t2717023072_StaticFields::get_offset_of_IABBanner_5(),
	AdSize_t2717023072_StaticFields::get_offset_of_Leaderboard_6(),
	AdSize_t2717023072_StaticFields::get_offset_of_SmartBanner_7(),
	AdSize_t2717023072_StaticFields::get_offset_of_FullWidth_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (BannerView_t2480907735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[6] = 
{
	BannerView_t2480907735::get_offset_of_client_0(),
	BannerView_t2480907735::get_offset_of_OnAdLoaded_1(),
	BannerView_t2480907735::get_offset_of_OnAdFailedToLoad_2(),
	BannerView_t2480907735::get_offset_of_OnAdOpening_3(),
	BannerView_t2480907735::get_offset_of_OnAdClosed_4(),
	BannerView_t2480907735::get_offset_of_OnAdLeavingApplication_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (CustomNativeEventArgs_t3359820132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[1] = 
{
	CustomNativeEventArgs_t3359820132::get_offset_of_U3CnativeAdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (CustomNativeTemplateAd_t3403582769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[1] = 
{
	CustomNativeTemplateAd_t3403582769::get_offset_of_client_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (Gender_t1633829762)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2151[4] = 
{
	Gender_t1633829762::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (InterstitialAd_t207380487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[6] = 
{
	InterstitialAd_t207380487::get_offset_of_client_0(),
	InterstitialAd_t207380487::get_offset_of_OnAdLoaded_1(),
	InterstitialAd_t207380487::get_offset_of_OnAdFailedToLoad_2(),
	InterstitialAd_t207380487::get_offset_of_OnAdOpening_3(),
	InterstitialAd_t207380487::get_offset_of_OnAdClosed_4(),
	InterstitialAd_t207380487::get_offset_of_OnAdLeavingApplication_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (MediationExtras_t2251835164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[1] = 
{
	MediationExtras_t2251835164::get_offset_of_U3CExtrasU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (MobileAds_t1050225196), -1, sizeof(MobileAds_t1050225196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2154[1] = 
{
	MobileAds_t1050225196_StaticFields::get_offset_of_client_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (Reward_t3704020935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[2] = 
{
	Reward_t3704020935::get_offset_of_U3CTypeU3Ek__BackingField_1(),
	Reward_t3704020935::get_offset_of_U3CAmountU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (RewardBasedVideoAd_t2288675867), -1, sizeof(RewardBasedVideoAd_t2288675867_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2156[10] = 
{
	RewardBasedVideoAd_t2288675867::get_offset_of_client_0(),
	RewardBasedVideoAd_t2288675867_StaticFields::get_offset_of_instance_1(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdLoaded_2(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdFailedToLoad_3(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdOpening_4(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdStarted_5(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdClosed_6(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdRewarded_7(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdLeavingApplication_8(),
	RewardBasedVideoAd_t2288675867::get_offset_of_OnAdCompleted_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (DummyClient_t519661512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[9] = 
{
	DummyClient_t519661512::get_offset_of_OnAdLoaded_0(),
	DummyClient_t519661512::get_offset_of_OnAdFailedToLoad_1(),
	DummyClient_t519661512::get_offset_of_OnAdOpening_2(),
	DummyClient_t519661512::get_offset_of_OnAdStarted_3(),
	DummyClient_t519661512::get_offset_of_OnAdClosed_4(),
	DummyClient_t519661512::get_offset_of_OnAdRewarded_5(),
	DummyClient_t519661512::get_offset_of_OnAdLeavingApplication_6(),
	DummyClient_t519661512::get_offset_of_OnAdCompleted_7(),
	DummyClient_t519661512::get_offset_of_OnCustomNativeTemplateAdLoaded_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (MobileAdsEventExecutor_t1363665247), -1, sizeof(MobileAdsEventExecutor_t1363665247_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2164[3] = 
{
	MobileAdsEventExecutor_t1363665247_StaticFields::get_offset_of_instance_4(),
	MobileAdsEventExecutor_t1363665247_StaticFields::get_offset_of_adEventsQueue_5(),
	MobileAdsEventExecutor_t1363665247_StaticFields::get_offset_of_adEventsQueueEmpty_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (Utils_t3548761374), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (GoogleMobileAdsClientFactory_t3556675256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (NativeAdTypes_t3925888818)+ sizeof (RuntimeObject), sizeof(NativeAdTypes_t3925888818 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2167[1] = 
{
	NativeAdTypes_t3925888818::get_offset_of_CustomTemplateAd_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (AdLoaderClient_t2216398974), -1, sizeof(AdLoaderClient_t2216398974_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2168[8] = 
{
	AdLoaderClient_t2216398974::get_offset_of_adLoaderPtr_0(),
	AdLoaderClient_t2216398974::get_offset_of_adLoaderClientPtr_1(),
	AdLoaderClient_t2216398974::get_offset_of_adTypes_2(),
	AdLoaderClient_t2216398974::get_offset_of_customNativeTemplateCallbacks_3(),
	AdLoaderClient_t2216398974::get_offset_of_OnCustomNativeTemplateAdLoaded_4(),
	AdLoaderClient_t2216398974::get_offset_of_OnAdFailedToLoad_5(),
	AdLoaderClient_t2216398974_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
	AdLoaderClient_t2216398974_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (GADUAdLoaderDidReceiveNativeCustomTemplateAdCallback_t2228922418), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (GADUAdLoaderDidFailToReceiveAdWithErrorCallback_t3488831432), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (BannerClient_t2577994961), -1, sizeof(BannerClient_t2577994961_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2171[17] = 
{
	BannerClient_t2577994961::get_offset_of_bannerViewPtr_0(),
	BannerClient_t2577994961::get_offset_of_bannerClientPtr_1(),
	BannerClient_t2577994961::get_offset_of_OnAdLoaded_2(),
	BannerClient_t2577994961::get_offset_of_OnAdFailedToLoad_3(),
	BannerClient_t2577994961::get_offset_of_OnAdOpening_4(),
	BannerClient_t2577994961::get_offset_of_OnAdClosed_5(),
	BannerClient_t2577994961::get_offset_of_OnAdLeavingApplication_6(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_10(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_11(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_12(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_13(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_14(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_15(),
	BannerClient_t2577994961_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (GADUAdViewDidReceiveAdCallback_t2543294242), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (GADUAdViewDidFailToReceiveAdWithErrorCallback_t643467547), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (GADUAdViewWillPresentScreenCallback_t2057580186), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (GADUAdViewDidDismissScreenCallback_t972393216), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (GADUAdViewWillLeaveApplicationCallback_t3323587265), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (CustomNativeTemplateClient_t296756194), -1, sizeof(CustomNativeTemplateClient_t296756194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2177[4] = 
{
	CustomNativeTemplateClient_t296756194::get_offset_of_customNativeAdPtr_0(),
	CustomNativeTemplateClient_t296756194::get_offset_of_customNativeTemplateAdClientPtr_1(),
	CustomNativeTemplateClient_t296756194::get_offset_of_clickHandler_2(),
	CustomNativeTemplateClient_t296756194_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (GADUNativeCustomTemplateDidReceiveClick_t350204406), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (Externs_t92207873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (InterstitialClient_t301873194), -1, sizeof(InterstitialClient_t301873194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2180[12] = 
{
	InterstitialClient_t301873194::get_offset_of_interstitialPtr_0(),
	InterstitialClient_t301873194::get_offset_of_interstitialClientPtr_1(),
	InterstitialClient_t301873194::get_offset_of_OnAdLoaded_2(),
	InterstitialClient_t301873194::get_offset_of_OnAdFailedToLoad_3(),
	InterstitialClient_t301873194::get_offset_of_OnAdOpening_4(),
	InterstitialClient_t301873194::get_offset_of_OnAdClosed_5(),
	InterstitialClient_t301873194::get_offset_of_OnAdLeavingApplication_6(),
	InterstitialClient_t301873194_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
	InterstitialClient_t301873194_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_8(),
	InterstitialClient_t301873194_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_9(),
	InterstitialClient_t301873194_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_10(),
	InterstitialClient_t301873194_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (GADUInterstitialDidReceiveAdCallback_t821971233), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (GADUInterstitialDidFailToReceiveAdWithErrorCallback_t1323914714), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (GADUInterstitialWillPresentScreenCallback_t539653454), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (GADUInterstitialDidDismissScreenCallback_t1339081348), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (GADUInterstitialWillLeaveApplicationCallback_t1816935820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (MobileAdsClient_t1008075298), -1, sizeof(MobileAdsClient_t1008075298_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2186[1] = 
{
	MobileAdsClient_t1008075298_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (MonoPInvokeCallbackAttribute_t3472581009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (RewardBasedVideoAdClient_t745716004), -1, sizeof(RewardBasedVideoAdClient_t745716004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2188[18] = 
{
	RewardBasedVideoAdClient_t745716004::get_offset_of_rewardBasedVideoAdPtr_0(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_rewardBasedVideoAdClientPtr_1(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdLoaded_2(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdFailedToLoad_3(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdOpening_4(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdStarted_5(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdClosed_6(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdRewarded_7(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdLeavingApplication_8(),
	RewardBasedVideoAdClient_t745716004::get_offset_of_OnAdCompleted_9(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_10(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_11(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_12(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_13(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_14(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_15(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_16(),
	RewardBasedVideoAdClient_t745716004_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (GADURewardBasedVideoAdDidReceiveAdCallback_t462486315), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (GADURewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_t3979086788), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (GADURewardBasedVideoAdDidOpenCallback_t3638490629), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (GADURewardBasedVideoAdDidStartCallback_t2792276088), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (GADURewardBasedVideoAdDidCloseCallback_t623082069), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (GADURewardBasedVideoAdDidRewardCallback_t990863796), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (GADURewardBasedVideoAdWillLeaveApplicationCallback_t3217042531), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (GADURewardBasedVideoAdDidCompleteCallback_t2076181), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (Utils_t143735646), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (GetSetAttribute_t1349027187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[2] = 
{
	GetSetAttribute_t1349027187::get_offset_of_name_0(),
	GetSetAttribute_t1349027187::get_offset_of_dirty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (MinAttribute_t4172004135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[1] = 
{
	MinAttribute_t4172004135::get_offset_of_min_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
