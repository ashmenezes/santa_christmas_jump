﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Void
struct Void_t1185182177;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H



extern "C" void DEFAULT_CALL ReversePInvokeWrapper_AdLoaderClient_AdLoaderDidReceiveNativeCustomTemplateAdCallback_m235606549(intptr_t ___adLoader0, intptr_t ___nativeCustomTemplateAd1, char* ___templateID2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_AdLoaderClient_AdLoaderDidFailToReceiveAdWithErrorCallback_m589219946(intptr_t ___adLoader0, char* ___error1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_BannerClient_AdViewDidReceiveAdCallback_m4128431281(intptr_t ___bannerClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_BannerClient_AdViewDidFailToReceiveAdWithErrorCallback_m4025696061(intptr_t ___bannerClient0, char* ___error1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_BannerClient_AdViewWillPresentScreenCallback_m1511257388(intptr_t ___bannerClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_BannerClient_AdViewDidDismissScreenCallback_m2011858139(intptr_t ___bannerClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_BannerClient_AdViewWillLeaveApplicationCallback_m1540845370(intptr_t ___bannerClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_CustomNativeTemplateClient_NativeCustomTemplateDidReceiveClickCallback_m2887696975(intptr_t ___nativeCustomAd0, char* ___assetName1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_InterstitialClient_InterstitialDidReceiveAdCallback_m1432647788(intptr_t ___interstitialClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_InterstitialClient_InterstitialDidFailToReceiveAdWithErrorCallback_m3505725602(intptr_t ___interstitialClient0, char* ___error1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_InterstitialClient_InterstitialWillPresentScreenCallback_m3809953402(intptr_t ___interstitialClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_InterstitialClient_InterstitialDidDismissScreenCallback_m40656568(intptr_t ___interstitialClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_InterstitialClient_InterstitialWillLeaveApplicationCallback_m728138636(intptr_t ___interstitialClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_m3213608154(intptr_t ___rewardBasedVideoAdClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m1914081133(intptr_t ___rewardBasedVideoAdClient0, char* ___error1);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m3789826458(intptr_t ___rewardBasedVideoAdClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_m2371426017(intptr_t ___rewardBasedVideoAdClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m1220242393(intptr_t ___rewardBasedVideoAdClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_m336921326(intptr_t ___rewardBasedVideoAdClient0, char* ___rewardType1, double ___rewardAmount2);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_m1154470228(intptr_t ___rewardBasedVideoAdClient0);
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidCompleteCallback_m2243408736(intptr_t ___rewardBasedVideoAdClient0);
extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[21] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AdLoaderClient_AdLoaderDidReceiveNativeCustomTemplateAdCallback_m235606549),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_AdLoaderClient_AdLoaderDidFailToReceiveAdWithErrorCallback_m589219946),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_BannerClient_AdViewDidReceiveAdCallback_m4128431281),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_BannerClient_AdViewDidFailToReceiveAdWithErrorCallback_m4025696061),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_BannerClient_AdViewWillPresentScreenCallback_m1511257388),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_BannerClient_AdViewDidDismissScreenCallback_m2011858139),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_BannerClient_AdViewWillLeaveApplicationCallback_m1540845370),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_CustomNativeTemplateClient_NativeCustomTemplateDidReceiveClickCallback_m2887696975),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_InterstitialClient_InterstitialDidReceiveAdCallback_m1432647788),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_InterstitialClient_InterstitialDidFailToReceiveAdWithErrorCallback_m3505725602),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_InterstitialClient_InterstitialWillPresentScreenCallback_m3809953402),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_InterstitialClient_InterstitialDidDismissScreenCallback_m40656568),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_InterstitialClient_InterstitialWillLeaveApplicationCallback_m728138636),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidReceiveAdCallback_m3213608154),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidFailToReceiveAdWithErrorCallback_m1914081133),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidOpenCallback_m3789826458),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidStartCallback_m2371426017),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidCloseCallback_m1220242393),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidRewardUserCallback_m336921326),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdWillLeaveApplicationCallback_m1154470228),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_RewardBasedVideoAdClient_RewardBasedVideoAdDidCompleteCallback_m2243408736),
};
