﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTrigger : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.StartsWith("FloorCollider"))
        {
            BallController.instance.UpdateComboCount();
            StartCoroutine(transform.parent.GetComponent<Ragdoll>().ExplodeSelf(false));
        }
    }

}
