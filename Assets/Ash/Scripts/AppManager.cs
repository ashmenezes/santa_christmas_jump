﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;
using System.Collections;

public class AppManager : MonoBehaviour {

    [System.Serializable]
    public class Floor
    {
        [SerializeField] public GameObject prefab;
        [SerializeField] public float weight;
        [SerializeField] public float sumWeight;

        public Floor(GameObject prefab, float weight)
        {
            this.prefab = prefab;
            this.weight = weight;
        }
    }

    [Header("Variables")]
    [SerializeField] public int Level = 1;
    [SerializeField] public int Best = 0;
    [SerializeField] int NumberOfFloors;
    [SerializeField] int MinNumOfZones;
    [SerializeField] int MaxNumOfZones;

    [Header("References")]
    [SerializeField] GameObject GameOverPanel;
    [SerializeField] GameObject GameOverEffects;
    [SerializeField] GameObject WinGamePanel;
    [SerializeField] List<GameObject> Floors;
    [SerializeField] Camera cam;
    [SerializeField] Transform StartFloorPosition;
    [SerializeField] GameObject UIPanel;
    [SerializeField] Text LevelTextBox;
    [SerializeField] Text BestTextBox;
    [SerializeField] Text NextLevelTextBox;

    [SerializeField] public AudioClip SlipSound;
    [SerializeField] public AudioClip BounceSound;

    [Header("Prefabs")]
    [SerializeField] List<Floor> FloorPrefabs;
    [SerializeField] GameObject startFloorPrefab;
    [SerializeField] public GameObject endFloorPrefab;
    [SerializeField] GameObject floorZone;

    float sumWeight = 0f;

    public static AppManager instance;

    private void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start () {

        // Check level PlayerPrefs
        if (!PlayerPrefs.HasKey("level"))
            PlayerPrefs.SetInt("level", 1);
        else
            Level = PlayerPrefs.GetInt("level");
        //Level = Random.Range(1, 45);

        LevelTextBox.text = "Level: " + Level;

        // Check Best PlayerPrefs
        if (!PlayerPrefs.HasKey("best"))
            PlayerPrefs.SetInt("best", 0);
        else
            Best = PlayerPrefs.GetInt("best");

        // Update Best number
        BestTextBox.text = "Best: " + Best;

        // Calculate number of floors based on level;
        NumberOfFloors = Mathf.RoundToInt((0.5695f * Level) + 19.698f) < 40 ? Mathf.RoundToInt((0.5695f * Level) + 19.698f) : 40;
        UIPanel.transform.Find("#OfFloors").GetComponent<Text>().text = "#OfFloors: " + NumberOfFloors;

        // Set weights based on level
        setDifficulty();

        // Drop the stem Y position based on the number of floors
        Transform Stem = LevelController.instance.transform.Find("Stem");
        Stem.position = new Vector3(Stem.transform.position.x, -0.997f * (NumberOfFloors * 2) + 81.9f, Stem.transform.position.z);

        // Resets game time
        Time.timeScale = 1;
        GameOverPanel.SetActive(false);

        // calculates sum weights
        foreach (Floor f in FloorPrefabs)
        {
            sumWeight += f.weight;
            f.sumWeight = sumWeight;
        }

        // Sort FloorPrefabs list to Descending order
        //FloorPrefabs = FloorPrefabs.OrderByDescending(o => o.sumWeight).ToList();

        // instantiate starting floor
        float yPosTracker = 0;
        GameObject start = Instantiate(startFloorPrefab, StartFloorPosition.position, Quaternion.identity, LevelController.instance.gameObject.transform);
        start.transform.eulerAngles = new Vector3(start.transform.eulerAngles.x, start.transform.eulerAngles.y+90, start.transform.eulerAngles.z);
        yPosTracker -= 2f;

        // instantiate remaining level
        for(int i = 0; i < NumberOfFloors-2; i++)
        {
            // Calculate new pos for next floor
            Vector3 Updatedpos = new Vector3(StartFloorPosition.position.x, StartFloorPosition.position.y + yPosTracker, StartFloorPosition.position.z);

            GameObject selectedFloor = GetRandomFloor();

            // instantiate new floor
            GameObject go = Instantiate(
                selectedFloor,
                Updatedpos,
                Quaternion.identity,
                LevelController.instance.gameObject.transform);

            // rotate new floor
            go.transform.eulerAngles = new Vector3(go.transform.eulerAngles.x, Random.Range(0, 360), go.transform.eulerAngles.z);

            // decrement counter
            yPosTracker -= 2f;

            // Add zones to floor
            int numberOfZonesPerFloor = Random.Range(MinNumOfZones, MaxNumOfZones);
            Transform floor = go.transform.Find("floor");
            for (int j = 0; j < numberOfZonesPerFloor; j++)
            {
                Transform chosenPlatform = floor.GetChild(Random.Range(0, floor.transform.childCount)); // Randomly choose platform
                 Instantiate(
                    floorZone,
                    new Vector3(chosenPlatform.position.x, chosenPlatform.position.y, chosenPlatform.position.z),
                    chosenPlatform.rotation,
                    floor); //  Instantiate zone at platform
            }
        }

        // instantiate end floor
        Instantiate(
            endFloorPrefab,
            new Vector3(
                StartFloorPosition.position.x,
                StartFloorPosition.position.y + yPosTracker,
                StartFloorPosition.position.z),
            Quaternion.identity,
            LevelController.instance.gameObject.transform);
    }

    public GameObject GetRandomFloor()
    {
        double r = Random.value * sumWeight;

        foreach (Floor f in FloorPrefabs)
        {
            if (f.sumWeight >= r)
            {
                return f.prefab;
            }
        }
        return default(GameObject); //should only happen when there are no entries
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void NextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public IEnumerator EndGame()
    {
        //Time.timeScale = 0;
        LevelController.instance.gameObject.GetComponent<LevelController>().enabled = false;
        PlayerPrefs.SetInt("last", Level);
        PlayerPrefs.SetInt("level", 1);

        yield return new WaitForSeconds(1f);

        GameOverEffects.SetActive(true);
        GameOverPanel.SetActive(true);

        if (GameCounter.NumberOfGamesPlayed == 2)
        {
            GetComponent<AdsManager>().ShowIntAd();
            GameCounter.NumberOfGamesPlayed = 0;
            yield break;
        }
        GameCounter.NumberOfGamesPlayed += 1;
    }

    public void WinGame()
    {
        Time.timeScale = 0;
        LevelController.instance.gameObject.GetComponent<LevelController>().enabled = false;
        GameOverEffects.SetActive(true);
        WinGamePanel.SetActive(true);
        NextLevelTextBox.text = "Level " + (Level + 1);

        PlayerPrefs.SetInt("level", (PlayerPrefs.GetInt("level") + 1));
        Debug.Log(PlayerPrefs.GetInt("level"));

        if (Level > PlayerPrefs.GetInt("best"))
        {
            PlayerPrefs.SetInt("best", Level);
            Best = Level;
            BestTextBox.text = "Best:" + Best;
        }

        if (GameCounter.NumberOfGamesPlayed == 2)
        {
            GetComponent<AdsManager>().ShowIntAd();
            GameCounter.NumberOfGamesPlayed = 0;
            return;
        }
        GameCounter.NumberOfGamesPlayed += 1;
    }

    public void SecondChance()
    {
        PlayerPrefs.SetInt("level", PlayerPrefs.GetInt("last"));
        RestartLevel();
    }

    public void setDifficulty()
    {
        if (Level >= 30)
        {
            FloorPrefabs[0].weight = 25;
            FloorPrefabs[1].weight = 25;
            FloorPrefabs[2].weight = 20;
            FloorPrefabs[3].weight = 10;
            FloorPrefabs[4].weight = 10;
            FloorPrefabs[5].weight = 10;
            MinNumOfZones = 4;
            MaxNumOfZones = 4;
        }else if (Level >= 25 && Level < 30)
        {
            FloorPrefabs[0].weight = 25;
            FloorPrefabs[1].weight = 25;
            FloorPrefabs[2].weight = 10;
            FloorPrefabs[3].weight = 20;
            FloorPrefabs[4].weight = 20;
            FloorPrefabs[5].weight = 0;
            MinNumOfZones = 4;
            MaxNumOfZones = 4;
        }else if (Level >= 20 && Level < 25)
        {
            FloorPrefabs[0].weight = 5;
            FloorPrefabs[1].weight = 23;
            FloorPrefabs[2].weight = 23;
            FloorPrefabs[3].weight = 24;
            FloorPrefabs[4].weight = 25;
            FloorPrefabs[5].weight = 0;
            MinNumOfZones = 3;
            MaxNumOfZones = 4;
        }else if (Level >= 4 && Level < 20)
        {
            FloorPrefabs[0].weight = 10;
            FloorPrefabs[1].weight = 30;
            FloorPrefabs[2].weight = 30;
            FloorPrefabs[3].weight = 30;
            FloorPrefabs[4].weight = 0;
            FloorPrefabs[5].weight = 0;
            MinNumOfZones = 2;
            MaxNumOfZones = 3;
        }
        else if (Level >= 2 && Level < 4)
        {
            FloorPrefabs[0].weight = 20;
            FloorPrefabs[1].weight = 30;
            FloorPrefabs[2].weight = 30;
            FloorPrefabs[3].weight = 20;
            FloorPrefabs[4].weight = 0;
            FloorPrefabs[5].weight = 0;
            MinNumOfZones = 1;
            MaxNumOfZones = 2;
        }
        else if (Level == 1)
        {
            FloorPrefabs[0].weight = 33;
            FloorPrefabs[1].weight = 33;
            FloorPrefabs[2].weight = 34;
            FloorPrefabs[3].weight = 0;
            FloorPrefabs[4].weight = 0;
            FloorPrefabs[5].weight = 0;
            MinNumOfZones = 1;
            MaxNumOfZones = 1;
        }else if (Level == 0)
        {
            FloorPrefabs[0].weight = 0;
            FloorPrefabs[1].weight = 0;
            FloorPrefabs[2].weight = 0;
            FloorPrefabs[3].weight = 1;
            FloorPrefabs[4].weight = 0;
            FloorPrefabs[5].weight = 0;
            MinNumOfZones = 0;
            MaxNumOfZones = 0;
        }
    }

}
