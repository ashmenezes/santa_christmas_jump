﻿using UnityEngine;
using System.Collections;

public class LevelController : MonoBehaviour
{

    float f_lastX = 0.0f;
    float f_difX = 0.5f;
    //int i_direction = 1;

    float mult = 3f; // 5f
    float multAnd = 0.25f; // 0.5f

    [SerializeField] GameObject tutorialPanel;

    public static LevelController instance;

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        #if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            f_difX = 0.0f;
        }
        else if (Input.GetMouseButton(0))
        {
            if (tutorialPanel.activeSelf) tutorialPanel.SetActive(false);
            f_difX = Mathf.Abs(f_lastX - Input.GetAxis("Mouse X"));

            if (f_lastX < Input.GetAxis("Mouse X"))
            {
                //i_direction = -1;
                transform.Rotate(Vector3.up, -f_difX * mult);
            }

            if (f_lastX > Input.GetAxis("Mouse X"))
            {
                //i_direction = 1;
                transform.Rotate(Vector3.up, f_difX * mult);
            }

            f_lastX = -Input.GetAxis("Mouse X");
        }

#else
        if (Input.touchCount > 0)
        {
            if (tutorialPanel.activeSelf) tutorialPanel.SetActive(false);
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                f_difX = Input.GetTouch(0).deltaPosition.x;

                if (f_lastX < Input.GetTouch(0).position.x)
                {
                    //i_direction = -1;
                    transform.Rotate(Vector3.up, -f_difX * multAnd);
                }

                if (f_lastX > Input.GetTouch(0).position.x)
                {
                    //i_direction = 1;
                    transform.Rotate(Vector3.up, f_difX * multAnd);
                }

                f_lastX = -Input.GetTouch(0).position.x;
            }
            else
            {
                f_difX = 0.0f;
            }
        }
#endif
    }
}