﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoll : MonoBehaviour {

    bool move = false;
    Rigidbody rigidBody;

    // Explosion
    public float radius = 50.0f;
    public float power = 400.0f;

    public IEnumerator ExplodeSelf(bool explode)
    {
        foreach (Transform child in transform.Find("floor"))
        {
            if (explode) child.GetChild(0).GetComponent<MeshRenderer>().material = Resources.Load("Materials/Red") as Material;
            Destroy(child.transform.GetChild(0).GetComponent<MeshCollider>());
            Destroy(transform.Find("FloorTrigger").gameObject);
            rigidBody = child.gameObject.AddComponent<Rigidbody>();
            rigidBody.useGravity = true;
            rigidBody.isKinematic = false;
            rigidBody.detectCollisions = true;

            move = true;
            rigidBody.AddTorque((Vector3.left + Vector3.forward) * 30f);

        }

        Explode();

        //yield return new WaitForSeconds(explode ? 0.75f : 0.75f);
        Invoke("Delete", explode ? 0.5f : 0.25f);
        yield return null;
    }

    void Explode()
    {
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null && (rb.name == "platform" || rb.name == "zone"))
            {
                rb.AddExplosionForce(power, explosionPos, radius, 3.0f);
                rb.detectCollisions = false;
            }

            //rb.AddExplosionForce(power, explosionPos, radius, 3.0f);
        }
    }

    void Delete()
    {
        foreach (Transform child in transform.Find("floor"))
        {
            Destroy(child.gameObject);
        }
    }
}
