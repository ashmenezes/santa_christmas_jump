﻿using UnityEngine;
using UnityEngine.UI;

public class BallController : MonoBehaviour {

    [Header("Variables")]
    [SerializeField] float velocity;
    [SerializeField] bool Combo_Mode = false;
    [SerializeField] float diffHeight;
    [SerializeField] public int floorComboCount = 0;
    [SerializeField] int MaxComboCount = 3;
    [SerializeField] bool ragdoll = false;

    [Header("References")]
    [SerializeField] Material currentMaterial;
    [SerializeField] Material RedBallMaterial;
    [SerializeField] GameObject Trail;
    [SerializeField] GameObject Santa;
    [SerializeField] Color defaultTrailColor;
    [SerializeField] Color comboTrailColor;

    [SerializeField] Text combomodetext;
    [SerializeField] Text combocounttext;

    [Header("Prefabs")]
    [SerializeField] GameObject SnowSplash;

    // Bounce
    Rigidbody rb;
    float storedVelocity;
    Vector3 savedVelocity;

    public static BallController instance;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0, 4f, 0);
        storedVelocity = rb.velocity.y;

        //GameObject.Find("UIPanel").transform.Find("ComboMode").GetComponent<Text>().text = "ComboMode: " + Combo_Mode;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!ragdoll)
        {
            // Bouncing code
            rb.velocity = ragdoll ? new Vector3(0, 0, 0) : new Vector3(0, storedVelocity, 0);

            // if collides with end
            if (collision.transform.parent.parent.parent.gameObject.name.StartsWith(AppManager.instance.endFloorPrefab.name))
            //if (collision.transform.parent.parent.parent.gameObject.name.StartsWith(AppManager.instance.endFloorPrefab.name))
            {
                AppManager.instance.WinGame();
            }
            else if (collision.gameObject.name.StartsWith("platform"))  //   if collides with platform
            {
                //CameraController.instance.gameObject.GetComponent<CameraController>().enabled = false;  // Camera stops following ball
                if (Combo_Mode)  // if in combo mode
                {
                    if (collision.transform.parent.parent.parent.gameObject.name.StartsWith("floor_") && !ragdoll)
                    {
                        StartCoroutine(collision.transform.parent.parent.parent.GetComponent<Ragdoll>().ExplodeSelf(true));
                        floorComboCount = 0;    // Reset combo count
                        CameraController.instance.gameObject.GetComponent<CameraController>().enabled = true;  // Camera follows ball
                        return;
                    }
                }
            }
            else if (collision.gameObject.name.StartsWith("zone"))  //   if collides with red zone
            {
                if (Combo_Mode)  // if in combo mode
                {
                    // Destroy(collision.transform.parent.parent.parent.gameObject); // destroy whole floor
                    Transform parent = collision.transform.parent;
                    Destroy(collision.gameObject);
                    if (parent.parent.parent.gameObject.name.StartsWith("floor_") && !ragdoll)
                    {
                        StartCoroutine(parent.parent.parent.GetComponent<Ragdoll>().ExplodeSelf(true));
                        CameraController.instance.gameObject.GetComponent<CameraController>().enabled = true;  // Camera follows ball
                        return;
                    }
                }
                else
                {
                    ragdoll = true;

                    CameraController.instance.enabled = false;

                    rb.velocity = new Vector3(0, 0, 0);
                    rb.freezeRotation = false;
                    rb.constraints = RigidbodyConstraints.None;
                    rb.AddForceAtPosition((transform.position - new Vector3(Random.Range(-10f, 10f), transform.position.y, Random.Range(-10f, 10f))).normalized * 100f, transform.position, ForceMode.Acceleration);
                    Santa.GetComponent<Animator>().enabled = false;

                    AppManager.instance.GetComponent<AudioSource>().PlayOneShot(AppManager.instance.SlipSound, 0.5f);

                    StartCoroutine(AppManager.instance.EndGame());
                    return;
                }
            }
            floorComboCount = 0;    // Reset combo count
            combocounttext.text = "Combocount: " + floorComboCount;
            AppManager.instance.GetComponent<AudioSource>().PlayOneShot(AppManager.instance.BounceSound, 0.5f);
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.name.StartsWith("platform") && transform.position.y > other.transform.position.y && !ragdoll)
        {
            CameraController.instance.enabled = false;  // Camera stops following ball
            floorComboCount = 0;
        }
        else if (other.name.StartsWith("FloorTrigger") && !ragdoll)
        {
            CameraController.instance.enabled = true;  // Camera follows ball
            //UpdateComboCount();
        }
    }

    private void Update()
    {
        if (floorComboCount > MaxComboCount-1)
            enableComboMode();   
        else
            disableComboMode();
    }

    public void UpdateComboCount()
    {
        // Increase combo count
        floorComboCount += 1;
        combocounttext.text = "ComboCount: " + floorComboCount;
    }

    void enableComboMode()
    {
        Combo_Mode = true;
        GetComponent<MeshRenderer>().material = RedBallMaterial;
        GameObject.Find("UIPanel").transform.Find("ComboMode").GetComponent<Text>().text = "ComboMode: " + Combo_Mode;
        Trail.GetComponent<TrailRenderer>().startColor = comboTrailColor;
        Trail.GetComponent<TrailRenderer>().endColor = comboTrailColor;
    }

    void disableComboMode()
    {
        Combo_Mode = false;
        GetComponent<MeshRenderer>().material = currentMaterial;
        GameObject.Find("UIPanel").transform.Find("ComboMode").GetComponent<Text>().text = "ComboMode: " + Combo_Mode;
        Trail.GetComponent<TrailRenderer>().startColor = defaultTrailColor;
        Trail.GetComponent<TrailRenderer>().endColor = defaultTrailColor;
    }

    void SnowParticleEffect()
    {
        GameObject ps = Instantiate(SnowSplash, transform.position, Quaternion.identity, LevelController.instance.transform);
        ps.GetComponent<ParticleSystem>().Play();
    }

}
