﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using GoogleMobileAds.Api;
using UnityEngine.SceneManagement;

public class AdsManager : MonoBehaviour
{
    private InterstitialAd interstitial;
    private RewardBasedVideoAd rewardBasedVideo;

    public void Start()
    {
#if UNITY_ANDROID
            string appId = "ca-app-pub-1398998864673726~2942046311";
#elif UNITY_IPHONE
            string appId = "ca-app-pub-1398998864673726~1480537246";      
#else
        string appId = "unexpected_platform";
#endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
        this.RequestInterstitial();
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;
        // Called when the user should be rewarded for watching a video.
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        this.RequestRewardBasedVideo();
    }

    private void RequestRewardBasedVideo()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-1398998864673726/1833812926";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/1712485313";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Create an empty ad request.
        //AdRequest request = new AdRequest.Builder().AddTestDevice("4CCA2E855C82B4E66105B9ADF02D698C").Build();
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        this.rewardBasedVideo.LoadAd(request, adUnitId);
    }

    private void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-1398998864673726/9165508034";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/4411468910";
#else
        string adUnitId = "unexpected_platform";
#endif
        
        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);

        interstitial.OnAdClosed += HandleOnAdClosed;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for "
                        + amount.ToString() + " " + type);
        AppManager.instance.SecondChance();
        GameCounter.NumberOfGamesPlayed = 0;
    }

    public void ShowIntAd()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
            interstitial.Destroy();
        }
    }

    public void ShowRewAd()
    {
        if (rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.Show();
        }
        RequestRewardBasedVideo();
    }

    public void DestroyAds()
    {
    }
}
